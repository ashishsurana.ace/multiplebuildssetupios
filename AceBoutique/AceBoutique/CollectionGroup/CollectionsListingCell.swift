//
//  CollectionsListingCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 05/05/21.
//

import UIKit

class CollectionsListingCell: UICollectionViewCell {
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var ImgCollectionItem: UIImageView!
    @IBOutlet weak var lblCollectionName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.lblCollectionName.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
    }
    
}
