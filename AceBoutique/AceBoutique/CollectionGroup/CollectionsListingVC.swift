//
//  CollectionsListingVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 05/05/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics

/**Presents the all collections of the store.*/
class CollectionsListingVC: UIViewController {
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var collectionListing: UICollectionView!
    @IBOutlet weak var btnSideMenu: UIControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.collectionListing.register(UINib(nibName: "CollectionsListingCell", bundle: nil), forCellWithReuseIdentifier: "CollectionsListingCell")
        self.fetchAllCollectionGraphQlAPI()
        self.collectionListing.cr.addHeadRefresh(animator:FastAnimator() as CRRefreshProtocol) {
            [weak self] in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                
                self?.fetchAllCollectionGraphQlAPI()
                
            })
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblScreenTitle.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        FirebaseEventsLog.request.trackScreen(screenName: "Collection", className: "CollectionsListingVC")
        self.lblScreenTitle.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    func fetchAllCollectionGraphQlAPI(){
        
        let query = Storefront.buildQuery { $0
            .collections(first: 250) { $0
                .edges { $0
                    .node { $0
                        .id()
                        .title()
                        .image{ $0
                            .id()
                            .originalSrc()
                            
                        }
                    }
                }
            }
        }
        
        appDelegateShared.showHud()
//        client.cachePolicy = .cacheFirst(expireIn: 3600)
        let task = client.queryGraphWith(query) { response, error in
            appDelegateShared.dismissHud()
            if let response = response {
                let collections  = response.collections.edges.map { $0.node }
                GlobalObjects.storeFrontCollectionLists.removeAllObjects()
                
                collections.forEach { collection in
                    
                    let arrCollections: [String: Any] = ["CollectionId":collection.id.rawValue,
                                                         "CollectionName": collection.title,
                                                         "shopifyImageUrl":collection.image?.originalSrc]
                    
                    GlobalObjects.storeFrontCollectionLists.add(arrCollections)
                }
                DispatchQueue.main.async {
                    self.collectionListing.cr.endHeaderRefresh()
                    self.collectionListing.cr.resetNoMore()
                    self.collectionListing.reloadData()
                }
            }else{
                if let error = error, case .http(let statusCode) = error {
                    print("Query failed. HTTP error code: \(statusCode)")
                    self.Showalert1(message: "errorMsg".localized)
                }
            }
        }
        
        task.resume()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CollectionsListingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return GlobalObjects.storeFrontCollectionLists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionListing.dequeueReusableCell(withReuseIdentifier:"CollectionsListingCell", for: indexPath) as! CollectionsListingCell
        
        cell.lblCollectionName.text! = "\(String(describing: (GlobalObjects.storeFrontCollectionLists.object(at: indexPath.row) as! NSDictionary).value(forKey: "CollectionName") as! String))"
        
        let imgURL = (GlobalObjects.storeFrontCollectionLists.object(at: indexPath.row) as! NSDictionary).value(forKey: "shopifyImageUrl") as? URL
        
        cell.ImgCollectionItem.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
            // Your code inside completion block
            if image == nil {
                cell.ImgCollectionItem.image = #imageLiteral(resourceName: "placeholder")
            }
        })
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if GlobalObjects.storeFrontCollectionLists.count != 0 {
            let collection_id = (GlobalObjects.storeFrontCollectionLists.object(at: indexPath.row) as! NSDictionary).value(forKey: "CollectionId") as? String
            let collection_name = (GlobalObjects.storeFrontCollectionLists.object(at: indexPath.row) as! NSDictionary).value(forKey: "CollectionName") as? String
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let detailVC = storyBoard.instantiateViewController(withIdentifier: "ProductListVC") as? ProductListVC
            detailVC!.CollectionID = collection_id
            detailVC!.ScreenTitle = collection_name
            self.navigationController?.pushViewController(detailVC!, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.collectionListing.frame.width
        if (UIDevice.current.userInterfaceIdiom == .pad){
            let cellWidth = ((width - 30)/2)
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                return CGSize(width:cellWidth, height: (cellWidth * 1.5))
            }else{
                return CGSize(width:((width - 30)/2), height: cellWidth + 35) // add 35 if cell not proper
            }
        }else{
            let cellWidth = ((width - 15)/2)
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                
                return CGSize(width: cellWidth, height: (cellWidth * 1.5))
                
            }else{
                
                return CGSize(width: cellWidth, height: cellWidth + 35)
            }
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if (UIDevice.current.userInterfaceIdiom == .pad){
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }else{
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
}
