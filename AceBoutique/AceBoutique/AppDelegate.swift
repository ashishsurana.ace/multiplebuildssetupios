//
//  AppDelegate.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 28/01/21.
//

import UIKit
import AKSideMenu
import IQKeyboardManagerSwift
import Firebase
import FirebaseAnalytics
//import MobileBuySDK
import FirebaseDatabase
import FirebaseFirestore
import UserNotifications
import FirebaseDynamicLinks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var Cart_DB = AppDataClass()
    var ref: DatabaseReference!
    
    let notificationCenter = UNUserNotificationCenter.current()
    let gcmMessageIDKey = "gcm.message_id"
    var version: String = ""
    
   
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        RunLoop.current.run(until: Date(timeIntervalSinceNow: 3.0))
        IQKeyboardManager.shared.enable = true
        Cart_DB.createDB()
        FirebaseApp.configure()
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        //        self.sendLocalNotification()
        Messaging.messaging().delegate = self
        Messaging.messaging().subscribe(toTopic: FirebaseSubscription.general.rawValue)
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(String(describing: token))")
//        self.updateUserTokenToFirebase(token: token!)
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        
        
        
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            
            let remoteNotificationPayload = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable : Any]
            if remoteNotificationPayload != nil {
                
                isFromNotification = true
                setMyUserDefaults(value: remoteNotificationPayload!, key: MyUserDefaults.NotificationPayload)
                let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "DummySplashVC") as! DummySplashVC
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
                
            }
            
        }
//        else if let url = launchOptions?[UIApplication.LaunchOptionsKey.url] as? URL {
//            print(url)
//
//        } else if let activityDictionary = launchOptions?[UIApplication.LaunchOptionsKey.userActivityDictionary] as? [AnyHashable: Any] {
//            print(activityDictionary)
//            for key in activityDictionary.keys {
//                if let userActivity = activityDictionary[key] as? NSUserActivity {
//                       if let url = userActivity.webpageURL {
//                        setMyUserDefaults(value: url.absoluteString, key: "continueURL")
//                    }
//                }
//            }
//        }
        else{
            
            let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "DummySplashVC") as! DummySplashVC
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
        
        
        return true
    }
    
    
    //MARK: - App Shortcut Handler Method -
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        
        var shortcutPath: String = ""
        
        switch shortcutItem.type {
        
        case "com.shopifyapp.Orders":
             
            shortcutPath = "com.shopifyapp.Orders"
            setMyUserDefaults(value: shortcutPath, key: "shortcutItem")
            let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "DummySplashVC") as! DummySplashVC
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
            
        case "com.shopifyapp.Favourite":
            
            shortcutPath = "com.shopifyapp.Favourite"
            setMyUserDefaults(value: shortcutPath, key: "shortcutItem")
            let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "DummySplashVC") as! DummySplashVC
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        
        case "com.shopifyapp.Cart":
            
            shortcutPath = "com.shopifyapp.Cart"
            setMyUserDefaults(value: shortcutPath, key: "shortcutItem")
            let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "DummySplashVC") as! DummySplashVC
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
            
        default:
            break
            
        }
    }
      
    //MARK: - Send Local Notification -
    
    func sendLocalNotification(){
        
        //Create content for your notification
        let content = UNMutableNotificationContent()
        content.title = "Local Notification"
        content.body = "This is to test triggering of notification"
      
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 900, repeats: false)
        let uuid = UUID().uuidString
        let req = UNNotificationRequest(identifier: uuid, content: content, trigger: trigger)
        
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.add(req) { (error) in
            print(error)
        }
        
    }
    
    //MARK: - Update Token to Firebase Database -
    
    func updateUserTokenToFirebase(token:String) {
        if token != nil {
            let db = Firestore.firestore()
            
            if UserDefaults.standard.object(forKey: MyUserDefaults.firebaseToken) != nil {
                let oldToken = UserDefaults.standard.string(forKey: MyUserDefaults.firebaseToken)
                if oldToken != token {
                    db.collection("users").addDocument(data:[
                        "did": token,
                        "pid": 2,
                        "sdt": FieldValue.serverTimestamp()
                    ]) { [self] err in
                        if let err = err {
                            print("Error writing document: \(err)")
                        }else {
                            print("Document successfully written!")
                            setMyUserDefaults(value: token, key: MyUserDefaults.firebaseToken)
                        }
                    }
                }
            }else{
                
                //insert token in db and firebase
                db.collection("users").addDocument(data:[
                    "did": token,
                    "pid": 2,
                    "sdt": FieldValue.serverTimestamp()
                ]) { [self] err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    }else {
                        print("Document successfully written!")
                        setMyUserDefaults(value: token, key: MyUserDefaults.firebaseToken)
                    }
                }
            }
        }
    }
    
    
    //MARK: - User Notification Delegates -
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        
        
        print("didReceiveRemoteNotification",userInfo)

        isFromNotification = true
        setMyUserDefaults(value: userInfo, key: MyUserDefaults.NotificationPayload)
//        self.setPushRootController()
        
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        
        print("fetchCompletionHandler",userInfo)

        isFromNotification = true
        setMyUserDefaults(value: userInfo, key: MyUserDefaults.NotificationPayload)
//        self.setPushRootController()
        completionHandler(UIBackgroundFetchResult.newData)
    }
   
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // [START_EXCLUDE]
        // Print message ID.
        
        print("willPresent",userInfo)
        
      
        if #available(iOS 14.0, *) {
            completionHandler([[.list, .sound]])
        } else {
            // Fallback on earlier versions
            completionHandler([[.alert, .sound]])
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        print("didReceive",userInfo)
        
        isFromNotification = true
        setMyUserDefaults(value: userInfo, key: MyUserDefaults.NotificationPayload)
//        self.setPushRootController()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window

        let state = UIApplication.shared.applicationState

        if state == .active {

            guard let actionType = userInfo["action"] as? String else {
                return
        }

            if actionType.lowercased() == NotificationActions.general.rawValue.lowercased() {

                let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "DummySplashVC") as! DummySplashVC
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()

            }else if actionType.lowercased() == NotificationActions.abandoned_checkout.rawValue.lowercased() {

                let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "WebViewCheckoutVC") as! WebViewCheckoutVC
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()

            }else if actionType.lowercased() == NotificationActions.abandoned_cart.rawValue.lowercased() {

                let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "CartNavigationController") as! CartNavigationController
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()

            }else if actionType.lowercased() == NotificationActions.open_collection.rawValue.lowercased() {
                
               let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()

            }else if actionType.lowercased() == NotificationActions.open_product.rawValue.lowercased() {
                
                let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
            } else {
                let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "DummySplashVC") as! DummySplashVC
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
            }
            
        }else{
            
            let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "DummySplashVC") as! DummySplashVC
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
      
        completionHandler()
    }
    
    
    //MARK: - Universal Linking -
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      let handled = DynamicLinks.dynamicLinks()
        .handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
          // ...
            print(dynamiclink)
            print(error)
            var strArray = dynamiclink?.url?.absoluteString.components(separatedBy: "&")

              if let strbaseURL = strArray?.first{
                   strArray = strbaseURL.components(separatedBy: "product_id=")
                  if  let productID = strArray?.last {
                      print(productID)
                      isFromDeepLink = true
                      setMyUserDefaults(value: productID, key: "DeepLinkProductID")
                      let storyboard = UIStoryboard(name: "Main", bundle: nil)
                      let window = UIWindow(frame: UIScreen.main.bounds)
                      appDelegateShared.window = window
                      if let detailVC = storyboard.instantiateViewController(withIdentifier: "DummySplashVC") as? DummySplashVC {
                          self.window?.rootViewController = detailVC
                          self.window?.makeKeyAndVisible()
                      }
                  }

              }
        }

      return handled
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
            print("OpenURLOptionsKey call \(url)")
       
        return true
        
    }
    
    func presentDetailViewController(_ redirectionUrl: URL) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        guard
            let rootVC = storyboard
                .instantiateViewController(withIdentifier: "rootController")
                as? RootViewController
        //          let navigationVC = storyboard
        //            .instantiateViewController(withIdentifier: "contentViewController")
        //              as? UINavigationController
        else { return }
        
        //        navigationVC.modalPresentationStyle = .formSheet
        //        navigationVC.pushViewController(detailVC, animated: true)
        
        window?.rootViewController = rootVC
        window?.makeKeyAndVisible()
        
    }
    
    //  MARK:- Messaging Delegates -
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        Messaging.messaging().token { token, error in
            // Check for error. Otherwise do what you will with token here
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let token = token {
                print("Remote instance ID token: \(token)")
//                let tokenFCM = token
//                self.updateUserTokenToFirebase(token: tokenFCM)
                setMyUserDefaults(value: token, key: MyUserDefaults.firebaseToken)
//                let dataDict:[String: String] = ["token": fcmToken ?? ""]
//                NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
            }
        }
    }
    
    
    //MARK: - App Delegates -
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTable"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadProductGrid"), object: nil)
        
        
        if UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken) != nil {
            GlobalObjects.tokenExpiresAt = (UserDefaults.standard.value(forKey: MyUserDefaults.TokenExpire) as! Date)
            
            let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
            let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: GlobalObjects.currentDate, to: GlobalObjects.tokenExpiresAt)
            
            if (difference.day! <= 1) {
                let accessToken = UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken)
                ClientMutations.shared.renewCustomerToken(accessToken: accessToken!) { (accesstoken, error) in
                    if let accessToken = accesstoken {
                        
                        UserDefaults.standard.set(accessToken, forKey: MyUserDefaults.CustomerToken)
                        UserDefaults.standard.synchronize()
                        
                    }else{
                        
                        let message = error[0].message
                        print(message)
                    }
                }
            }
        }
        
        DispatchQueue.global().async {
            _ = try? self.isUpdateAvailable { (update, error) in
                if let error = error {
                    print(error)
                } else if let update = update {
                    print(update)
                    if update {
                        GlobalObjects.isUpdateAvailable = true
                    }
                }
            }
        }
    }
    
    //MARK: - Check for App updates -
    
    func isUpdateAvailable(completion: @escaping (Bool?, Error?) -> Void) throws -> URLSessionDataTask {
        guard let info = Bundle.main.infoDictionary,
              let currentVersion = info["CFBundleShortVersionString"] as? String,
              let identifier = info["CFBundleIdentifier"] as? String,
              let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
            throw VersionError.invalidBundleInfo
        }
        print(currentVersion)
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                if let error = error { throw error }
                guard let data = data else { throw VersionError.invalidResponse }
                let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
                guard let result = (json?["results"] as? [Any])?.first as? [String: Any], let version1 = result["version"] as? String else {
                    throw VersionError.invalidResponse
                }
                self.version = version1
                print(self.version)
                if currentVersion < self.version {
                    completion(true, nil)
                }else{
                    completion(false, nil)
                }
                
            } catch {
                completion(nil, error)
            }
        }
        task.resume()
        return task
    }
    
    
    //MARK:- Remove WhiteSpace and New Line Space
    
    func TrimString(string:String) -> String
    {
        var trimmedString:String?
        trimmedString = string.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return trimmedString!
    }
    
    func isValidURL(_ testStr:String) -> Bool {
        
        //        let urlRegEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let emaiRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emaiRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    
    //MARK:- Hudder Method
    
    func showHud()
    {
        SVProgressHUD.setDefaultAnimationType(.flat)
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setBackgroundColor(UIColor.clear)
        SVProgressHUD.setForegroundColor(UIColor.black)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    
    func dismissHud()
    {
        SVProgressHUD.dismiss()
        
    }
    
}


extension UIViewController {
    
    func Showalert1(message: String) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "btnOk".localized, style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithAction(_ title:String, msg:String, button1:String, withBlocks block: @escaping (_ status: Int) -> Void)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: button1, style: .default, handler: { (Finish) in
            block(1)
        }))
        present(alert, animated: true, completion: nil)
    }
    
}

