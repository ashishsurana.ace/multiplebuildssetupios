//
//  CartItems.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation

class MyCartItems {
    
    var id: Int
    var variant_id: String
    var title: String
    var variant_type: String
    var variant_value: String
    var price:String
    var currency_code: String
    var quantity: Int
    var Img_url: String
    
    init(id: Int, variant_id: String, title: String, variant_type: String, variant_value: String, price:String, currency_code: String, quantity: Int,  Img_url: String){
        
        self.id = id
        self.variant_id = variant_id
        self.title = title
        self.variant_type = variant_type
        self.variant_value = variant_value
        self.price = price
        self.currency_code = currency_code
        self.quantity = quantity
        self.Img_url = Img_url
        
    }
}
