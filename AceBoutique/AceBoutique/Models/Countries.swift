//
//  Countries.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 05/04/21.
//

import Foundation

// MARK: - WelcomeElement
struct Countries: Codable {
    var name, iso3, iso2, numericCode: String?
    var phoneCode, capital, currency, currencySymbol: String?
    var tld: String?
    var native: String?
    var region: Region?
    var subregion: String?
    var timezones: [Timezone]?
    var translations: Translations?
    var latitude, longitude, emoji, emojiU: String?
    var states: [State]?

    enum CodingKeys: String, CodingKey {
        case name, iso3, iso2
        case numericCode = "numeric_code"
        case phoneCode = "phone_code"
        case capital, currency
        case currencySymbol = "currency_symbol"
        case tld, native, region, subregion, timezones, translations, latitude, longitude, emoji, emojiU, states
    }
}

enum Region: String, Codable {
    case africa = "Africa"
    case americas = "Americas"
    case asia = "Asia"
    case empty = ""
    case europe = "Europe"
    case oceania = "Oceania"
    case polar = "Polar"
}

// MARK: - State
struct State: Codable {
    var id: Int?
    var name, stateCode: String?
    var latitude, longitude: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case stateCode = "state_code"
        case latitude, longitude
    }
}

// MARK: - Timezone
struct Timezone: Codable {
    var zoneName: String?
    var gmtOffset: Int?
    var gmtOffsetName, abbreviation, tzName: String?
}

// MARK: - Translations
struct Translations: Codable {
    var kr, br, pt, nl: String?
    var hr, fa, de, es: String?
    var fr, ja, it, cn: String?
}

typealias countries = [Countries]



//import Foundation
//
//// MARK: - Welcome
//struct CountiresList: Codable {
//    var countries: [Country]
//}
//
//// MARK: - Country
//struct Country: Codable {
//    var id: Int?
//    var name: String?
//    var tax: Double?
//    var code, taxName: String?
//    var provinces: [Province]?
//
//    enum CodingKeys: String, CodingKey {
//        case id, name, tax, code
//        case taxName = "tax_name"
//        case provinces
//    }
//}
//
//// MARK: - Province
//struct Province: Codable {
//    var id, countryID: Int?
//    var name, code: String?
//    var tax: Double?
//    var taxName: String?
//    var taxType: String?
//    var taxPercentage: Double?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case countryID = "country_id"
//        case name, code, tax
//        case taxName = "tax_name"
//        case taxType = "tax_type"
//        case taxPercentage = "tax_percentage"
//    }
//}
//
//enum TaxName: String, Codable {
//    case grt = "GRT"
//    case stateTax = "State Tax"
//}
//
//// MARK: - Encode/decode helpers
//
@objcMembers class JSONNull: NSObject, Codable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    override public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

