//
//  shopLocales.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 12/07/21.
//

import Foundation


// MARK: - -

@objc class shopLocales: NSObject {
    
    var locale: String
    var name: String
    var primary: Bool
    var published: Bool
    
    init(locale:String, name:String,primary:Bool,published: Bool) {
        
        self.locale = locale
        self.name = name
        self.primary = primary
        self.published = published
    }
    
}


