//
//  Favourites.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation

class FavouriteItems {
    
    var id: Int
    var variant_id: String
    var user_email: String
    var title: String
    var price:String
    var currency_code: String
    var Img_url: String
    
    init(id: Int, variant_id: String, user_email: String, title: String, price:String, currency_code: String, Img_url: String){
        
        self.id = id
        self.variant_id = variant_id
        self.user_email = user_email
        self.title = title
        self.price = price
        self.currency_code = currency_code
        self.Img_url = Img_url
        
    }
}
