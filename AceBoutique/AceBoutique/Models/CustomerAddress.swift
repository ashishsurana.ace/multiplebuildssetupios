//
//  CustomerAddress.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation


class CustomerAddress {
    
    var id: String?
    var firstName: String?
    var lastName: String?
    var address1: String?
    var address2: String?
    var phone: String?
    var city: String?
    var state: String?
    var country: String?
    var zipcode:String?
    
    
    init(id: String?, firstName: String?, lastName: String?, address1: String?, address2: String?, phone: String?, city: String?, state: String?, country: String?, zipcode:String?) {
        
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.address1 = address1
        self.address2 = address2
        self.phone = phone
        self.city = city
        self.state = state
        self.country = country
        self.zipcode = zipcode
        
    }
}
