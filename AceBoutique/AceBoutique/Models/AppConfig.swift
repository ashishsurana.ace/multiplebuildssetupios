//
//  AppConfig.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 17/02/21.
//

import Foundation


struct ConfigResponse: Codable {
    var globalSettings: GlobalSettings?
    var menu: Menu?
    var shopProperties: ShopProperties?
    var homeScreenSettings: [HomeScreenSetting]?
    
    enum CodingKeys: String, CodingKey {
        case globalSettings = "global_settings"
        case menu
        case shopProperties = "shop_properties"
        case homeScreenSettings = "home_screen_settings"
    }
}

// MARK: - GlobalSettings
struct GlobalSettings: Codable {
    var themeColor: String?
    var brandLogoURL: String?
    var splashScreenURL: String?
    var planID: Int
    var appStoreURL, playStoreURL, appLayoutType, plan, planExpDate, appUpdate: String?
    
    enum CodingKeys: String, CodingKey {
        case themeColor = "theme_color"
        case brandLogoURL = "brand_logo_url"
        case splashScreenURL = "splash_screen_url"
        case appStoreURL = "app_store_url"
        case playStoreURL = "play_store_url"
        case appLayoutType = "app_layout_type"
        case planID = "plan_id"
        case plan
        case planExpDate = "plan_exp_date"
        case appUpdate = "app_update"
    }
}

// MARK: - shopProperties

struct ShopProperties: Codable {

    var ShopDomain: String?
    var APIKEY: String?
    var platformKey: String?
    var adminToken: String?
    var GraphQLVersion: String?
    
    enum CodingKeys: String, CodingKey {
        
        case ShopDomain = "shop_domain"
        case APIKEY = "storefront_access_key"
        case platformKey = "platform_api_key"
        case adminToken = "admin_access_key"
        case GraphQLVersion = "graphql_version"
    }
}

// MARK: - HomeScreenSetting
struct HomeScreenSetting: Codable {
    var layoutType: String?
    var title: String?
    var data: DataClass?
    
    enum CodingKeys: String, CodingKey {
        case layoutType = "layout_type"
        case title, data
    }
}

// MARK: - DataClass
struct DataClass: Codable {
    var collections: [Collection]?
    var imageSlider: [Image]?
    var product: Product?
    var imageBanner: Image?
    var announcement: Announcement?
    
    enum CodingKeys: String, CodingKey {
        case collections
        case imageSlider = "image_slider"
        case product
        case imageBanner = "image_banner"
        case announcement
    }
}

// MARK: - Announcement
struct Announcement: Codable {
    var announcementTitle, startDateTime, endDateTime, textColor: String?
    var backgroundColor: String?
    var announcementDescription: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case announcementTitle = "announcement_title"
        case startDateTime = "start_date_time"
        case endDateTime = "end_date_time"
        case textColor = "text_color"
        case backgroundColor = "background_color"
        case announcementDescription = "announcement_description"
    }
}

// MARK: - Collection
struct Collection: Codable {
    var id: Int
    var platformCollectionID: String?
    var isShowCustomTitle, isShowOverlay: Bool
    var customImageURL: String?
    var customTitle: String?
    var shopifyImageUrl: URL?
    
    enum CodingKeys: String, CodingKey {
        case id
        case platformCollectionID = "platform_collection_id"
        case isShowCustomTitle = "is_show_custom_title"
        case isShowOverlay = "is_show_overlay"
        case customImageURL = "custom_image_url"
        case customTitle = "custom_title"
        case shopifyImageUrl = "shopify_Image_Url"
    }
}

// MARK: - Image
struct Image: Codable {
    var id: Int
    var bannerImageURL: String?
    var platformCollectionID, platformProductID: String?
    var isClickable, isCollection, isProduct: Bool
    var onClickRedirection: String?
    var imageURL: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case bannerImageURL = "banner_image_url"
        case platformCollectionID = "platform_collection_id"
        case platformProductID = "platform_product_id"
        case isClickable = "is_clickable"
        case isCollection = "is_collection"
        case isProduct = "is_product"
        case onClickRedirection = "on_click_redirection"
        case imageURL = "image_url"
    }
}

// MARK: - Product
struct Product: Codable {
    var id: Int
    var platformCollectionID: String?
    var showTotalProducts: Int
    
    enum CodingKeys: String, CodingKey {
        case id
        case platformCollectionID = "platform_collection_id"
        case showTotalProducts = "show_total_products"
    }
}

// MARK: - Menu
struct Menu: Codable {
    var backgroundImageURL: String?
    var items: [Item]?
    
    enum CodingKeys: String, CodingKey {
        case backgroundImageURL = "background_image_url"
        case items
    }
}

// MARK: - Item
struct Item: Codable {
    var title: String?
}

// MARK: - Encode/decode helpers

//@objcMembers class JSONNull: NSObject, Codable {
//    
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//    
//    override public init() {}
//    
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//    
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}


struct RotatingImage: Codable {
    var id: Int
    var bannerImageURL: String?
    var platformCollectionID, platformProductID: String?
    var isClickable, isCollection, isProduct: Bool
    var onClickRedirection: String?
    var imageURL: String?
    
    init(id: Int, bannerImageURL: String?, platformCollectionID:String?, platformProductID: String?, isClickable:Bool, isCollection:Bool, isProduct: Bool, onClickRedirection: String?, imageURL: String?) {
        self.id = id
        self.bannerImageURL = bannerImageURL
        self.platformCollectionID = platformCollectionID
        self.platformProductID = platformProductID
        self.isClickable = isClickable
        self.isCollection = isCollection
        self.isProduct = isProduct
        self.onClickRedirection = onClickRedirection
        self.imageURL = imageURL
    }
}
