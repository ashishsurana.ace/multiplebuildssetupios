//
//  StoreLists.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 23/09/21.
//

import Foundation

struct StoreLists: Codable {
    var code: Int?
    var message: String?
    var data: [DataList]?
}

// MARK: - Datum
struct DataList: Codable {
    var storeID, storeURL, configURL: String?

    enum CodingKeys: String, CodingKey {
        case storeID = "store_id"
        case storeURL = "store_url"
        case configURL = "config_url"
    }
}
