//
//  Orders.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation

class OrderHistory {
    
    var id:String
    var orderNumber: String
    var total:String
    var orderDate: String
    var totalItems: Int
    var ImgUrl:URL
    var currencyCode: String
    
    init(id:String, orderNumber:String,total:String,orderDate: String,totalItems: Int, ImgUrl: URL, currencyCode: String) {
        
        self.id = id
        self.orderNumber = orderNumber
        self.total = total
        self.orderDate = orderDate
        self.totalItems = totalItems
        self.ImgUrl = ImgUrl
        self.currencyCode = currencyCode
    }
}

class OrderLineItems {
    
    var productName: String
    var variantName:String
    var variantValue: String
    var price: Int
    var quantity: Int
    var ImgUrl:URL
    var currency_code: String
    
    init(productName: String, variantName:String, variantValue: String, price: Int, quantity: Int, ImgUrl: URL, currency_code: String) {
        
        self.productName = productName
        self.variantName = variantName
        self.variantValue = variantValue
        self.price = price
        self.quantity = quantity
        self.ImgUrl = ImgUrl
        self.currency_code = currency_code
    }
}
