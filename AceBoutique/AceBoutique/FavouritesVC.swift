//
//  FavouritesVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 20/03/21.
//

import UIKit
import Firebase
import FirebaseAnalytics


class FavouritesVC: UIViewController {
    
    @IBOutlet weak var FavCollectionView: UICollectionView!
    
    
    var arrFavItems = [FavouriteItems]()
    var Fav_DB = FavouriteQueries()
    var shortcutPath:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.FavCollectionView.register(UINib(nibName: "GridCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GridCollectionCell")
        self.FavCollectionView.register(UINib(nibName: "EmptyShowCollectionCell", bundle: nil), forCellWithReuseIdentifier: "EmptyShowCollectionCell")
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWhenAPNSCall(_:)), name: NSNotification.Name(rawValue: "reloadTable"), object: nil)
        
        print("shortcut path",self.shortcutPath)
    }
    
    
    @objc func reloadWhenAPNSCall(_ notification: NSNotification) {
        
        self.FavCollectionView.reloadData()
        self.view.layoutIfNeeded()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        FirebaseEventsLog.request.trackScreen(screenName: "Wish list", className: "FavouritesVC")
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.arrFavItems.removeAll()
        if UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail) != nil {
            let email_Id = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
            self.arrFavItems = self.Fav_DB.GetFavByUserId(user_email: email_Id!)
            self.arrFavItems = self.arrFavItems.sorted { $0.id  > $1.id }
        }
        
        self.FavCollectionView.reloadData()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func redirectToHome(sender: UIButton){
        
        let baseVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardView")
        let contentViewController = UINavigationController(rootViewController: baseVC!)
        self.sideMenuViewController?.setContentViewController(contentViewController, animated: true)
        self.sideMenuViewController?.hideMenuViewController()
    }
    
    // MARK: - FAV BUTTON ACTION
    
    @objc func addToFavorite(sender: AnyObject) -> () {
        print(sender.tag!)
        
        var superview = sender.superview
        while let view = superview, !(view is UICollectionViewCell) {
            superview = view?.superview
        }
        guard let cell = superview as? UICollectionViewCell else {
            print("button is not contained in a table view cell")
            return
        }
        guard let indexPath = FavCollectionView.indexPath(for: cell) else {
            print("failed to get index path for cell containing button")
            return
        }
        
        let variant_id = self.arrFavItems[sender.tag!].variant_id
        
        self.Fav_DB.deleteFavItem(variant_Id: variant_id)
        
        self.arrFavItems.removeAll()
        let email_Id = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
        self.arrFavItems = self.Fav_DB.GetFavByUserId(user_email: email_Id!)
        self.arrFavItems = self.arrFavItems.sorted { $0.id  > $1.id }
        
        self.FavCollectionView.reloadData()
        
    }
    
}

extension FavouritesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.arrFavItems.count != 0 {
            return self.arrFavItems.count
        }else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.arrFavItems.count != 0 {
            let cell = FavCollectionView.dequeueReusableCell(withReuseIdentifier:"GridCollectionCell", for: indexPath) as! GridCollectionCell
            
            cell.lblItemName.text! =  self.arrFavItems[indexPath.row].title //"\(title)"
            
            let price = (self.arrFavItems[indexPath.row].price as NSString).floatValue
            let currency = self.arrFavItems[indexPath.row].currency_code
            let locale = Locale.getSymbolForCurrencyCode(code: currency)
            cell.lblPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
            cell.ImgFav.image = #imageLiteral(resourceName: "fav_filled")
            cell.ImgFav.image = cell.ImgFav.image!.withRenderingMode(.alwaysTemplate)
            cell.ImgFav.tintColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
            
            cell.btnFavorite.isHidden = false
            cell.widthFavouritConstraint.constant = 30
            cell.btnFavorite.tag = indexPath.row
            cell.btnFavorite.addTarget(self, action: #selector(self.addToFavorite(sender:)), for: .touchUpInside)
            
            let imgURL = URL(string: self.arrFavItems[indexPath.row].Img_url)
            
            cell.ImgProductGrid.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                // Your code inside completion block
                if image == nil {
                    
                }
            })
            
            return cell
            
        }else {
            
            let cell1 = FavCollectionView.dequeueReusableCell(withReuseIdentifier:"EmptyShowCollectionCell", for: indexPath) as! EmptyShowCollectionCell
            cell1.lblShowMessage.text! = "empty_wishlist_msg".localized
            cell1.lblButtonText.text! = "Home".localized.uppercased()
            cell1.btnEmptyAction.addTarget(self, action: #selector(self.redirectToHome(sender:)), for: .touchUpInside)
            return cell1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.arrFavItems.count != 0 {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let detailVC = storyBoard.instantiateViewController(withIdentifier: "ProductDetailVC") as? ProductDetailVC
            detailVC?.Product_Id = self.arrFavItems[indexPath.row].variant_id
            self.navigationController?.pushViewController(detailVC!, animated: true)
            
        }else{
            let baseVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardView")
            let contentViewController = UINavigationController(rootViewController: baseVC!)
            self.sideMenuViewController?.setContentViewController(contentViewController, animated: true)
            self.sideMenuViewController?.hideMenuViewController()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.arrFavItems.count != 0 {
            let width = self.FavCollectionView.frame.width
            //            let jsonData = GlobalObjects.jsonString.data(using: .utf8)!
            //            let objConfig = try! JSONDecoder().decode(ConfigResponse.self, from: jsonData)
            
            if (UIDevice.current.userInterfaceIdiom == .pad){
                if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                    return CGSize(width:((width - 30)/2), height: 565)
                }else{
                    return CGSize(width:((width - 30)/2), height: 455)
                }
            }else{
                if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                    return CGSize(width: ((width - 15)/2), height: 365)
                }else{
                    return CGSize(width: ((width - 15)/2), height: 255)
                }
            }
            
        }else{
            return CGSize(width: self.FavCollectionView.frame.width, height: self.FavCollectionView.frame.height)
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if (UIDevice.current.userInterfaceIdiom == .pad){
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }else{
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
    }
}
