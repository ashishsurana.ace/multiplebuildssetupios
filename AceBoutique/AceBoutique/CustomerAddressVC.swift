//
//  CustomerAddressVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 01/04/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics

class CustomerAddressVC: UIViewController, UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource  {
    
    
    
    @IBOutlet weak var lblSaveAddress: UILabel!
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtAddress1: UITextField!
    @IBOutlet weak var txtAddress2: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtZipcode: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btnSaveAddress: UIControl!
    
    @IBOutlet weak var stateBaseView: UIView!
    @IBOutlet weak var heightStateConstant: NSLayoutConstraint!
        
    weak var pickerView: UIPickerView?
    var arrProvince = [State]()
    let country_Id: Int = 0
    var isStateAvailable: Bool! = false
    var arrAllCountries: countries!
    
    //MARK: - Picker View Delegates
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if self.txtCountry.isFirstResponder{
            if self.arrAllCountries.count != 0 {
                return self.arrAllCountries.count
            }else{
                return 0
            }
        }else if self.txtState.isFirstResponder{
            if self.arrProvince.count != 0 {
            return self.arrProvince.count
            }else {
                return 0
            }
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if self.txtCountry.isFirstResponder {
            return self.arrAllCountries[row].name
        }else if self.txtState.isFirstResponder {
            if let name = self.arrProvince[row].name {
                return name
            }else{
                return ""
            }
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if self.txtCountry.isFirstResponder{
            self.txtCountry.text! = self.arrAllCountries[row].name!
            self.txtState.text! = ""
//            let itemId = self.arrAllCountries.countries[row].id
            self.arrProvince.removeAll()
            self.arrProvince = self.arrAllCountries[row].states!
                    if self.arrProvince.count == 0 {
                        self.stateBaseView.isHidden = true
                        self.heightStateConstant.constant = 0.0
                        self.isStateAvailable = true
                    }else {
                        self.stateBaseView.isHidden = false
                        self.heightStateConstant.constant = 48.0
                    }
            
        }else if self.txtState.isFirstResponder{
            if self.arrProvince.count != 0 {
                self.txtState.text = self.arrProvince[row].name
                self.isStateAvailable = true
            }
        }else {
            
        }
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblScreenTitle.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.txtEmail.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        self.txtFirstName.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        self.txtLastName.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        self.txtAddress1.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        self.txtAddress2.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        self.txtCity.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        self.txtCountry.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        self.txtState.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        self.txtZipcode.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        self.txtPhone.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        self.lblSaveAddress.font = UIFont.appBoldFontWith(size: AppFontSize.size17)
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        
        self.txtCountry.inputView = pickerView
        self.txtState.inputView = pickerView
        
        self.pickerView = pickerView
    }
    
    
//    private func loadJson(fromURLString urlString: String,
//                          completion: @escaping (Result<Data, Error>) -> Void) {
//        if let url = URL(string: urlString) {
//            let request = NSMutableURLRequest(url: url)
//            request.setValue(adminToken, forHTTPHeaderField: "X-Shopify-Access-Token")
//            request.httpMethod = "GET"
//            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//            let session = URLSession.shared
//            appDelegateShared.showHud()
//            let urlSession = session.dataTask(with: request as URLRequest) { (data, response, error) in
//                if let error = error {
//                    appDelegateShared.dismissHud()
//                    completion(.failure(error))
//                }
//
//                if let data = data {
//                    appDelegateShared.dismissHud()
//                    completion(.success(data))
//                }
//            }
//
//            urlSession.resume()
//        }
//    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        FirebaseEventsLog.request.trackScreen(screenName: "Add new address", className: "CustomerAddressVC")
        self.btnSaveAddress.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblSaveAddress.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
//        self.arrAllCountries.removeAll()
        self.arrAllCountries = arrCountries
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView?.reloadAllComponents()
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSaveAddressTapped(_ sender: Any) {
        
        if self.txtEmail.text! != "" &&  self.txtFirstName.text! != "" && self.txtLastName.text! != "" &&
            self.txtAddress1.text! != "" && self.txtAddress2.text! != "" && self.txtCity.text! != "" && self.txtCountry.text! != "" && self.isStateAvailable == true &&  self.txtZipcode.text! != "" && self.txtPhone.text! != ""{
            
            let stringValue:String? = self.validationForm()
            
            if ((stringValue?.length)! > 0){
                
                let alert = UIAlertController(title: "", message: stringValue!, preferredStyle:.alert)
                
                alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                DispatchQueue.main.async{
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }else{
                
                if (self.txtPhone.text!.length == 10) {
                    
                    let accessToken =  UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken)
                    
                    let input = Storefront.MailingAddressInput.create(
                        address1:  .value("\(self.txtAddress1.text!)"),
                        address2:  .value("\(self.txtAddress2.text!)"),
                        city:      .value("\(self.txtCity.text!)"),
                        country:   .value("\(self.txtCountry.text!)"),
                        firstName: .value("\(self.txtFirstName.text!)"),
                        lastName:  .value("\(self.txtLastName.text!)"),
                        phone:     .value("\(self.txtPhone.text!)"),
                        province:  .value("\(self.txtState.text!)"),
                        zip:       .value("\(self.txtZipcode.text!)")
                    )
                    
                    let mutation = Storefront.buildMutation { $0
                        .customerAddressCreate(customerAccessToken: accessToken!, address: input) { $0
                            .customerAddress { $0
                                .id()
                                .firstName()
                                .lastName()
                                .phone()
                                .address1()
                                .address2()
                                .city()
                                .province()
                                .country()
                                .zip()
                            }
                            .customerUserErrors { $0
                                .code()
                                .field()
                                .message()
                            }
                        }
                    }
                    
                    let task  =  client.mutateGraphWith(mutation) { (mutation, error) in

                        print("Registration error:",error.debugDescription)

                        if let address = mutation?.customerAddressCreate?.customerAddress {

                            GlobalObjects.CustomerAddresses.append(CustomerAddress(id: address.id.rawValue, firstName: address.firstName!, lastName: address.lastName!, address1: address.address1!, address2: address.address2!, phone: address.phone, city: address.city!, state: address.province!, country: address.country!, zipcode: address.zip!))
                            self.dismiss(animated: true, completion: nil)

                        } else {

                            let errors = mutation?.customerAddressCreate?.customerUserErrors ?? []
                            print("Failed to add customer address: \(errors)")
                            let errorMessage = errors[0].message
                            let alert = UIAlertController(title: "", message: errorMessage, preferredStyle:.alert)

                            alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                            DispatchQueue.main.async{
                                self.present(alert, animated: true, completion: nil)

                            }

                        }
                    }
                    
                    task.resume()
                    
                }else{
                    self.showValidationAlert(message: "invalidPhoneValidation".localized)
                }
            }
            
        }else if self.txtEmail.text! == "" {
            self.showValidationAlert(message: "nilEmailValidation".localized)
        }else if self.txtFirstName.text! == "" {
            self.showValidationAlert(message: "nilFirstnameValidation".localized)
        }else if self.txtLastName.text! == "" {
            self.showValidationAlert(message: "nilLastnameValidation".localized)
        }else if self.txtAddress1.text! == "" {
            self.showValidationAlert(message: "noAddressline1Validation".localized)
        }else if self.txtAddress2.text! == "" {
            self.showValidationAlert(message: "noAddressline2Validation".localized)
        }else if self.txtCity.text! == "" {
            self.showValidationAlert(message: "noCityValidation".localized)
        }else if self.txtCountry.text! == "" {
            self.showValidationAlert(message: "noCountryValidation".localized)
        }else if self.isStateAvailable == false {
            self.showValidationAlert(message: "noStateValidation".localized)
        }else if self.txtZipcode.text! == "" {
            self.showValidationAlert(message: "noZipcodeValidation".localized)
        }else if self.txtPhone.text! == "" {
            self.showValidationAlert(message: "noPhoneValidation".localized)
        }
    }
    
    func validationForm() -> String
    {
        var errorMessage:String? = ""
        
        self.txtEmail.text = appDelegateShared.TrimString(string: self.txtEmail.text!)
        
        if appDelegateShared.isValidURL(self.txtEmail.text!) == false {
            errorMessage = "emailValidation".localized
        }
        
        return errorMessage!
    }
    
    func showValidationAlert(message: String){
        
        let alert = UIAlertController(title: "", message: message, preferredStyle:.alert)
        
        alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
        DispatchQueue.main.async {
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
