////
////  Navigator.swift
////  AceBoutique
////
////  Created by Aashiyana Shaikh on 22/06/21.
////
//
//import Foundation
//import UIKit
//
//struct Navigator {
//
//    func getDestination(for url: URL) -> UIViewController? {
//
//        let storyboard = UIStoryboard(name: "Main", bundle: .main)
//
//        let rootViewController = storyboard.instantiateInitialViewController() as? RootViewController
//
//        let destination = Destination(for: url)
//
//        switch destination {
//
//        case .collections: return rootViewController
//
//        case .productDetail:
//
//            let navController = tabBarController?.viewControllers?[1] as? UINavigationController
//
//            guard let userDetailsVC = storyboard.instantiateViewController(withIdentifier: "UserDetailsVC") as? UserDetailsViewController else { return nil }
//
//            navController?.pushViewController(userDetailsVC, animated: false)
//
//            return tabBarController
//
//        case .safari: return nil
//
//        }
//     }
//
//    enum Destination {
//
//        case collections
//
//        case productDetail
//
//        case safari
//
//        init(for url: URL) {
//
//            print(url.lastPathComponent)
//
//            if url.lastPathComponent == "product_id" {
//
//                self = .productDetail
//
//            } else if url.lastPathComponent == "collection_id"{
//
//                self = .collections
//
//            } else {
//
//                self = .safari
//            }
//        }
//    }
//}
