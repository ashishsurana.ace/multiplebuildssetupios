//
//  CheckoutUserSessionCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 25/03/21.
//

import UIKit

class CheckoutUserSessionCell: UITableViewCell {
    
    @IBOutlet weak var lblEmail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblEmail.font = UIFont.appRegularFontWith(size: AppFontSize.size13)
        if UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail) != nil {
            
            let email = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
            
            let signedas = "signedas".localized
            self.lblEmail.text! = "\(signedas) \(String(describing: email!))"
            
            let emailString = self.lblEmail.text!
            let boldString = "\(String(describing: email!))"
            
            
            let formattedText = String.format(strings: [boldString],
                                              boldFont: UIFont.boldSystemFont(ofSize: 14),
                                              boldColor: UIColor.black,
                                              inString: emailString,
                                              font: UIFont.systemFont(ofSize: 14),
                                              color: UIColor.darkGray)
            
            self.lblEmail.numberOfLines = 0
            self.lblEmail.attributedText = formattedText
            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
