//
//  checkoutLoginSessionCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 24/03/21.
//

import UIKit

class checkoutLoginSessionCell: UITableViewCell {
    
    @IBOutlet weak var lblAlreadyHaveAccount: UILabel!
    @IBOutlet weak var lblSignedAccount: UILabel!
    
    @IBOutlet weak var btnCheckoutAsGuest: UIControl!
    @IBOutlet weak var btnSignIn: UIControl!
    @IBOutlet weak var heightbtnCheckoutAsGuestConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblAlreadyHaveAccount.font = UIFont.appRegularFontWith(size: AppFontSize.size13)
        self.lblSignedAccount.font = UIFont.appMediumFontWith(size: AppFontSize.size13)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
