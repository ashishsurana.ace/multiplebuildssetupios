//
//  shippingAddressCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 24/03/21.
//

import UIKit

class shippingAddressCell: UITableViewCell {
    
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var addressBaseView: UIControl!
    @IBOutlet weak var heightAddressViewConstant: NSLayoutConstraint!
    
    @IBOutlet weak var lblShippingLabel: UILabel!
    @IBOutlet weak var fullAddressBaseView: UIControl!
    @IBOutlet weak var heightFullAddressConstant: NSLayoutConstraint!
    @IBOutlet weak var lblFullAddress: UILabel!
    @IBOutlet weak var btnAddAddress: UIButton!
    
    @IBOutlet weak var btnViewAllAddresses: UIControl!
    @IBOutlet weak var shippingBaseView: UIControl!
    @IBOutlet weak var heightShippingConstant: NSLayoutConstraint!
    @IBOutlet weak var lblShippingMethodName: UILabel!
    
    @IBOutlet weak var lblAddShippingAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblShippingLabel.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        self.lblAddShippingAddress.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblFullAddress.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.btnAddAddress.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.btnAddAddress.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)), for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
