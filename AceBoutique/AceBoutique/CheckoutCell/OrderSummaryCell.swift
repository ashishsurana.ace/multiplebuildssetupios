//
//  OrderSummaryCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 24/03/21.
//

import UIKit

class OrderSummaryCell: UITableViewCell {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var btnAddNotes: UIControl!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var btnViewNotes: UIControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
//        btnAddNotes.clipsToBounds = true
//        btnAddNotes.layer.cornerRadius = 5
//        btnAddNotes.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

