//
//  MyCartVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/03/21.
//

import UIKit
import SQLite3
import MobileBuySDK
import SafariServices
import FirebaseAnalytics
import Firebase


class CartNavigationController: UINavigationController {
    // Exists to provide type-safe instantiation from storyboard
}

class MyCartVC: UIViewController {
    
    @IBOutlet weak var ImgBackButton: UIImageView!
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var tblCartItemListing: UITableView!
    @IBOutlet weak var heightCartSummaryView: NSLayoutConstraint!
    @IBOutlet weak var SummaryBaseView: UIView!
    @IBOutlet weak var lblTotalItems: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var btnCheckout: UIControl!
    @IBOutlet weak var lblCheckout: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    
    var arrCartItems = [MyCartItems]()
    var Cart_DB = CartQueries()
    var totalQty: Int! = 0
    var totalPrice: Float! = 0.0
    var db: OpaquePointer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tblCartItemListing.tableFooterView = UIView()
        self.tblCartItemListing.register(UINib(nibName: "EmptyCartCell", bundle: nil), forCellReuseIdentifier: "EmptyCartCell")
        self.tblCartItemListing.register(UINib(nibName: "CartItemListingCell", bundle: nil), forCellReuseIdentifier: "CartItemListingCell")
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWhenAPNSCall(_:)), name: NSNotification.Name(rawValue: "reloadTable"), object: nil)

        self.getAllCartItems()
    }

    @objc func reloadWhenAPNSCall(_ notification: NSNotification) {
        
        self.tblCartItemListing.reloadData()
        self.view.layoutIfNeeded()
        
    }
    
    func getAllCartItems(){
        
        self.arrCartItems.removeAll()
        self.totalQty = 0
        self.totalPrice = 0.0
        
        self.arrCartItems = Cart_DB.GetAllCartItems()
        self.arrCartItems = self.arrCartItems.sorted { $0.id  > $1.id }
        
        if self.arrCartItems.count != 0 {
            var currencyCode = String()
            for item in self.arrCartItems {
                
                self.totalQty += item.quantity
                let price = ((item.price as NSString).floatValue)*Float(item.quantity)
                self.totalPrice += price
                currencyCode = item.currency_code
            }
            
            if self.totalQty > 1 {
                self.lblItems.text! = "items".localized
            }else {
                self.lblItems.text! = "item".localized
            }
            
            self.lblTotalItems.text! = String(totalQty!)
            let locale = Locale.getSymbolForCurrencyCode(code: currencyCode)
            self.lblTotalPrice.text! = "\(locale)\(String(format: "%.2f", totalPrice!))"
            
            var cartItems = [String: Any]()
            var addedItems = [Any]()
            for items in self.arrCartItems {
                let itemDetails: [String: Any] = [
                    AnalyticsParameterItemID: items.id,
                    AnalyticsParameterItemName: items.title,
                    AnalyticsParameterItemVariant: items.variant_value,
                    AnalyticsParameterQuantity : items.quantity,
                    AnalyticsParameterPrice: items.price,
                    AnalyticsParameterCurrency: items.currency_code
                    
                ]
                addedItems.append(itemDetails)
            }
            cartItems = [ AnalyticsParameterCurrency: currencyCode,
                          AnalyticsParameterValue: self.totalPrice,
                          AnalyticsParameterItems: addedItems]
            FirebaseEventsLog.request.logViewCartEvent(items: cartItems)
            print(self.arrCartItems.count)
            
        }else{
            
            self.setCartLayout()
        }
        
        self.tblCartItemListing.reloadData()
        
    }
    
    
    @IBAction func btnBackTapped(_ sender: Any) {
        if isFromNotification == true {
            isFromNotification = false
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let rootView = storyBoard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
            UIApplication.shared.windows.first?.rootViewController = rootView
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }else{
        self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        if CheckNetwork.isConnectedToNetwork() {
        FirebaseEventsLog.request.trackScreen(screenName: "Cart", className: "MyCartVC")
        self.lblScreenTitle.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.lblTotalItems.font = UIFont.appMediumFontWith(size: AppFontSize.size19)
        self.lblItems.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.lblTotal.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.lblTotalPrice.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.lblCheckout.font = UIFont.appMediumFontWith(size: AppFontSize.size19)
        
        self.navigationController?.isNavigationBarHidden = true
        self.btnCheckout.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblCheckout.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
        self.setCartLayout()
//        }
    }
    
    func setCartLayout(){
        
        if self.arrCartItems.count == 0 {
            
            self.heightCartSummaryView.constant = 0.0
            self.SummaryBaseView.isHidden = true
            self.tblCartItemListing.isScrollEnabled = false
            
        }else{
            
            self.heightCartSummaryView.constant = 104
            self.SummaryBaseView.isHidden = false
            self.tblCartItemListing.isScrollEnabled = true
        }
        
        
        self.tblCartItemListing.reloadData()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func startShoppingAction(sender: UIButton) {
        
        if let parentVC = self.parent as? UINavigationController {
            if (parentVC.parent as? RootViewController) != nil {
                let baseVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardView")
                let contentViewController = UINavigationController(rootViewController: baseVC!)
                self.sideMenuViewController?.setContentViewController(contentViewController, animated: true)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    @objc func addMoreQuantity(sender: UIButton) {
        print(sender.tag)
        
        let item_id = self.arrCartItems[sender.tag].variant_id
        let item_price = (self.arrCartItems[sender.tag].price as NSString).floatValue
        var item_Qty = self.arrCartItems[sender.tag].quantity
        item_Qty += 1
        //        item_price = (item_price)*Float(item_Qty)
        Cart_DB.updateCartItem(item_Qty, price: String(item_price), variant_id: item_id)
        self.getAllCartItems()
        
    }
    
    @objc func decreaseQuantity(sender: UIButton) {
        print(sender.tag)
        
        let item_id = self.arrCartItems[sender.tag].variant_id
        let item_price = (self.arrCartItems[sender.tag].price as NSString).floatValue
        var item_Qty = self.arrCartItems[sender.tag].quantity
        item_Qty -= 1
        if item_Qty != 0 {
            Cart_DB.updateCartItem(item_Qty, price: String(item_price), variant_id: item_id)
        }else{
            Cart_DB.deleteCartItem(variant_Id: item_id)
            Messaging.messaging().unsubscribe(fromTopic: "cart")
        }
        
        self.getAllCartItems()
    }
    
    @IBAction func btnCheckoutTapped(_ sender: Any) {
        
        //        let myAlert = self.storyboard!.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
        //        self.present(myAlert, animated: true, completion: nil)
        
        if CHECKOUT_CUSTOMER_ACCOUNTS_STATUS.lowercased() == customerAccountStatus.disabled.rawValue.lowercased() {
            
            var cartItems = [Storefront.CheckoutLineItemInput]()
            for item in self.arrCartItems {
                cartItems.append(Storefront.CheckoutLineItemInput.create(quantity: Int32(item.quantity), variantId: GraphQL.ID(rawValue: "\(String(describing: item.variant_id))")))
            }
            
            var customAttributes = [Storefront.AttributeInput]()
            if UserDefaults.standard.object(forKey: MyUserDefaults.firebaseToken) != nil {
            let fcmToken = UserDefaults.standard.string(forKey: MyUserDefaults.firebaseToken)!
            customAttributes.append(Storefront.AttributeInput(key: "pid", value: "2"))
            customAttributes.append(Storefront.AttributeInput(key: "did", value: fcmToken))
            }
            let checkout = Storefront.CheckoutCreateInput.create(
                email: .value("guest@gmail.com"),
                lineItems: .value(cartItems),
                customAttributes: .value(customAttributes)
                
            )
            
            let mutation = Storefront.buildMutation { $0
                .checkoutCreate(input: checkout) { $0
                    .checkout { $0
                        .id()
                        .webUrl()
                    }
                    .checkoutUserErrors { $0
                        .code()
                        .field()
                        .message()
                        
                    }
                }
            }
            
            let task = client.mutateGraphWith(mutation) { response, error in
                
                var message: String
                let error = response?.checkoutCreate?.checkoutUserErrors ?? []
                if error.count != 0 {
                    self.view.isUserInteractionEnabled = true
                    message = error[0].message
                    let alert = UIAlertController(title: "", message: message, preferredStyle:.alert)
                    
                    alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                    DispatchQueue.main.async{
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }else {
                    
                    let checkout = response?.checkoutCreate?.checkout
                    self.openSafariFor(checkout)
                    
                }
                
            }
            task.resume()
            
        }else{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let detailVC = storyBoard.instantiateViewController(withIdentifier: "CheckoutNavController") as? CheckoutNavController {
                self.navigationController?.present(detailVC, animated: true, completion: nil)
            }
        }
        
    }
    
    func openSafariFor(_ checkout: Storefront.Checkout?) {
        
        let accessToken =  UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken)
        let webURL = checkout!.webUrl as URL
        let checkoutId = checkout!.id.rawValue

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let detailVC = storyBoard.instantiateViewController(withIdentifier: "WebViewCheckoutVC") as? WebViewCheckoutVC {
            detailVC.accessToken = accessToken ?? ""
            detailVC.url = webURL
            detailVC.checkoutId = checkoutId
            self.present(detailVC, animated: true, completion: nil)
        }
        
    }
    
}

extension MyCartVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrCartItems.count != 0 {
            return self.arrCartItems.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  self.arrCartItems.count != 0 {
            
            let cell = tblCartItemListing.dequeueReusableCell(withIdentifier:"CartItemListingCell", for: indexPath) as! CartItemListingCell
            
            cell.lblProductName.text! = self.arrCartItems[indexPath.row].title
            if self.arrCartItems[indexPath.row].variant_type != "" {
                cell.lblVarianttype.text! = "\(self.arrCartItems[indexPath.row].variant_type):"
            }else{
                cell.lblVarianttype.text! = ""
            }
            cell.lblVariantSizeColor.text! = self.arrCartItems[indexPath.row].variant_value
            cell.lblProductQuantity.text! = String(self.arrCartItems[indexPath.row].quantity)
            let locale = Locale.getSymbolForCurrencyCode(code: self.arrCartItems[indexPath.row].currency_code)
            let price = (self.arrCartItems[indexPath.row].price as NSString).floatValue
            
            cell.lblProductPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
            
            cell.btnRemoveProduct.addTarget(self, action: #selector(decreaseQuantity(sender:)), for: .touchUpInside)
            cell.btnRemoveProduct.tag = indexPath.row
            
            cell.btnAddMoreProduct.addTarget(self, action: #selector(addMoreQuantity(sender:)), for: .touchUpInside)
            cell.btnAddMoreProduct.tag = indexPath.row
            
            let imgURL = URL(string: self.arrCartItems[indexPath.row].Img_url)
                        
            cell.ImgProduct.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                if image == nil {
                    
                }else{
                    
                }
            })
            
            if (UIDevice.current.userInterfaceIdiom == .pad){
                if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                    cell.ImgWidthConstraint.constant = 142
                    cell.ImgHeightConstraint.constant = 180
                }else{
                    cell.ImgWidthConstraint.constant = 150
                    cell.ImgHeightConstraint.constant = 150
                }
            }else{
                if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                    cell.ImgWidthConstraint.constant = 112
                    cell.ImgHeightConstraint.constant = 160
                }else{
                    cell.ImgWidthConstraint.constant = 120
                    cell.ImgHeightConstraint.constant = 120
                }
            }
            
            return cell
            
        }else{
            
            let cell = tblCartItemListing.dequeueReusableCell(withIdentifier:"EmptyCartCell", for: indexPath) as! EmptyCartCell
            cell.btnStartShoppingTapped.addTarget(self, action: #selector(startShoppingAction(sender:)), for: .touchUpInside)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.arrCartItems.count != 0 {
          
            if (UIDevice.current.userInterfaceIdiom == .pad){
                if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                    return 200
                }else{
                    return 180
                }
            }else{
                if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                    return 180
                }else{
                    return 140
                }
            }
            
        }else{
            return self.tblCartItemListing.frame.height
            
        }
    }
    
}

public extension UIAlertController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindow.Level.alert + 1  // Swift 3-4: UIWindowLevelAlert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}
