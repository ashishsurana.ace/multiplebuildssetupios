//
//  EmptyCartCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/03/21.
//

import UIKit

class EmptyCartCell: UITableViewCell {
    
    @IBOutlet weak var lblEmptyCartMsg: UILabel!
    @IBOutlet weak var btnStartShoppingTapped: UIControl!
    @IBOutlet weak var lblStartShopping: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.lblEmptyCartMsg.font = UIFont.appMediumFontWith(size: AppFontSize.size19)
        self.lblStartShopping.font = UIFont.appBoldFontWith(size: AppFontSize.size17)
        self.btnStartShoppingTapped.backgroundColor =  UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblStartShopping.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
