//
//  CartItemListingCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/03/21.
//

import UIKit

class CartItemListingCell: UITableViewCell {
    
    
    @IBOutlet weak var ImgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblVariantSizeColor: UILabel!
    @IBOutlet weak var lblVarianttype: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var btnAddMoreProduct: UIControl!
    @IBOutlet weak var btnRemoveProduct: UIControl!
    @IBOutlet weak var lblProductQuantity: UILabel!
    @IBOutlet weak var ImgWidthConstraint: NSLayoutConstraint!
    //iphone = 112, ipad = 142
    
    @IBOutlet weak var ImgHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblProductPrice.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        self.lblVarianttype.font = UIFont.appRegularFontWith(size: AppFontSize.size15)
        self.lblVariantSizeColor.font = UIFont.appBoldFontWith(size: AppFontSize.size15)
        self.lblProductPrice.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        self.lblProductQuantity.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
