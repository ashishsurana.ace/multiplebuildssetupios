//
//  OrderHistoryListingVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 13/04/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics

class OrderHistoryListingVC: UIViewController {
    
    
    @IBOutlet weak var OrderHistoryCollection: UICollectionView!
    
    
    var Orders: Storefront.OrderConnection!
    var Cursor: String?
    var ImgHeight: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.OrderHistoryCollection.register(UINib(nibName: "OrderHistoryListCell", bundle: nil), forCellWithReuseIdentifier: "OrderHistoryListCell")
        self.OrderHistoryCollection.register(UINib(nibName: "EmptyShowCollectionCell", bundle: nil), forCellWithReuseIdentifier: "EmptyShowCollectionCell")
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPad_Grid_Portrait_height
            }else{
                self.ImgHeight = GlobalObjects.iPad_Grid_Square_height
            }
        }else{
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPhone_Grid_Portrait_height
            }else{
                self.ImgHeight = GlobalObjects.iPhone_Grid_Square_height
            }
        }
        
        self.OrderHistoryCollection.isHidden = true
        if let accessToken = UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken) {
            self.Cursor = nil
            GlobalObjects.OrderLists.removeAll()
            self.getOrderHistory(accessToken: accessToken, cursor: self.Cursor, orders: 10, discounts: 10, lineItems: 10, ImgHeight: Int32(self.ImgHeight))
            //            self.OrderHistoryCollection.reloadData()
        }
        
        self.OrderHistoryCollection.cr.addHeadRefresh(animator:FastAnimator() as CRRefreshProtocol) {
            [weak self] in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                
                if let accessToken = UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken) {
                    self?.Cursor = nil
                    GlobalObjects.OrderLists.removeAll()
                    self?.getOrderHistory(accessToken: accessToken, cursor: self?.Cursor, orders: 10, discounts: 10, lineItems: 10, ImgHeight: Int32((self?.ImgHeight)!))
                }
                
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        FirebaseEventsLog.request.trackScreen(screenName: "Order list", className: "OrderHistoryListingVC")
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func getOrderHistory(accessToken: String, cursor: String?, orders:Int32, discounts:Int32, lineItems:Int32, ImgHeight: Int32) {
        
        ShopifyOrderHistoryList.shared.orderHistoryList(accessToken: accessToken, cursor: cursor, orders: 10, discounts: 10, lineItems: 10, ImgHeight: Int32(self.ImgHeight)) { result, error in
            if let result = result{
                self.Orders = result
                let orders = result.edges.map {$0.node}
                
                orders.forEach { items in
                    
                    let Id = items.id.rawValue
                    let orderNumber = items.name
                    let subtotal = NSDecimalNumber(decimal: items.originalTotalPrice.amount).stringValue
                    let currencyCode = items.originalTotalPrice.currencyCode.rawValue
                    let orderDate = items.processedAt
                    let linelitems1 = items.lineItems.edges.map {$0.node}
                    var orderItems: Int = 0
                    linelitems1.forEach { items in
                        orderItems += Int(items.quantity)
                    }
                    
                    let ImgUrl = linelitems1[0].variant?.image?.transformedSrc.absoluteURL
                    
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "MMM dd, yyyy"
                    inputFormatter.locale = appLocale
                    
                    let showDate = inputFormatter.string(from: orderDate)
                    
                    GlobalObjects.OrderLists.append(OrderHistory(id: Id, orderNumber: orderNumber, total: subtotal, orderDate: showDate, totalItems: orderItems, ImgUrl: ImgUrl!, currencyCode: currencyCode))
                    
                }
                
                DispatchQueue.main.async {
                    self.OrderHistoryCollection.cr.endHeaderRefresh()
                    self.OrderHistoryCollection.cr.resetNoMore()
                    self.OrderHistoryCollection.isHidden = false
                    self.OrderHistoryCollection.reloadData()
                }
                
            }else{
                self.Showalert1(message: error!)
            }
        }
    }
    
    @objc func redirectToHome(){
        
        let baseVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardView")
        let contentViewController = UINavigationController(rootViewController: baseVC!)
        self.sideMenuViewController?.setContentViewController(contentViewController, animated: true)
        self.sideMenuViewController?.hideMenuViewController()
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension OrderHistoryListingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if GlobalObjects.OrderLists.count != 0 {
            return GlobalObjects.OrderLists.count
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if GlobalObjects.OrderLists.count != 0 {
            let cell = OrderHistoryCollection.dequeueReusableCell(withReuseIdentifier:"OrderHistoryListCell", for: indexPath) as! OrderHistoryListCell
            
            cell.lblOrderNumber.text! = "\(GlobalObjects.OrderLists[indexPath.row].orderNumber)"
            
            let totalItems = Int(GlobalObjects.OrderLists[indexPath.row].totalItems)
            if totalItems > 1 {
                cell.lblItems.text! = "items".localized
            }else{
                cell.lblItems.text! = "item".localized
            }
            
            cell.lblTotalText.text = "total".localized
            cell.lblItemCounts.text! = "\(GlobalObjects.OrderLists[indexPath.row].totalItems)"
            
            let price = (GlobalObjects.OrderLists[indexPath.row].total as NSString).floatValue
            let currency = GlobalObjects.OrderLists[indexPath.row].currencyCode
            let locale = Locale.getSymbolForCurrencyCode(code: currency)
            
            cell.lblTotalPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
            let strPrderplace = "orderplace".localized
            cell.lblOrderPlacedDate.text! = "\(strPrderplace) \(GlobalObjects.OrderLists[indexPath.row].orderDate)"
            
            let imgURL = GlobalObjects.OrderLists[indexPath.row].ImgUrl
            
            cell.ImgView.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                // Your code inside completion block
                if image == nil {
                    
                }
            })
            
            return cell
            
        }else{
            
            let cell1 = OrderHistoryCollection.dequeueReusableCell(withReuseIdentifier:"EmptyShowCollectionCell", for: indexPath) as! EmptyShowCollectionCell
            cell1.lblShowMessage.text! = "noordermsg".localized
            cell1.lblButtonText.text! = "btnstartshopping".localized
            cell1.btnEmptyAction.addTarget(self, action: #selector(self.redirectToHome), for: .touchUpInside)
            return cell1
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if GlobalObjects.OrderLists.count != 0 {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let detailVC = storyBoard.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
            detailVC.orderId = GlobalObjects.OrderLists[indexPath.row].id
            self.present(detailVC, animated: true, completion: nil)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.OrderHistoryCollection.frame.width
        
        //        print((width - 15)/2)
        if GlobalObjects.OrderLists.count != 0 {
            return CGSize(width: width , height: 155)
        }else{
            return CGSize(width: width , height: self.OrderHistoryCollection.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if GlobalObjects.OrderLists.count != 0 {
            self.Cursor = self.Orders?.edges.last?.cursor
            let hasNextPage =  self.Orders?.pageInfo.hasNextPage
            let accessToken = UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken)
            
            if hasNextPage! && indexPath.row == GlobalObjects.OrderLists.count - 1 {
                self.getOrderHistory(accessToken: accessToken!, cursor: self.Cursor, orders: 10, discounts: 10, lineItems: 10, ImgHeight: Int32(self.ImgHeight))
                
            }
        }
    }
}

