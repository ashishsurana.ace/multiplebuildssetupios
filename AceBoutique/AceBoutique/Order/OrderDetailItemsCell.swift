//
//  OrderDetailItemsCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 15/04/21.
//

import UIKit

class OrderDetailItemsCell: UICollectionViewCell {
    
    @IBOutlet weak var ImgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblVarianttype: UILabel!
    @IBOutlet weak var lblVariantSizeColor: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblProductQuantity: UILabel!
    @IBOutlet weak var lblQtyText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblProductName.font = UIFont.appMediumFontWith(size: AppFontSize.size15)
        self.lblVarianttype.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblVariantSizeColor.font = UIFont.appMediumFontWith(size: AppFontSize.size14)
        self.lblProductPrice.font = UIFont.appRegularFontWith(size: AppFontSize.size15)
        self.lblQtyText.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblProductQuantity.font = UIFont.appRegularFontWith(size: AppFontSize.size15)
    }
    
}
