//
//  ShopifyOrderHistoryList.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 14/06/21.
//

import Foundation
import MobileBuySDK


class ShopifyOrderHistoryList {
    
    static let shared = ShopifyOrderHistoryList()
    func orderHistoryList(accessToken: String, cursor: String?, orders:Int32, discounts:Int32, lineItems:Int32, ImgHeight: Int32, completion: @escaping (Storefront.OrderConnection?, String?) -> Void){
        
        QueryOrderHistoryList.shared.orderHistoryList(accessToken: accessToken, cursor: cursor, orders: orders, discounts: discounts, lineItems: lineItems, ImgHeight: ImgHeight) { orders, error in
            
            if let orders = orders {
                
                completion(orders, nil)
                
            }else{
                
                let error = "errorMsg".localized
                completion(nil, error)
            }
        }
    }
}
