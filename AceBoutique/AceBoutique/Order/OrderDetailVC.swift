//
//  OrderDetailVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 13/04/21.
//

import UIKit
import MobileBuySDK
import Foundation
import FirebaseAnalytics

class OrderDetailVC: UIViewController {
    
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var lblTotalTExt: UILabel!
    @IBOutlet weak var lblDiscountText: UILabel!
    @IBOutlet weak var lblTaxText: UILabel!
    @IBOutlet weak var lblShippingText: UILabel!
    @IBOutlet weak var lblSubtotalTExt: UILabel!
    @IBOutlet weak var lblOrderText: UILabel!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblOrderPlacedDate: UILabel!
    @IBOutlet weak var lblSubtotalPrice: UILabel!
    @IBOutlet weak var lblShippingPrice: UILabel!
    @IBOutlet weak var lblTaxPrice: UILabel!
    @IBOutlet weak var lblDsicountPrice: UILabel!
    @IBOutlet weak var lblDiscountCode: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblItemCounts: UILabel!
    
    @IBOutlet weak var backViewOrderSummary: UIView!
    @IBOutlet weak var heightOrderSumConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionItems: UICollectionView!
    
    var orderId:String!
    var OrderDetail: Storefront.Order!
    var lineItems = [OrderLineItems]()
    var ImgHeight: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.fontSetup()
        self.collectionItems.register(UINib(nibName: "OrderDetailItemsCell", bundle: nil), forCellWithReuseIdentifier: "OrderDetailItemsCell")
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPad_Grid_Portrait_height
            }else{
                self.ImgHeight = GlobalObjects.iPad_Grid_Square_height
            }
        }else{
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPhone_Grid_Portrait_height
            }else{
                self.ImgHeight = GlobalObjects.iPhone_Grid_Square_height
            }
        }
        
    }
    
    func fontSetup(){
        
        self.lblScreenTitle.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.lblOrderText.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblOrderNumber.font = UIFont.appMediumFontWith(size: AppFontSize.size16)
        self.lblOrderPlacedDate.font = UIFont.appItalicFontWith(size: AppFontSize.size14)
        self.lblSubtotalTExt.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblShippingText.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblShippingPrice.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblTaxText.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblDiscountText.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        
        self.lblDiscountCode.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblSubtotalPrice.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblShippingPrice.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblTaxPrice.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblDsicountPrice.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblDsicountPrice.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblDiscountCode.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblTotalAmount.font = UIFont.appMediumFontWith(size: AppFontSize.size14)
        self.lblTotalTExt.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblItems.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblItemCounts.font = UIFont.appMediumFontWith(size: AppFontSize.size14)
        
        self.lblDiscountCode.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        FirebaseEventsLog.request.trackScreen(screenName: "Order detail", className: "OrderDetailVC")
        if let accessToken = UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken) {
            
            self.getDataFromApollo(id: self.orderId!)
            self.getOrderDetails(orderId: self.orderId!, lineItems: 50, ImgHeight: Int32(self.ImgHeight))
            
        }
    }
    
    func getDataFromApollo(id:String) {
        localeId = GetCurrentLanguage()
        Network.request.apollo.fetch(query: OrderDetailsByIdQuery(id: GraphQL.ID(rawValue:"\(String(describing:id))").rawValue)) { result in
            print(result)
            switch result {
            case .success(let graphQLResult):
                guard let data = try? result.get().data else { return }
                
                if graphQLResult.data != nil {
                    if graphQLResult.data?.node?.asOrder != nil {
                        let orderData = graphQLResult.data!.node!.asOrder!
                        
                        if orderData.totalShippingPriceSet != nil {
                            if (orderData.totalShippingPriceSet.presentmentMoney.amount as NSString).floatValue <= 0 {
                                self.lblShippingPrice.text! = "free".localized
                                self.lblShippingPrice.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
                            }else{
                                
                                let locale = Locale.getSymbolForCurrencyCode(code: orderData.totalShippingPriceSet.presentmentMoney.currencyCode.rawValue)
                                let price = (orderData.totalShippingPriceSet.presentmentMoney.amount as NSString).floatValue
                                self.lblShippingPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
                                self.lblShippingPrice.textColor = UIColor.black
                            }
                        }
                        if (orderData.totalTaxSet!.presentmentMoney.amount as NSString).floatValue <=
                            0 {
                            
                            self.lblTaxPrice.text! = "free".localized
                            
                        }else{
                            
                            let locale = Locale.getSymbolForCurrencyCode(code: orderData.totalTaxSet!.presentmentMoney.currencyCode.rawValue)
                            let price = (orderData.totalTaxSet!.presentmentMoney.amount as NSString).floatValue
                            self.lblTaxPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
                            
                        }
                        
                        if (orderData.discountCode != nil) {
                            self.lblDiscountCode.text = "\(String(describing: orderData.discountCode!))"
                        }else{
                            
                            self.lblDiscountCode.text = "autodiscount".localized
                        }
                        
                        if orderData.cartDiscountAmountSet != nil {
                            let locale = Locale.getSymbolForCurrencyCode(code: orderData.cartDiscountAmountSet!.presentmentMoney.currencyCode.rawValue)
                            let price = (orderData.cartDiscountAmountSet!.presentmentMoney.amount as NSString).floatValue
                            self.lblDsicountPrice.text! = "-\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
                            
                        }else{
                            
                            self.lblDsicountPrice.text! = "0.00"
                            self.lblDiscountText.isHidden = true
                            self.lblDsicountPrice.isHidden = true
                            self.lblDiscountCode.isHidden = true
                            
                        }
                        
                        let locale = Locale.getSymbolForCurrencyCode(code: orderData.subtotalPriceSet!.presentmentMoney.currencyCode.rawValue)
                        let price = (orderData.subtotalPriceSet!.presentmentMoney.amount as NSString).floatValue
                        self.lblSubtotalPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
                        
                        self.backViewOrderSummary.isHidden = false
                        self.heightOrderSumConstraint.constant = 109.0
                        self.view.layoutIfNeeded()
                        self.view.setNeedsDisplay()
                        
                    }else{
                        self.backViewOrderSummary.isHidden = true
                        self.heightOrderSumConstraint.constant = 0.0
                        self.view.layoutIfNeeded()
                        self.view.setNeedsDisplay()
                    }
                }else{
                    self.backViewOrderSummary.isHidden = true
                    self.heightOrderSumConstraint.constant = 0.0
                    self.view.layoutIfNeeded()
                    self.view.setNeedsDisplay()
                }
            case .failure(let error):
                print("OrderDetailVC Error loading data \(error.localizedDescription)")
                self.backViewOrderSummary.isHidden = true
                self.heightOrderSumConstraint.constant = 0.0
                self.view.layoutIfNeeded()
                self.view.setNeedsDisplay()
            }
        }
    }
    
    func getOrderDetails(orderId:String, lineItems:Int32, ImgHeight: Int32){
        
        ShopifyOrderDetail.shared.orderHistoryList(orderId: orderId, lineItems: lineItems, ImgHeight: ImgHeight) { result, error in
            if let result = result {
                
                self.OrderDetail = result
                let lineItems = self.OrderDetail.lineItems.edges.map {$0.node}
                var orderItems: Int = 0
                var variant_name: String!
                var variant_value: String!
                lineItems.forEach { items in
                    orderItems += Int(items.quantity)
                    
                    let prodOptions = items.variant?.selectedOptions
                    if prodOptions!.count > 1 {
                        
                        variant_name = "\(prodOptions![0].name.uppercased()) / \(prodOptions![1].name.uppercased())"
                        variant_value = "\(prodOptions![0].value.uppercased()) / \(prodOptions![1].value.uppercased())"
                    }else {
                        
                        variant_name = "\(prodOptions![0].name.uppercased())"
                        variant_value = "\(prodOptions![0].value.uppercased())"
                    }
                    let price = NSDecimalNumber(decimal: (items.variant?.priceV2.amount)!).intValue
                    let quantity = items.quantity
                    let ImgUrl = items.variant?.image?.transformedSrc.absoluteURL
                    self.lineItems.append(OrderLineItems(productName: items.title, variantName: variant_name, variantValue: variant_value, price: price, quantity: Int(quantity), ImgUrl: ImgUrl!, currency_code: items.variant!.priceV2.currencyCode.rawValue))
                }
                
                let orderDate = self.OrderDetail.processedAt
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "MMM dd, yyyy"
                
                let showDate = inputFormatter.string(from: orderDate)
                
                if orderItems > 1 {
                    self.lblItems.text! = "items".localized
                }else{
                    self.lblItems.text! = "item".localized
                }
                
                self.lblOrderNumber.text! = self.OrderDetail.name
                self.lblItemCounts.text! = String(orderItems)
                let orderplaced = "orderplace".localized
                self.lblOrderPlacedDate.text! = "\(orderplaced) \(showDate)"
                
                let currency = self.OrderDetail.totalPriceV2.currencyCode.rawValue
                let locale = Locale.getSymbolForCurrencyCode(code: currency)
                let price = self.OrderDetail.totalPriceV2.amount
                
                self.lblTotalAmount.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
                
                self.collectionItems.reloadData()
                
                
            }else{
                self.Showalert1(message: error!)
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}

extension OrderDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.lineItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionItems.dequeueReusableCell(withReuseIdentifier:"OrderDetailItemsCell", for: indexPath) as! OrderDetailItemsCell
        
        cell.lblProductName.text! = self.lineItems[indexPath.row].productName
        cell.lblVariantSizeColor.text! = self.lineItems[indexPath.row].variantValue
        cell.lblVarianttype.text! = "\(self.lineItems[indexPath.row].variantName):"
        cell.lblProductQuantity.text! = "\(self.lineItems[indexPath.row].quantity)"
      
        let price = Float(self.lineItems[indexPath.row].price)
        let locale = Locale.getSymbolForCurrencyCode(code: self.lineItems[indexPath.row].currency_code)
        
        cell.lblProductPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
        
        let imgURL = self.lineItems[indexPath.row].ImgUrl
        
        cell.ImgProduct.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
            // Your code inside completion block
            if image == nil {
                
            }
        })
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.collectionItems.frame.width
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            return CGSize(width: width , height: 200)
        }else{
            return CGSize(width: width , height: 180)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}

