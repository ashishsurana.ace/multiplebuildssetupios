//
//  OrderHistoryListCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 12/04/21.
//

import UIKit

class OrderHistoryListCell: UICollectionViewCell {
    
    @IBOutlet weak var lblOrderText: UILabel!
    @IBOutlet weak var ImgView: UIImageView!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblOrderPlacedDate: UILabel!
    @IBOutlet weak var lblItemCounts: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblTotalText: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblOrderText.font = UIFont.appRegularFontWith(size: AppFontSize.size15)
        self.lblOrderNumber.font = UIFont.appMediumFontWith(size: AppFontSize.size16)
        self.lblOrderPlacedDate.font = UIFont.appItalicFontWith(size: AppFontSize.size14)
        
        self.lblItemCounts.font = UIFont.appMediumFontWith(size: AppFontSize.size16)
        self.lblItems.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblTotalText.font = UIFont.appRegularFontWith(size: AppFontSize.size14)
        self.lblTotalPrice.font = UIFont.appMediumFontWith(size: AppFontSize.size16)
    }
    
}
