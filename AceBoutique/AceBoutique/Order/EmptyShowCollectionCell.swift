//
//  EmptyCollectionCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 30/04/21.
//

import UIKit

class EmptyShowCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblShowMessage: UILabel!
    @IBOutlet weak var btnEmptyAction: UIControl!
    @IBOutlet weak var lblButtonText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.lblShowMessage.font = UIFont.appRegularFontWith(size: AppFontSize.size19)
        self.lblButtonText.font = UIFont.appBoldFontWith(size: AppFontSize.size17)
        self.btnEmptyAction.backgroundColor =  UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblButtonText.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
    }
    
}
