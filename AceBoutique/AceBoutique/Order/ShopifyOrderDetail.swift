//
//  ShopifyOrderDetail.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 14/06/21.
//

import Foundation
import MobileBuySDK


class ShopifyOrderDetail {
    
    static let shared = ShopifyOrderDetail()
    
    func orderHistoryList(orderId:String, lineItems:Int32, ImgHeight: Int32, completion: @escaping (Storefront.Order?, String?) -> Void){
        
        QueryOrderDetail.shared.orderHistoryDetail(orderId: orderId, lineItems: lineItems, ImgHeight: ImgHeight) { result, error in
            if let response = result {
                completion(response, nil)
            }else{
                let error = "errorMsg".localized
                completion(nil, error)
            }
        }
    }
}
