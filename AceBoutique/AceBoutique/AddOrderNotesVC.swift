//
//  AddOrderNotesVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 07/04/21.
//

import UIKit
import FirebaseAnalytics

class AddOrderNotesVC: UIViewController {
    
    @IBOutlet weak var txtNotesView: UITextView!
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var btnAddNotes: UIControl!
    @IBOutlet weak var lblAddNotes: UILabel!
    
    var strOrderNotes:String! = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblScreenTitle.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.btnAddNotes.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblAddNotes.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
        FirebaseEventsLog.request.trackScreen(screenName: "Add order note", className: "AddOrderNotesVC")
        if self.strOrderNotes! != "" {
            self.txtNotesView.text! = self.strOrderNotes!
        }else{
            self.txtNotesView.text! = ""
        }
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAddNotesTapped(_ sender: Any) {
        
        if let presenter = presentingViewController as? CheckoutNavController {
            
            let imageDataDict = ["notes":self.txtNotesView.text!]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showOrderNotes"), object: nil, userInfo: imageDataDict)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
