//
//  FirebaseAnalyticsEvents.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/05/21.
//

import Foundation
import UIKit
import FirebaseAnalytics
import MobileBuySDK


struct FirebaseEventsLog {
    
    
    static var request = FirebaseEventsLog()
    
    func setUserId(userId: String) {
        Analytics.setUserID(userId)
    }
    
    func trackScreen(screenName: String, className: String) {
        
        FirebaseAnalytics.Analytics.logEvent(AnalyticsEventScreenView, parameters: [
            
            AnalyticsParameterScreenName: screenName,
            AnalyticsParameterScreenClass: className
        ])
    }
    
    
    func logWishListEvent(itemId: String, itemName: String) {
        FirebaseAnalytics.Analytics.logEvent(AnalyticsEventAddToWishlist, parameters: [
            
            AnalyticsParameterItemID:itemId,
            AnalyticsParameterItemName: itemName
        ])
    }
    
    
    func logViewItemEvent(product: Storefront.Product) {
        
        FirebaseAnalytics.Analytics.logEvent(AnalyticsEventViewItem, parameters: [
            
            AnalyticsParameterItemID:product.id.rawValue,
            AnalyticsParameterItemName: product.title,
            AnalyticsParameterCurrency: product.priceRange.minVariantPrice.currencyCode.rawValue,
            AnalyticsParameterPrice: product.priceRange.minVariantPrice.amount
        ])
    }
    
    func logCartEvent(itemId: String, itemName: String, itemVariant: String, price: String, currency:String) {
        
        FirebaseAnalytics.Analytics.logEvent(AnalyticsEventAddToCart, parameters: [
            
            AnalyticsParameterItemID:itemId,
            AnalyticsParameterItemName: itemName,
            AnalyticsParameterItemVariant: itemVariant,
            AnalyticsParameterPrice: price,
            AnalyticsParameterCurrency: currency
        ])
    }
    
    func logViewCartEvent(items: [String: Any]) {
        
        FirebaseAnalytics.Analytics.logEvent(AnalyticsEventViewCart, parameters:items)
    }
    
    func logCheckoutEvent( items: [String: Any]) {
        
        FirebaseAnalytics.Analytics.logEvent(AnalyticsEventBeginCheckout, parameters: items)
    }
    
    func logPurchaseEvent(items: [String: Any] ){
        FirebaseAnalytics.Analytics.logEvent(AnalyticsEventPurchase, parameters: items)
    }
    
}
