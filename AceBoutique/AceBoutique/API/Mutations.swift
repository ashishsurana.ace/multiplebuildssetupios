//
//  Mutations.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation
import MobileBuySDK

/**This class having the store mutations for login, renew token, logout and create new account.*/
final class ClientMutations {
    
    static let shared = ClientMutations()
    
    @discardableResult
    func login(email: String, password: String, completion: @escaping (String?, [Storefront.CustomerUserError]) -> Void) -> Task {
        
        let mutation = self.mutationForLogin(email: email, password: password)
        let task     = client.mutateGraphWith(mutation) { (mutation, error) in
            print("login error:",error.debugDescription)
            
            if let container = mutation?.customerAccessTokenCreate?.customerAccessToken {
                GlobalObjects.tokenExpiresAt = container.expiresAt
                
                setMyUserDefaults(value: container.expiresAt, key: MyUserDefaults.TokenExpire)
                print("token expires at:",container.expiresAt)
                completion(container.accessToken, [])
                
            } else {
                
                let errors = mutation?.customerAccessTokenCreate?.customerUserErrors ?? []
                print("Failed to login customer: \(errors)")
                completion(nil, errors)
            }
        }
        
        task.resume()
        return task
    }
    
    
    
    func mutationForLogin(email: String, password: String) -> Storefront.MutationQuery {
        let input = Storefront.CustomerAccessTokenCreateInput(email: email, password: password)
        return Storefront.buildMutation { $0
            .customerAccessTokenCreate(input: input) { $0
                .customerAccessToken { $0
                    .accessToken()
                    .expiresAt()
                    
                }
                .customerUserErrors { $0
                    .code()
                    .field()
                    .message()
                }
            }
        }
    }
    
    
    @discardableResult
    func renewCustomerToken(accessToken:String, completion: @escaping (String?, [Storefront.UserError]) -> Void) -> Task {
        
        let mutation = self.mutationForCustomerTokenRenew(accessToken: accessToken)
        let task     = client.mutateGraphWith(mutation) { (mutation, error) in
            print("login error:",error.debugDescription)
            
            if let container = mutation?.customerAccessTokenRenew?.customerAccessToken {
                GlobalObjects.tokenExpiresAt = container.expiresAt
                setMyUserDefaults(value: container.expiresAt, key: MyUserDefaults.TokenExpire)
                
                print("New token expires at:",container.expiresAt)
                completion(container.accessToken, [])
                
            } else {
                
                let errors = mutation?.customerAccessTokenRenew?.userErrors ?? []
                print("Failed to update token: \(errors)")
                completion(nil, errors)
            }
        }
        
        task.resume()
        return task
    }
    
    func mutationForCustomerTokenRenew(accessToken:String) -> Storefront.MutationQuery {
        return Storefront.buildMutation {$0
            .customerAccessTokenRenew(customerAccessToken: accessToken) {$0
                .customerAccessToken {$0
                    .accessToken()
                    .expiresAt()
                }
                .userErrors{ $0
                    .field()
                    .message()
                }
            }
        }
    }
    
    
    
    
    
    @discardableResult
    func registration(email: String, password: String, firstName: String, lastName:String, phone:String?, completion: @escaping (Storefront.Customer?, [Storefront.CustomerUserError]) -> Void) -> Task {
        
        let mutation = self.mutationForCreateAccount(email: email, password: password, firstName: firstName, lastName: lastName, phone: phone)
        let task   = client.mutateGraphWith(mutation) { (mutation, error) in
            
            print("Registration error:",error.debugDescription)
            
            if let container = mutation?.customerCreate?.customer {
                completion(container, [])
            } else {
                let errors = mutation?.customerCreate?.customerUserErrors ?? []
                print("Failed to register customer: \(error)")
                completion(nil, errors)
            }
        }
        
        task.resume()
        return task
    }
    
    
    func mutationForCreateAccount( email: String, password: String, firstName: String , lastName:String, phone: String? ) -> Storefront.MutationQuery {
        let input = Storefront.CustomerCreateInput.create(email: email, password: password, firstName: .value(firstName), lastName: .value(lastName), phone: .undefined, acceptsMarketing: .value(true))
        
        return Storefront.buildMutation { $0
            .customerCreate(input: input) { $0
                .customer { $0
                    .id()
                    .email()
                    .firstName()
                    .lastName()
                }
                .customerUserErrors { $0
                    .code()
                    .field()
                    .message()
                }
            }
        }
        
    }
    
    
    @discardableResult
    func logout(accessToken: String, completion: @escaping (Bool, String) -> Void) -> Task {
        
        let mutation = self.mutationForLogout(accessToken: accessToken)
        let task     = client.mutateGraphWith(mutation) { (mutation, error) in
            print("Logout error:",error.debugDescription)
            
            if let deletedToken = mutation?.customerAccessTokenDelete?.deletedAccessToken {
                completion(deletedToken == accessToken, "")
            } else {
                let errors = mutation?.customerAccessTokenDelete?.userErrors ?? []
                print("Failed to logout customer: \(errors)")
                if errors.count != 0{
                    completion(false, errors[0].message)
                }
            }
        }
        
        task.resume()
        return task
    }
    
    func mutationForLogout(accessToken: String) -> Storefront.MutationQuery {
        return Storefront.buildMutation { $0
            .customerAccessTokenDelete(customerAccessToken: accessToken) { $0
                .deletedAccessToken()
                .userErrors { $0
                    .field()
                    .message()
                }
            }
        }
    }
  
    
    @discardableResult
    func makeDefaultAddress(accessToken: String, addressID: String, completion: @escaping ([Storefront.CustomerUserError]) -> Void) -> Task {
        
        let mutation = self.mutationForDefaultAddress(accessToken: accessToken, addressID: addressID)
        let task     = client.mutateGraphWith(mutation) { (mutation, error) in
            print("default address error:",error.debugDescription)
            
            if let errors = mutation?.customerDefaultAddressUpdate?.customerUserErrors {
                if errors.count == 0 {
                    completion([])
                }else{
                    print("Failed to set default address: \(errors)")
                    completion(errors)
                }
                
            }else{
                completion([])
                
            }
        }
        task.resume()
        return task
    }
    
    func mutationForDefaultAddress(accessToken: String, addressID: String) -> Storefront.MutationQuery {
        return Storefront.buildMutation { $0
            .customerDefaultAddressUpdate(customerAccessToken: accessToken, addressId:  GraphQL.ID(rawValue: "\(String(describing: addressID))")){ $0
                .customer{$0
                    .id()
                }
                .customerUserErrors{$0
                    .code()
                    .field()
                    .message()
                }
                
            }
        }
    }
    
    
}

