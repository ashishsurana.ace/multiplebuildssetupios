//
//  QueryGetCustomerAddress.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 15/06/21.
//

import Foundation
import MobileBuySDK

/**Class calling the GraphQL query to get customers address.*/
final class QueryGetCustomerAddress {
    
    static let shared = QueryGetCustomerAddress()
    
//    private init() {
//        client.cachePolicy = .cacheFirst(expireIn: 900)
//    }
    
    //  MARK: - Get Orders -
    @discardableResult
    func customerAddress(accessToken: String, addresses: Int32, completion: @escaping (Storefront.Customer?, Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForCustomerAddress(accessToken: accessToken, addresses: addresses)
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let customer = query.customer
                completion(customer, nil)
            } else {
                print("Failed to load address: \(String(describing: error))")
                completion(nil, error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForCustomerAddress(accessToken: String, addresses: Int32) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .customer(customerAccessToken: accessToken) { $0
                .addresses(first: addresses) { $0
                    .edges { $0
                        .node { $0
                            .id()
                            .firstName()
                            .lastName()
                            .phone()
                            .address1()
                            .address2()
                            .city()
                            .province()
                            .country()
                            .zip()
                        }
                    }
                }
                .defaultAddress { $0
                    .id()
                    .firstName()
                    .lastName()
                    .phone()
                    .address1()
                    .address2()
                    .city()
                    .province()
                    .country()
                    .zip()
                    
                }
            }
        }
    }
}



