//
//  QueryFetchProducts.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation
import MobileBuySDK

/**This class fetches the the products for dashboard grid.*/
final class QueryFetchProducts {
    
    
    static let shared = QueryFetchProducts()
    
//    private init() {
//        client.cachePolicy = .cacheFirst(expireIn: 3600)
//    }
    
    //  MARK: - Products -
    @discardableResult
    func fetchProductGridData(isTitlePresent: Bool, id: GraphQL.ID, total: Int32, imgMaxHeight: Int32, completion: @escaping ([Storefront.Product], String, Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForProducts(id: id, total: total, imgMaxHeight: imgMaxHeight)
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let collectionObj = query.node as? Storefront.Collection
                var sliderTitle: String!
                if isTitlePresent == false {
                    sliderTitle = collectionObj!.title
                }else{
                    sliderTitle = ""
                }
                let products = collectionObj!.products.edges.map { $0.node }
                completion(products,sliderTitle, nil)
            } else {
                print("Failed to load collections: \(String(describing: error))")
                completion([],"", error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForProducts(id: GraphQL.ID, total: Int32, imgMaxHeight: Int32) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .node(id: id) { $0
                .onCollection { $0
                    .id()
                    .title()
                    .products(first: total, reverse: true, sortKey: Storefront.ProductCollectionSortKeys.created) { $0
                        .edges { $0
                            .node { $0
                                .id()
                                .title()
                                .productType()
                                .description()
                                .images(first: 1){ $0
                                    .edges { $0
                                        .node { $0
                                            .id()
                                            .originalSrc()
                                            .transformedSrc(maxHeight: imgMaxHeight)
                                        }
                                    }
                                }
                                .priceRange { $0
                                    .minVariantPrice{ $0
                                        .amount()
                                        .currencyCode()
                                    }
                                    .maxVariantPrice{ $0
                                        .amount()
                                        .currencyCode()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
