//
//  QueryLastOrder.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 18/06/21.
//

import Foundation
import MobileBuySDK

/**This class is calling the query to fetch only the last order by the customer.*/
final class QueryLastOrder {
    
    static let shared = QueryLastOrder()
    
    //  MARK: - Get Orders -
    @discardableResult
    func getLastOrder(accessToken: String, completion: @escaping (Storefront.OrderConnection?, Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForLastOrder(accessToken: accessToken)
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let orders = query.customer!.orders
                completion(orders, nil)
            } else {
                print("Failed to load orders: \(String(describing: error))")
                completion(nil, error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForLastOrder(accessToken: String) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .customer(customerAccessToken: accessToken) { $0
                .orders(first: 1, reverse: true) {$0
                    .pageInfo { $0
                        .hasNextPage()
                    }
                    .edges {$0
                        .cursor()
                        
                        .node {$0
                            
                            .id()
                            .orderNumber()
                            .name()
                            .processedAt()
                        }
                    }
                }
            }
        }
    }
}

