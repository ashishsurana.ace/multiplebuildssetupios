//
//  QueryCheckout.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 18/06/21.
//

import Foundation
import MobileBuySDK

/**Class calling the GraphQL query for complete checkout with user update.*/
final class QueryCompleteCheckout {
    
    static let shared = QueryCompleteCheckout()
    
    //  MARK: - Get Orders -
    @discardableResult
    func callCheckoutAPI(checkoutId: String, completion: @escaping (Storefront.Checkout?, Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForCheckout(checkoutId: checkoutId)
        
        let retry = Graph.RetryHandler<Storefront.QueryRoot>(endurance: .finite(80), interval: 2.0, condition: { (query, error) -> Bool in
            let mycondition = (query?.node as? Storefront.Checkout)?.order == nil
            if mycondition {
                //                print("Order In Progress")
                return true
            }else{
                
                //                print("Order Placed")
                return false
            }
        })
        
        let task  = client.queryGraphWith(query, retryHandler: retry) { query, error in
            
            let checkout = (query?.node as? Storefront.Checkout)
            print("Order Handler Running...")
            
            let orderID  = checkout?.order?.id
            if orderID != nil {
                print("ORDER ID:", orderID!)
                completion(checkout, nil)
            }else{
                print("Failed to load orders: \(String(describing: error))")
                completion(nil, error)
                
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForCheckout(checkoutId: String) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .node(id: GraphQL.ID(rawValue: "\(checkoutId)")) { $0
                .onCheckout { $0
                    .order { $0
                        .id()
                        .processedAt()
                        .orderNumber()
                        .name()
                        .totalPriceV2{ $0
                            .amount()
                            .currencyCode()
                            
                        }
                    }
                }
            }
        }
    }
}
