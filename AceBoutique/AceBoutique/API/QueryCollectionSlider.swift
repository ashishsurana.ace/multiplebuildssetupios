//
//  Queries.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation
import MobileBuySDK

/**this class calls the GraphQL query to fetch store collections data.*/
final class ClientQueries {
    
    static let Queries = ClientQueries()
    
//    private init() {
//        client.cachePolicy = .cacheFirst(expireIn: 3600)
//    }
    
    //  MARK: - Collections -
    @discardableResult
    func fetchCollections(ids: [GraphQL.ID], completion: @escaping ([Storefront.Collection], Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForCollections(ids: ids)
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let collections = query.nodes as? [Storefront.Collection]
                completion(collections ?? [], nil)
            } else {
                print("Failed to load collections: \(String(describing: error))")
                completion([], error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForCollections(ids: [GraphQL.ID]) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .nodes(ids: ids) { $0
                .onCollection { $0
                    .id()
                    .title()
                    .image{ $0
                        .id()
                        .originalSrc()
                        
                    }
                }
            }
        }
    }
}
