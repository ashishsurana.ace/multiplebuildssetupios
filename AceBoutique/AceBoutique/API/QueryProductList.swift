//
//  QueryProductList.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/06/21.
//

import Foundation
import MobileBuySDK

/**Class is providing the list of products for dashboard product slider. */
final class QueryFetchProductsLists {
    
    
    static let shared = QueryFetchProductsLists()
    
//    private init() {
//        client.cachePolicy = .cacheFirst(expireIn: 3600)
//    }
    
    //  MARK: - Products -
    @discardableResult
    func fetchProductListData(id: GraphQL.ID, cursor: String?, products: Int32,variants: Int32, sort_Type:Storefront.ProductCollectionSortKeys?, reverse: Bool?, ImgHeight: Int32, completion: @escaping (Storefront.Collection?, Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForProductList(id: id, cursor: cursor, products: products, variants: variants, sort_Type: sort_Type, reverse: reverse, ImgHeight: ImgHeight)
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let collectionObj = query.node as? Storefront.Collection
                completion(collectionObj, nil)
            } else {
                print("Failed to load collections: \(String(describing: error))")
                completion(nil, error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForProductList(id: GraphQL.ID, cursor: String?, products: Int32,variants: Int32, sort_Type:Storefront.ProductCollectionSortKeys?, reverse: Bool?, ImgHeight: Int32) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .node(id: id) { $0
                .onCollection { $0
                    .id()
                    .title()
                    .products(first: products, after: cursor, reverse: reverse, sortKey: sort_Type) { $0
                        .pageInfo { $0
                            .hasNextPage()
                        }
                        .edges { $0
                            .cursor()
                            .node { $0
                                .id()
                                .title()
                                .images(first: 1) { $0
                                    .pageInfo { $0
                                        .hasNextPage()
                                    }
                                    .edges { $0
                                        .cursor()
                                        .node { $0
                                            .originalSrc()
                                            .transformedSrc(maxHeight: ImgHeight)
                                            
                                        }
                                    }
                                }
                                .variants(first: variants) { $0
                                    .edges { $0
                                        .node { $0
                                            .id()
                                            .price()
                                            .title()
                                            .compareAtPrice()
                                            .availableForSale()
                                            .image{ $0
                                                .originalSrc()
                                                .transformedSrc(maxHeight: ImgHeight)
                                                
                                            }
                                        }
                                    }
                                }
                                .priceRange { $0
                                    .minVariantPrice{ $0
                                        .amount()
                                        .currencyCode()
                                        
                                    }
                                    .maxVariantPrice{ $0
                                        .amount()
                                        .currencyCode()
                                        
                                    }
                                    
                                }
                            }
                        }
                        
                    }
                }
            }
        }
    }
}
