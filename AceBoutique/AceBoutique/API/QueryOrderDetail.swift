//
//  QuerySearchDetail.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 14/06/21.
//

import Foundation
import MobileBuySDK

/**Calling the GraphQL query to get the order details.*/
final class QueryOrderDetail {
    
    static let shared = QueryOrderDetail()
    
    private init() {
        client.cachePolicy = .cacheFirst(expireIn: 3600)
    }
    
    //  MARK: - Get Orders -
    @discardableResult
    func orderHistoryDetail(orderId:String, lineItems:Int32, ImgHeight: Int32, completion: @escaping (Storefront.Order?, Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForOrderDetail(orderId: orderId, lineItems: lineItems, ImgHeight: ImgHeight)
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let orders = query.node as? Storefront.Order
                completion(orders, nil)
            } else {
                print("Failed to load order detail: \(String(describing: error))")
                completion(nil, error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForOrderDetail(orderId:String, lineItems:Int32, ImgHeight: Int32) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .node(id: GraphQL.ID(rawValue: "\(String(describing:orderId))")) { $0
                .onOrder { $0
                    .id()
                    .orderNumber()
                    .name()
                    .fulfillmentStatus()
                    .processedAt()
                    .originalTotalPrice{$0
                        .amount()
                        .currencyCode()
                    }
                    .totalPriceV2 {$0
                        .amount()
                        .currencyCode()
                    }
                    .subtotalPriceV2 {$0
                        .amount()
                        .currencyCode()
                    }
                    .totalShippingPriceV2{$0
                        .amount()
                        .currencyCode()
                    }
                    .totalTaxV2{$0
                        .amount()
                        .currencyCode()
                    }
                    
                    .lineItems(first: lineItems){$0
                        .edges{$0
                            .node{$0
                                .quantity()
                                .title()
                                .variant{$0
                                    .priceV2{$0
                                        .amount()
                                        .currencyCode()
                                    }
                                    .image{$0
                                        .transformedSrc( maxHeight: ImgHeight)
                                    }
                                    .unitPrice{$0
                                        .amount()
                                        .currencyCode()
                                    }
                                    .selectedOptions{$0
                                        .name()
                                        .value()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
