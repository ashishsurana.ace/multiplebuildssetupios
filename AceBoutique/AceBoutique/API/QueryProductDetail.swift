//
//  QueryProductDetail.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/06/21.
//

import Foundation
import MobileBuySDK

/**This class calls the query for product detail.*/
final class QueryFetchProductDetail {
    
    static let shared = QueryFetchProductDetail()
    
//    private init() {
//        client.cachePolicy = .cacheFirst(expireIn: 3600)
//    }
    
    //  MARK: - Products -
    @discardableResult
    func fetchProductDetail(id: GraphQL.ID, images: Int32,variants: Int32, ImgHeight: Int32, variantImgHeight: Int32, completion: @escaping (Storefront.Product?, Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForProductDetail(id: id, images: images, variants: variants, ImgHeight: ImgHeight, variantImgHeight: variantImgHeight)
       
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let product = query.node as? Storefront.Product
                completion(product, nil)
            } else {
                print("Failed to load product detail: \(String(describing: error))")
                completion(nil, error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForProductDetail(id: GraphQL.ID, images: Int32,variants: Int32, ImgHeight: Int32, variantImgHeight: Int32) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .node(id: id) { $0
                .onProduct { $0
                    .id()
                    .title()
                    .description()
                    .descriptionHtml()
                    .availableForSale()
                    .images(first: images) { $0
                        .edges { $0
                            .node { $0
                                .id()
                                .originalSrc()
                                .transformedSrc(maxHeight: ImgHeight)
                            }
                        }
                    }
                    .options{ $0
                        .name()
                        .values()
                    }
                    .variants(first: variants) { $0
                        .edges { $0
                            .node { $0
                                .id()
                                .price()
                                .title()
                                .compareAtPrice()
                                .availableForSale()
                                .image{ $0
                                    .originalSrc()
                                    .transformedSrc(maxHeight: variantImgHeight)
                                    
                                }
                            }
                        }
                    }
                    
                    .priceRange { $0
                        .minVariantPrice{ $0
                            .amount()
                            .currencyCode()
                            
                        }
                        
                    }
                }
            }
        }
    }
}
