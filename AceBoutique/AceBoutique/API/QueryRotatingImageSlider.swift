//
//  QueryRotatingImageSlider.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 04/08/21.
//

import Foundation
import MobileBuySDK


final class CollectionSliderQueries {
    
    static let Queries = CollectionSliderQueries()
    
//    private init() {
//        client.cachePolicy = .cacheFirst(expireIn: 3600)
//    }
    
    //  MARK: - Collections -
    @discardableResult
    func fetchCollectionSliderImg(ids: [GraphQL.ID], completion: @escaping ([Storefront.Collection], Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForCollectionSliderImg(ids: ids)
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let collections = query.nodes as? [Storefront.Collection]
                completion(collections ?? [], nil)
            } else {
                print("Failed to load collections: \(String(describing: error))")
                completion([], error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForCollectionSliderImg(ids: [GraphQL.ID]) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .nodes(ids: ids) { $0
                .onCollection { $0
                    .id()
                    .image(maxWidth: Int32(ScreenSize.WIDTH)){ $0
                        .originalSrc()
                        .transformedSrc(maxWidth: Int32(ScreenSize.WIDTH))
                        
                    }
                }
            }
        }
    }
}


final class ProductSliderQueries {
    
    static let shared = ProductSliderQueries()
    
//    private init() {
//        client.cachePolicy = .cacheFirst(expireIn: 3600)
//    }
    
    //  MARK: - Products -
    @discardableResult
    func fetchProductSliderImg(id: GraphQL.ID, completion: @escaping (Storefront.Product?, Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForProductSliderImg(id: id)
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let product = query.node as? Storefront.Product
                completion(product, nil)
            } else {
                print("Failed to load product detail: \(String(describing: error))")
                completion(nil, error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForProductSliderImg(id: GraphQL.ID) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .node(id: id) { $0
                .onProduct { $0
                    .id()
                    .images(first:10) { $0
                        .edges { $0
                            .node { $0
                                .originalSrc()
                                .transformedSrc(maxWidth: Int32(ScreenSize.WIDTH))
                            }
                        }
                    }
                }
            }
        }
    }
}
