//
//  QueryOrderHistoryList.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 14/06/21.
//

import Foundation
import MobileBuySDK

/**This class calls the  GraphQL query to fetch the user order history.*/
final class QueryOrderHistoryList {
    
    static let shared = QueryOrderHistoryList()
    
    private init() {
        client.cachePolicy = .cacheFirst(expireIn: 3600)
    }
    
    //  MARK: - Get Orders -
    @discardableResult
    func orderHistoryList(accessToken: String, cursor: String?, orders:Int32, discounts:Int32, lineItems:Int32, ImgHeight: Int32, completion: @escaping (Storefront.OrderConnection?, Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForOrderHistory(accessToken: accessToken, cursor: cursor, orders: orders, discounts: discounts, lineItems: lineItems, ImgHeight: ImgHeight)
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let orders = query.customer?.orders
                //                print("customer orders",orders!)
                completion(orders, nil)
            } else {
                print("Failed to load orders: \(String(describing: error))")
                completion(nil, error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForOrderHistory(accessToken: String, cursor: String?, orders:Int32, discounts:Int32, lineItems:Int32, ImgHeight: Int32) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .customer(customerAccessToken: accessToken) { $0
                .orders(first: orders, after: cursor, reverse: true) {$0
                    .pageInfo { $0
                        .hasNextPage()
                    }
                    .edges {$0
                        .cursor()
                        
                        .node {$0
                            
                            .id()
                            .orderNumber()
                            .name()
                            .processedAt()
                            .originalTotalPrice{$0
                                .amount()
                                .currencyCode()
                            }
                            .subtotalPriceV2 {$0
                                .amount()
                                .currencyCode()
                            }
                            .totalShippingPriceV2{$0
                                .amount()
                                .currencyCode()
                            }
                            .totalPriceV2 {$0
                                .amount()
                                .currencyCode()
                            }
                            .fulfillmentStatus()
                            .shippingDiscountAllocations {$0
                                .allocatedAmount{$0
                                    .amount()
                                    .currencyCode()
                                    
                                }
                            }
                            .discountApplications(first:discounts){$0
                                .edges {$0
                                    .node{$0
                                        .allocationMethod()
                                        .onDiscountCodeApplication{$0
                                            .allocationMethod()
                                            .applicable()
                                            .code()
                                        }
                                        .value{$0
                                            .onPricingPercentageValue{$0
                                                .percentage()
                                            }
                                            .onMoneyV2{$0
                                                .amount()
                                            }
                                            
                                        }
                                    }
                                }
                            }
                            
                            .lineItems(first: lineItems){$0
                                .edges{$0
                                    .node{$0
                                        .quantity()
                                        .title()
                                        .variant{$0
                                            .priceV2{$0
                                                .amount()
                                                .currencyCode()
                                            }
                                            .image{$0
                                                .originalSrc()
                                                .transformedSrc(maxHeight: ImgHeight)
                                            }
                                            .unitPrice{$0
                                                .amount()
                                                .currencyCode()
                                            }
                                            .selectedOptions{$0
                                                .name()
                                                .value()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
