//
//  QuerySearchProducts.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/06/21.
//

import Foundation
import MobileBuySDK

/**This class calling the GraphQL query of search to get all products. */
final class QuerySearchProducts {
    
    
    static let shared = QuerySearchProducts()
    
//    private init() {
//        client.cachePolicy = .cacheFirst(expireIn: 3600)
//    }
    
    //  MARK: - Search Products -
    @discardableResult
    func searchShopifyProducts(products: Int32, query: String, cursor: String?, sort_Type:Storefront.ProductSortKeys?, reverse: Bool?, ImgHeight: Int32, completion: @escaping (Storefront.ProductConnection?, Graph.QueryError?) -> Void) -> Task {
        
        let query = self.queryForSearchProducts(products: products, query: query, cursor: cursor, sort_Type: sort_Type, reverse: reverse, ImgHeight: ImgHeight)
        let task  = client.queryGraphWith(query) { (query, error) in
            
            if let query = query {
                let product = query.products
                completion(product, nil)
            } else {
                print("Failed to load products: \(String(describing: error))")
                completion(nil, error)
            }
        }
        
        task.resume()
        return task
    }
    
    //  MARK: - Storefront -
    func queryForSearchProducts(products: Int32, query: String, cursor: String?, sort_Type:Storefront.ProductSortKeys?, reverse: Bool?, ImgHeight: Int32) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .products(first: products, after: cursor, reverse: reverse, sortKey: sort_Type, query:query) { $0
                .pageInfo { $0
                    .hasNextPage()
                }
                .edges { $0
                    .cursor()
                    .node { $0
                        .id()
                        .title()
                        .description()
                        .images(first: 1,maxHeight: ImgHeight) { $0
                            .pageInfo { $0
                                .hasNextPage()
                            }
                            .edges { $0
                                .cursor()
                                .node { $0
                                    .originalSrc()
                                    .transformedSrc(maxHeight: ImgHeight)
                                }
                            }
                        }
                        
                        .priceRange { $0
                            .minVariantPrice{ $0
                                .amount()
                                .currencyCode()
                                
                            }
                            .maxVariantPrice{ $0
                                .amount()
                                .currencyCode()
                                
                            }
                            
                        }
                    }
                }
                
            }
        }
    }
}
