//
//  HTTPRequest.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation
import UIKit

/**This class calls http request.*/
final class callAPI {
    
    static let shared = callAPI()
    
    func loadJson(fromURLString urlString: String,
                  completion: @escaping (Result<Data, Error>) -> Void) {
        if let url = URL(string: urlString) {
            let urlSession = URLSession(configuration: .ephemeral).dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                }
                
                if let data = data {
                    completion(.success(data))
                }
            }
            
            urlSession.resume()
        }
    }
    
    
    func parse(jsonData: Data) ->  String {
        var StrJson: String!
        do {
            let decodedData = try JSONDecoder().decode(ConfigResponse.self, from: jsonData)
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            
            do {
                let jsonData = try encoder.encode(decodedData)
                
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    
                    StrJson = jsonString
                    
                }
            } catch {
                print(error.localizedDescription)
            }
            
        } catch {
            
            print("decode error")
        }
       return StrJson
    }
    
}
