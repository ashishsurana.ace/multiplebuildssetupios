//
//  rotatingImageSliderCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/02/21.
//

import UIKit
import MobileBuySDK

class rotatingImageSliderCell: UITableViewCell {
    
    @IBOutlet weak var collectionSlider: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var scrollView:UIScrollView!
    var frame1: CGRect = CGRect(x:0, y:0, width:0, height:0)
    var ImgRotatingUrls = [String?]()
    var objArrImageSlider = [RotatingImage]()
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.pageControl.currentPage = 0
        self.pageControl.pageIndicatorTintColor = UIColor.gray
        self.pageControl.currentPageIndicatorTintColor = UIColor.black
        
        self.collectionSlider.delegate = self
        self.collectionSlider.dataSource = self
        self.collectionSlider.register(UINib(nibName: "rotatingImgCollectionCell", bundle: nil), forCellWithReuseIdentifier: "rotatingImgCollectionCell")
        DispatchQueue.main.async {
            self.collectionSlider.reloadData()
            self.collectionSlider.layoutIfNeeded()
        }
    }
    
}

extension rotatingImageSliderCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = collectionSlider.contentOffset
        visibleRect.size = collectionSlider.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = collectionSlider.indexPathForItem(at: visiblePoint) else { return }
        
        print("Current visible cell index:",indexPath.row)
        if self.ImgRotatingUrls.count > 1 {
            self.pageControl.isHidden = false
            self.pageControl.numberOfPages = self.ImgRotatingUrls.count
            self.pageControl.currentPage = indexPath.row
            self.pageControl.pageIndicatorTintColor = UIColor.gray
            self.pageControl.currentPageIndicatorTintColor = UIColor.black
        }else{
            
            self.pageControl.isHidden = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.ImgRotatingUrls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionSlider.dequeueReusableCell(withReuseIdentifier:"rotatingImgCollectionCell", for: indexPath) as! rotatingImgCollectionCell
        
        if self.ImgRotatingUrls.count > 1 {
            self.pageControl.isHidden = false
        }else{
            
            self.pageControl.isHidden = true
        }
        
        cell.cnstrntImageWidth.constant = ScreenSize.WIDTH
        cell.cnstrntImageHeight.constant = ScreenSize.WIDTH * (0.66)
        
        self.pageControl.numberOfPages = self.ImgRotatingUrls.count
        
        let strURL = self.ImgRotatingUrls[indexPath.row]
        
        if strURL != nil {
            let imgURL = URL(string:strURL!)
            cell.ImgSlider.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                // Your code inside completion block
                if image == nil {
                    cell.ImgSlider.image = #imageLiteral(resourceName: "placeholder")
                }
            })
        }else{
            cell.ImgSlider.image = #imageLiteral(resourceName: "placeholder")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.objArrImageSlider[indexPath.row]
        
        if item.isClickable == true {
            self.collectionSlider.isUserInteractionEnabled = false
            if item.isCollection == true {
                let query = Storefront.buildQuery { $0
                    .node( id: GraphQL.ID(rawValue: "\(item.platformCollectionID!)")) { $0
                        .onCollection { $0
                            .title()
                        }
                    }
                }
                
//                client.cachePolicy = .cacheFirst(expireIn: 3600)
                if CheckNetwork.isConnectedToNetwork() == true {
                let task = client.queryGraphWith(query) { response, error in
                    print("repsonse....:", response as Any)
                    let collection = response?.node as? Storefront.Collection
                    let title = collection?.title
                    let imageDataDict = ["id":item.platformCollectionID!, "name":"\(title!)"]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showProductLists"), object: nil, userInfo: imageDataDict)
                }
                
                task.resume()
                }
                
            }else if item.isProduct == true {
                
                let productDataDict = ["id":item.platformProductID!]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showProductDetail"), object: nil, userInfo: productDataDict)
            }else {
                print("No action added.")
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ScreenSize.WIDTH, height: ScreenSize.WIDTH * (0.66))
    }
    
}
