//
//  BannerImageCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/02/21.
//

import UIKit

class BannerImageCell: UITableViewCell {
    
    @IBOutlet weak var ImgBanner: UIImageView!
    
    
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                ImgBanner.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                ImgBanner.addConstraint(aspectConstraint!)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCustomImage(image : UIImage) {
        
        let aspect = image.size.width / image.size.height
        
        let constraint = NSLayoutConstraint(item: ImgBanner!, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: ImgBanner!, attribute: NSLayoutConstraint.Attribute.height, multiplier: aspect, constant: 0.0)
        constraint.priority =  UILayoutPriority.init(999)
        
        aspectConstraint = constraint
        
        ImgBanner!.image = image
    }
}
