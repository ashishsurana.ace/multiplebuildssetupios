//
//  CollectionTableCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/02/21.
//

import UIKit

class CollectionTableCell: UITableViewCell {
    
    weak var delegate:CollectionProductSliderDelegate?
    
    @IBOutlet weak var btnViewAll: UIControl!
    @IBOutlet weak var lblProductSliderTitle: UILabel!
    @IBOutlet weak var headertView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var ProductCollection: UICollectionView!
    
    @IBOutlet weak var heightCollectionViewConstant: NSLayoutConstraint!
    
    var ImgProductsList = [URL]()
    var CollectionName: String?
    var Collection_ID:String!
    var arrProductSliderList = NSMutableArray()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.ProductCollection.delegate = self
        self.ProductCollection.dataSource = self
        self.ProductCollection.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "ProductCell")
        self.ProductCollection.register(UINib(nibName: "ProductViewMoreCell", bundle: nil), forCellWithReuseIdentifier: "ProductViewMoreCell") 
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWhenProductFound(_:)), name: Notification.Name("ReloadProductGrid"), object: nil)
        
        DispatchQueue.main.async {
            
            self.ProductCollection.reloadData()
            self.heightCollectionViewConstant.constant = self.ProductCollection.collectionViewLayout.collectionViewContentSize.height
            self.ProductCollection.layoutIfNeeded()
            
        }
    }
    
    
    @objc func reloadWhenProductFound(_ notification: NSNotification) {
        self.ProductCollection.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension CollectionTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ImgProductsList.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if  indexPath.row > self.ImgProductsList.count - 1 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"ProductViewMoreCell", for: indexPath) as! ProductViewMoreCell
            return cell
            
        }else
        {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"ProductCell", for: indexPath) as! ProductCell
            
            let title = ((self.arrProductSliderList.object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as? String ?? "")
            cell.lblProductName.text! = "\(title)"
            cell.lblProductName.font = UIFont.appRegularFontWith(size: AppFontSize.size12)
            
            let price = ((self.arrProductSliderList.object(at: indexPath.row) as! NSDictionary).value(forKey: "min_price") as? NSNumber ?? 0 )
            let currency = ((self.arrProductSliderList.object(at: indexPath.row) as! NSDictionary).value(forKey: "Min_Currency") as? String ?? "")
            
            print(title,price,currency)
            let locale = Locale.getSymbolForCurrencyCode(code: currency)
            cell.lblPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
            cell.lblPrice.font = UIFont.appMediumFontWith(size: AppFontSize.size14)
            
            let imgURL = self.ImgProductsList[indexPath.row]
            
            cell.ImgProduct.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                // Your code inside completion block
                if image == nil {
                    
                }
                cell.ImgProduct.clipsToBounds = true
                cell.ImgProduct.cornerRadius = 2
            })
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        let jsonData = GlobalObjects.jsonString.data(using: .utf8)!
        //        let objConfig = try! JSONDecoder().decode(ConfigResponse.self, from: jsonData)
        
        if (UIDevice.current.userInterfaceIdiom == .pad) {
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                return CGSize(width: 200, height: 320)
            }else{
                return CGSize(width: 180, height: 250)
            }
            
        }else{
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                return CGSize(width: 150, height: 260)
            }else{
                return CGSize(width: 150, height: 230)
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if  indexPath.row > self.ImgProductsList.count - 1 {
            if delegate != nil {
                delegate?.didcellTapped(collectionID:Collection_ID!, CollectionName: CollectionName ?? "")
            }
        }else{
            
            let product_ID = (self.arrProductSliderList.object(at: indexPath.row) as! NSDictionary).value(forKey: "Id") as! String
            let productDataDict = ["id":product_ID]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showProductDetail"), object: nil, userInfo: productDataDict)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.heightCollectionViewConstant.constant = self.ProductCollection.collectionViewLayout.collectionViewContentSize.height
            self.ProductCollection.layoutIfNeeded()
        }
    }
}



