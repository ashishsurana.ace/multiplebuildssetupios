//
//  ProductViewMoreCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/02/21.
//

import UIKit

protocol CollectionProductSliderDelegate:class {
    
    func didcellTapped(collectionID:String, CollectionName: String)
}

class ProductViewMoreCell: UICollectionViewCell {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblViewMore: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
