//
//  AnnouncementBarCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/02/21.
//

import UIKit

class AnnouncementBarCell: UITableViewCell {

    @IBOutlet weak var BaseView: UIView!
    @IBOutlet weak var lblAnnouncement: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
