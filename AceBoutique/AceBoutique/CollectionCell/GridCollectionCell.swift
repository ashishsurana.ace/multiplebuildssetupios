//
//  GridCollectionCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/02/21.
//

import UIKit

class GridCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var ImgFav: UIImageView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var ImgProductGrid: UIImageView!
    @IBOutlet weak var btnQuickAdd: UIControl!
    @IBOutlet weak var heightQuickAddConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnFavorite: UIControl!
    @IBOutlet weak var heightFavorite: NSLayoutConstraint!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var widthFavouritConstraint: NSLayoutConstraint!
    @IBOutlet weak var ImgHeightConstraint: NSLayoutConstraint!
    
    
    var imgHeight: CGFloat!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblPrice.font = UIFont.appMediumFontWith(size: AppFontSize.size14)
        self.lblItemName.font = UIFont.appRegularFontWith(size: AppFontSize.size12)
        
        ImgProductGrid.clipsToBounds = true
        ImgProductGrid.layer.cornerRadius = 2
        ImgProductGrid.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner] //For top corners
        
        btnQuickAdd.clipsToBounds = true
        btnQuickAdd.layer.cornerRadius = 2
        btnQuickAdd.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner] //For bottom corners
        
    }
}


