//
//  ImageBannerCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 08/02/21.
//

import UIKit
import MobileBuySDK

class CollectionSliderTableCell: UITableViewCell {
    
    @IBOutlet weak var collectionCategories: UICollectionView!
    
    var storeFrontCollections = NSMutableArray()
    //    var ImgCollections = [URL]()
    var arrData = NSMutableArray()
    var Collection_ID:String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.collectionCategories.delegate = self
        self.collectionCategories.dataSource = self
        self.collectionCategories.register(UINib(nibName: "CollectionCells", bundle: nil), forCellWithReuseIdentifier: "CollectionCells")
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension CollectionSliderTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.storeFrontCollections.count != 0 {
            return self.storeFrontCollections.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.storeFrontCollections.count != 0 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"CollectionCells", for: indexPath) as! CollectionCells
            var collectionName: String!
            let isShowTitle = (self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "isShowCustomTitle") as? Bool
            if isShowTitle == true {
                if isPrimaryLanguageSelected == true {
                    collectionName = ((self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "customTitle") as? String ?? "")
                    if collectionName == "" {
                        collectionName = ((self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "ShopifyCollectionName") as? String ?? "")
                    }
                }else{
                    collectionName = ((self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "ShopifyCollectionName") as? String ?? "")
                }
                
                print("collection Name:",collectionName)
                cell.lblCollectionName.text! = collectionName.TrimString(collectionName)
                cell.lblCollectionName.font = UIFont.appMediumFontWith(size: AppFontSize.size14)
            }else{
                cell.lblCollectionName.text! = ""
            }
            if ((self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "isShowOverlay") as! Bool) == true {
                
                cell.overlayView.isHidden = false
                
            }else{
                
                cell.overlayView.isHidden = true
            }
            
            var ImageUrl: URL!
            
            let strUrl = (self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "customImageURL") as? String
            
            if strUrl == nil {
                ImageUrl = (self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "shopifyImageUrl")  as? URL
            }else{
                ImageUrl = URL(string:strUrl!)
            }
            
            cell.ImgCollections.sd_setImage(with: ImageUrl, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                // Your code inside completion block
                cell.ImgCollections.cornerRadius = 2
                cell.ImgCollections.clipsToBounds = true
                if image == nil {
                    
                }
            })
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"CollectionCells", for: indexPath) as! CollectionCells
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.storeFrontCollections.count != 0 {
            let collection_id = (self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "shopifyCollectionID") as? String
            var collectionName: String!
            if isPrimaryLanguageSelected == true {
                collectionName = ((self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "customTitle") as? String ?? "")
                if collectionName == "" {
                    collectionName = ((self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "ShopifyCollectionName") as? String ?? "")
                }
            }else{
                collectionName = ((self.storeFrontCollections.object(at: indexPath.row) as! NSDictionary).value(forKey: "ShopifyCollectionName") as? String ?? "")
            }
            let imageDataDict = ["id":collection_id!, "name":collectionName!]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showProductLists"), object: nil, userInfo: imageDataDict)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            return CGSize(width: 220, height: 105)
        }else{
            return CGSize(width: 155, height: 75)
        }
        
    }
}
