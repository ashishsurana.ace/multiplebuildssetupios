//
//  HomeCollectionCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 28/01/21.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {

    @IBOutlet weak var backview: UIView!
    @IBOutlet weak var ImgCollection: UIImageView!
    @IBOutlet weak var lblCollectionName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
