//
//  GridLayoutTableCell.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/02/21.
//

import UIKit
import MobileBuySDK

class GridLayoutTableCell: UITableViewCell {
    
    @IBOutlet weak var btnViewAll: UIControl!
    @IBOutlet weak var lblGridTitle: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var GridCollection: UICollectionView!
    @IBOutlet weak var heightCollectionview: NSLayoutConstraint!
    
    var ImgProductGridURLs = [URL]()
    var arrProductDetail = NSMutableArray()
    var CollectionName: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.GridCollection.delegate = self
        self.GridCollection.dataSource = self
        self.GridCollection.register(UINib(nibName: "GridCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GridCollectionCell")
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWhenProductFound(_:)), name: Notification.Name("ReloadProductGrid"), object: nil)
        DispatchQueue.main.async {
            self.GridCollection.reloadData()
            self.heightCollectionview.constant = self.GridCollection.collectionViewLayout.collectionViewContentSize.height
            self.GridCollection.layoutIfNeeded()
        }
    }
    
    @objc func reloadWhenProductFound(_ notification: NSNotification) {
        self.GridCollection.reloadData()
        self.GridCollection.layoutIfNeeded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}

extension GridLayoutTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ImgProductGridURLs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"GridCollectionCell", for: indexPath) as! GridCollectionCell
        
        let title = ((self.arrProductDetail.object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as? String ?? "")
        cell.lblItemName.text! = "\(title)"
        
        
        let price = ((self.arrProductDetail.object(at: indexPath.row) as! NSDictionary).value(forKey: "min_price") as? NSNumber ?? 0 )
        let currency = ((self.arrProductDetail.object(at: indexPath.row) as! NSDictionary).value(forKey: "Min_Currency") as? String ?? "")
        
        let locale = Locale.getSymbolForCurrencyCode(code: currency)
        cell.lblPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
        
        let imgURL = self.ImgProductGridURLs[indexPath.row]
        
        cell.ImgProductGrid.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
            // Your code inside completion block
            if image == nil {
                
            }
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let product_ID = ((self.arrProductDetail.object(at: indexPath.row) as! NSDictionary).value(forKey: "Id") as? String ?? "")
        let productDataDict = ["id":product_ID]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showProductDetail"), object: nil, userInfo: productDataDict)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.GridCollection.frame.width
        
        //        let jsonData = GlobalObjects.jsonString.data(using: .utf8)!
        //        let objConfig = try! JSONDecoder().decode(ConfigResponse.self, from: jsonData)
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            let cellWidth = ((width - 30)/2)
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                return CGSize(width:cellWidth, height: (cellWidth * 1.5) + 50)
            }else{
                return CGSize(width:((width - 30)/2), height: cellWidth + 50)
            }
        }else{
            let cellWidth = ((width - 15)/2)
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                
                return CGSize(width: cellWidth, height: (cellWidth * 1.5) + 50)
            }else{
                return CGSize(width: cellWidth, height: cellWidth + 50)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if (UIDevice.current.userInterfaceIdiom == .pad){
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }else{
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.heightCollectionview.constant = self.GridCollection.collectionViewLayout.collectionViewContentSize.height
            self.GridCollection.layoutIfNeeded()
        }
    }
}
