//
//  ProductDetailVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 05/03/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics
import Firebase


/** this class show the detail of the product and features like
 - Share product
 - Variants
 - Product images
 - Product description
 - Add/View cart
 */
class dropdownCell: UITableViewCell {
    
    @IBOutlet weak var lblVariantName: UILabel!
}

class ProductDetailVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblVariantTitle: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblProductDescription: UILabel!
    @IBOutlet weak var ImgFav: UIImageView!
    @IBOutlet weak var backViewAddToCart: UIView!
    @IBOutlet weak var ProductImageBackView: UIView!
    @IBOutlet weak var heightBackViewAddToCartConstraint: NSLayoutConstraint!
    @IBOutlet weak var multiplerProductImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnAddToCart: UIControl!
    @IBOutlet weak var lblAddToCart: UILabel!
    
    @IBOutlet weak var tblDropDownView: UITableView!
    @IBOutlet weak var heightTableDropDown: NSLayoutConstraint!
    @IBOutlet weak var btnTopCart: UIControl!
    @IBOutlet weak var ImgSliderCollection: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var BadgeView: UIView!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var SizeSelectionView: UIControl!
    @IBOutlet weak var heightSizeSelectionViewConstraint: NSLayoutConstraint!
    
    
    var Product_Id: String!
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    var arrVariants = [String]()
    var imgURL = [URL]()
    var visualEffectView: UIVisualEffectView!
    var Cart_DB = CartQueries()
    var Fav_DB = FavouriteQueries()
    var arrFavItems = [FavouriteItems]()
    
    
    var variant_id: String!
    var variant_title: String!
    var variant_type: String!
    var variant_value: String!
    var price:String!
    var currency_code: String!
    var quantity: Int!
    var Img_url: String!
    var isAddedToCart: Bool = false
    var email_Id = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
    var ImgHeight: Int!
    var variantImgHeight: Int!
    var storefrontProductDetails: Storefront.Product!
    var userInfo: [AnyHashable: Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.ImgSliderCollection.register(UINib(nibName: "ProductDetailImageSliderCell", bundle: nil), forCellWithReuseIdentifier: "ProductDetailImageSliderCell")
        
        if isFromNotification == true {
            if UserDefaults.standard.object(forKey: MyUserDefaults.NotificationPayload) != nil {
                self.userInfo = getMyUserDefaults(key: MyUserDefaults.NotificationPayload) as! [AnyHashable: Any]
                guard let actionType = self.userInfo["action"] as? String else {
                    return
                }
                if actionType.lowercased() == NotificationActions.open_product.rawValue.lowercased() {
                    guard let product_id = self.userInfo["id"] as? String else {
                    return
                }
                self.Product_Id = product_id
                }
            }
        }else if isFromDeepLink == true {
            if UserDefaults.standard.object(forKey: "DeepLinkProductID") != nil {
                var productId = getMyUserDefaults(key: "DeepLinkProductID") as? String
                productId = productId?.removingPercentEncoding
                productId = productId?.removingPercentEncoding
                self.Product_Id = productId?.removingPercentEncoding
            }
        }
    }
    
    
    @IBAction func btnAddFavorite(_ sender: Any) {

        if CHECKOUT_CUSTOMER_ACCOUNTS_STATUS.lowercased() == customerAccountStatus.disabled.rawValue.lowercased() {
                  
              let alert = UIAlertController(title: "", message: "msgAccountDisabled".localized, preferredStyle:.alert)
              
              alert.addAction(UIAlertAction(title: "btncancel".localized, style: .default, handler: { (alert) in
              }))
              
              alert.addAction(UIAlertAction(title: "btnOk".localized, style: .default, handler: { (alert) in
              }))
              
              DispatchQueue.main.async {
                  
                  self.present(alert, animated: true, completion: nil)
              }
          }else{
              if UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken) != nil {
                  
                  if self.arrFavItems.contains(where: {$0.variant_id == Product_Id}) {
                      
                      self.Fav_DB.deleteFavItem(variant_Id: Product_Id)
                      
                  }else{
                      
                      let title = self.storefrontProductDetails.title
                      
                      let price = "\(self.storefrontProductDetails.priceRange.minVariantPrice.amount)"
                      let currency_code = self.storefrontProductDetails.priceRange.minVariantPrice.currencyCode.rawValue
                      
                      let imageSlider = self.storefrontProductDetails.images.edges.map { $0.node }
                      
                      let Img_url = imageSlider[0].transformedSrc.absoluteString
                      self.Fav_DB.InsertFavItems(variant_id: Product_Id, user_email: self.email_Id!, title: title, price: price, currency_code: currency_code, Img_url: Img_url)
                      
                      FirebaseEventsLog.request.logWishListEvent(itemId: Product_Id, itemName: title)
                      
                  }
                  
                  self.arrFavItems = self.Fav_DB.GetFavByUserId(user_email: self.email_Id!)
                  self.checkForFav()
                  
              }else{
                  
                  let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                  let detailVC = storyBoard.instantiateViewController(withIdentifier: "LoginRegistrationVC") as? LoginRegistrationVC
                  //            self.navigationController?.pushViewController(detailVC!, animated: true)
                  self.present(detailVC!, animated: true, completion: nil)
              }
          }
        
        
        
        
    }
    
    override func updateViewConstraints() {
        
        let tbleHeight = tblDropDownView.contentSize.height
        
        if tbleHeight >= 280 {
            heightTableDropDown.constant = 280
        }else{
            heightTableDropDown.constant = tblDropDownView.contentSize.height
        }
        self.tblDropDownView.layoutIfNeeded()
        self.tblDropDownView.reloadData()
        
        super.updateViewConstraints()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        FirebaseEventsLog.request.trackScreen(screenName: "Product detail", className: "ProductDetailVC")
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            let newConstraint = self.multiplerProductImageConstraint.constraintWithMultiplier(1.0)
            self.ProductImageBackView.removeConstraint(self.multiplerProductImageConstraint)
            self.ProductImageBackView.addConstraint(newConstraint)
            self.ProductImageBackView.layoutIfNeeded()
            self.multiplerProductImageConstraint = newConstraint
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                
                self.ImgHeight = GlobalObjects.iPad_Grid_Portrait_height
                self.variantImgHeight = 600
            }else{
                
                self.ImgHeight = GlobalObjects.iPad_Grid_Square_height
                self.variantImgHeight = 400
            }
        }else{
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPhone_Grid_Portrait_height
                self.variantImgHeight = 600
            }else{
                self.ImgHeight = GlobalObjects.iPhone_Grid_Square_height
                self.variantImgHeight = 400
            }
        }
        
        self.lblAddToCart.font = UIFont.appSemiBoldFontWith(size: AppFontSize.size17)
        self.lblProductName.font = UIFont.appSemiBoldFontWith(size: AppFontSize.size16)
        self.lblProductPrice.font = UIFont.appRegularFontWith(size: AppFontSize.size15)
        self.lblProductDescription.font = UIFont.appRegularFontWith(size: AppFontSize.size15)
        self.btnAddToCart.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblAddToCart.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
        self.pageControl.currentPage = 0
        self.pageControl.pageIndicatorTintColor = UIColor.gray
        self.pageControl.currentPageIndicatorTintColor = UIColor.black
        self.tblDropDownView.isHidden = true
        self.tblDropDownView.tableFooterView = UIView()
        
        let cartitems  = Cart_DB.GetAllCartItems()
        
        var totalQty: Int! = 0
        if cartitems.count != 0 {
            for item in cartitems {
                totalQty += item.quantity
            }
            self.BadgeView.isHidden = false
            self.BadgeView.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
            self.lblCartCount.text! = String(totalQty)
            self.lblCartCount.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
        }else{
            
            self.BadgeView.isHidden = true
        }
        
        self.isAddedToCart = false
        if UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail) != nil {
            self.email_Id = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
            self.arrFavItems = self.Fav_DB.GetFavByUserId(user_email: self.email_Id!)
        }
        self.fetchProductDetails(productID: self.Product_Id!, images: 50, variants: 50, ImgHeight: Int32(self.ImgHeight), variantImgHeight: Int32(self.variantImgHeight))
        self.checkForFav()
        
    }
    
    func checkForFav(){
        
        if self.arrFavItems.contains(where: {$0.variant_id == Product_Id}) {
            self.ImgFav.image = #imageLiteral(resourceName: "fav_filled")
            self.ImgFav.image = self.ImgFav.image!.withRenderingMode(.alwaysTemplate)
            self.ImgFav.tintColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        }else{
            self.ImgFav.image = #imageLiteral(resourceName: "fav_line")
            self.ImgFav.image = self.ImgFav.image!.withRenderingMode(.alwaysTemplate)
            self.ImgFav.tintColor = UIColor.gray
        }
    }
    
    func fetchProductDetails(productID: String, images: Int32,variants: Int32, ImgHeight: Int32, variantImgHeight: Int32){
        
        let Id = GraphQL.ID(rawValue: "\(String(describing: productID))")
        self.storefrontProductDetails = nil
        ShopifyProductDetails.shared.fetchProductDetails(id: Id, images: images, variants: variants, ImgHeight: ImgHeight, variantImgHeight: variantImgHeight) { product, error in
            if let product = product {
                self.storefrontProductDetails = product
                self.setDetailContent()
            }else{
                self.showAlertWithAction("", msg: error!, button1: "btnOk".localized) { isClicked in
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func setDetailContent(){
        
        FirebaseEventsLog.request.logViewItemEvent(product: self.storefrontProductDetails)
        self.arrVariants.removeAll()
        self.imgURL.removeAll()
        
        self.lblProductName.text! = self.storefrontProductDetails.title
        
        let price = self.storefrontProductDetails.priceRange.minVariantPrice.amount
        let currency = self.storefrontProductDetails.priceRange.minVariantPrice.currencyCode.rawValue
        
        let locale = Locale.getSymbolForCurrencyCode(code: currency)
        self.lblProductPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
        
        self.lblProductDescription.setHTMLFromString(htmlText: self.storefrontProductDetails.descriptionHtml)

        if self.storefrontProductDetails.availableForSale == false {
            self.btnAddToCart.alpha = 0.5
            self.btnAddToCart.isUserInteractionEnabled = false
            self.lblAddToCart.text! = "btnOutOfStock".localized
        }
       
            let prodOptions = self.storefrontProductDetails.options
            
            if prodOptions.count > 1 {
                
                self.lblVariantTitle.text! = "SELECT \(prodOptions[0].name.uppercased()) / \(prodOptions[1].name.uppercased())"
                self.arrVariants.append("\(self.lblVariantTitle.text!)")
                
            }else {
                
                self.lblVariantTitle.text! = "SELECT \(prodOptions[0].name.uppercased())"
                self.arrVariants.append("\(self.lblVariantTitle.text!)")
            }
            
            let variants = self.storefrontProductDetails.variants.edges.map { $0.node }
            
            for item in variants {
                if item.title != "Default Title" {
                    self.arrVariants.append(item.title.TrimString(item.title))
                }
            }
        
        if self.arrVariants.count <= 1 {
            self.SizeSelectionView.isHidden = true
            self.heightSizeSelectionViewConstraint.constant = 0.0
        }
       
        let imageSlider = self.storefrontProductDetails.images.edges.map { $0.node }
        
        for index in 0..<imageSlider.count {
            self.imgURL.append(imageSlider[index].originalSrc)
        }
        
        self.pageControl.numberOfPages = self.imgURL.count
        self.ImgSliderCollection.reloadData()
    }
        
    
    func setAddtoCartStatus(isAvailable:Bool!){
        if !isAvailable {
            self.btnAddToCart.alpha = 0.5
            self.btnAddToCart.isUserInteractionEnabled = false
            self.lblAddToCart.text! = "btnOutOfStock".localized
            
        }else{
            self.btnAddToCart.alpha = 1.0
            self.btnAddToCart.isUserInteractionEnabled = true
            self.lblAddToCart.text! = "btnaddtocart".localized
        }
    }
    
    @IBAction func btnAddToCartTapped(_ sender: Any) {
        if self.arrVariants.count == 1 {
            
            let variants = self.storefrontProductDetails.variants.edges.map { $0.node }
            
            self.variant_id = variants[0].id.rawValue
            self.variant_title = self.storefrontProductDetails.title
           
            self.price = "\(variants[0].price)"
            self.currency_code = self.storefrontProductDetails.priceRange.minVariantPrice.currencyCode.rawValue
            self.quantity = 1
            self.Img_url = variants[0].image!.transformedSrc.absoluteString
            
            
            let isItem = self.Cart_DB.GetCartItemById(variant_Id: self.variant_id!)
            
            if isItem.count == 0 {
                self.Cart_DB.InsertCartItem(variant_id: self.variant_id!, title: self.variant_title!, variant_type: "", variant_value: "", price: self.price!, currency_code: self.currency_code!, quantity: self.quantity!, Img_url: self.Img_url!)
            }else{
                var item_qty = isItem[0].quantity
                item_qty += 1
                self.Cart_DB.updateCartItem(item_qty, price: self.price!, variant_id: self.variant_id!)
            }
            
            Messaging.messaging().subscribe(toTopic: "cart")
            
            FirebaseEventsLog.request.logCartEvent(itemId: self.variant_id!, itemName: self.variant_title!, itemVariant: "", price: self.price!, currency: self.currency_code!)
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let detailVC = storyBoard.instantiateViewController(withIdentifier: "CartNavigationController") as? CartNavigationController {
                self.navigationController?.present(detailVC, animated: true, completion: nil)
            }
            
            return
        }
        if self.lblVariantTitle.text! != self.arrVariants[0] {
            
            let isItem = self.Cart_DB.GetCartItemById(variant_Id: self.variant_id!)
            
            if isItem.count == 0 {
                self.Cart_DB.InsertCartItem(variant_id: self.variant_id!, title: self.variant_title!, variant_type: self.variant_type!, variant_value: self.variant_value!, price: self.price!, currency_code: self.currency_code!, quantity: self.quantity!, Img_url: self.Img_url!)
            }else{
                var item_qty = isItem[0].quantity
                item_qty += 1
                self.Cart_DB.updateCartItem(item_qty, price: self.price!, variant_id: self.variant_id!)
            }
            
            Messaging.messaging().subscribe(toTopic: "cart")
            FirebaseEventsLog.request.logCartEvent(itemId: self.variant_id!, itemName: self.variant_title!, itemVariant: self.variant_type!, price: self.price!, currency: self.currency_code!)
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let detailVC = storyBoard.instantiateViewController(withIdentifier: "CartNavigationController") as? CartNavigationController {
                self.navigationController?.present(detailVC, animated: true, completion: nil)
            }
            
        }else{
            
            self.isAddedToCart = true
            self.showVariantLists()
        }
        
    }
    
    @IBAction func btnTopCartTapped(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let detailVC = storyBoard.instantiateViewController(withIdentifier: "CartNavigationController") as? CartNavigationController {
            self.navigationController?.present(detailVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        
        if let actionType = self.userInfo["action"] as? String{
            if actionType.lowercased() == NotificationActions.open_product.rawValue.lowercased() {
                if isFromNotification == true {
                    isFromNotification = false
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let rootView = storyBoard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
                    UIApplication.shared.windows.first?.rootViewController = rootView
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                }
            }else{
            navigationController?.popViewController(animated: true)
            }

        }else if isFromDeepLink == true{
            isFromDeepLink = false
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let rootView = storyBoard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
            UIApplication.shared.windows.first?.rootViewController = rootView
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func SelectVariantTapped(_ sender: Any) {
        
        self.showVariantLists()
        
    }
    
    
    @IBAction func btnShareDeepLinking(_ sender: Any) {
        var titleTemp = self.storefrontProductDetails.title
        titleTemp = titleTemp.replacingOccurrences(of: " - ", with: "-")
        titleTemp = titleTemp.replacingOccurrences(of: "  ", with: "-")
        titleTemp = titleTemp.replacingOccurrences(of: " ", with: "-")

        guard let link = URL(string: "https://\(shopDomain)/products/\(titleTemp)?product_id=\(self.Product_Id!)") else { return }
        let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)

        let bundleID = Bundle.main.bundleIdentifier
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleID!)
        linkBuilder!.iOSParameters!.appStoreID = appStoreID

        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: bundleID!)

        guard let longDynamicLink = linkBuilder?.url else { return }
        print("The long URL is: \(longDynamicLink)")
        
        
        linkBuilder!.shorten() { url, warnings, error in
            if let url = url {
                
                let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
     
                if let popoverController = activityViewController.popoverPresentationController {
                    popoverController.sourceView = self.view
                    popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                }

                self.present(activityViewController, animated: true, completion: nil)
            }else{
                print("error generating deep link:\(error?.localizedDescription)")
            }

        }
          
    }
    
    func showVariantLists(){
        
        if #available(iOS 13.0, *) {
            self.visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .systemChromeMaterialDark))
        } else {
            // Fallback on earlier versions
            self.visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .prominent))
        }
        self.visualEffectView.backgroundColor = UIColor.black
        self.visualEffectView.alpha = 0.2
        self.visualEffectView.frame = self.view.bounds
        self.visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //        visualEffectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundViewTapped)))
        self.view.insertSubview(self.visualEffectView, at: 3)
        
        self.visualEffectView.isUserInteractionEnabled = true
        self.visualEffectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertClose(gesture:))))
        self.tblDropDownView.isHidden = false
        self.tblDropDownView.reloadData()
        self.updateViewConstraints()
    }
    
    @objc func alertClose(gesture: UITapGestureRecognizer){
        self.tblDropDownView.isHidden = true
        self.visualEffectView.removeFromSuperview()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

extension ProductDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.arrVariants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell1 = tblDropDownView.dequeueReusableCell(withIdentifier: "dropdownCell", for: indexPath) as! dropdownCell
        
        cell1.lblVariantName.text! = self.arrVariants[indexPath.row]
        cell1.lblVariantName.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
        return cell1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.isAddedToCart == false {
            
            if indexPath.row != 0 {
                
                let variants = self.storefrontProductDetails.variants.edges.map { $0.node }
                
                self.variant_id = variants[indexPath.row - 1].id.rawValue
                self.variant_title = self.storefrontProductDetails.title
                
                let prodOptions = self.storefrontProductDetails.options
                if prodOptions.count > 1 {
                    self.variant_type = "\(prodOptions[0].name.uppercased()) / \(prodOptions[1].name.uppercased())"
                }else {
                    self.variant_type = "\(prodOptions[0].name.uppercased())"
                }
                
                self.variant_value = self.arrVariants[indexPath.row]
                self.price = "\(variants[indexPath.row - 1].price)"
                self.currency_code = self.storefrontProductDetails.priceRange.minVariantPrice.currencyCode.rawValue
                self.quantity = 1
                self.Img_url = variants[indexPath.row - 1].image!.transformedSrc.absoluteString
                
                self.lblVariantTitle.text! = self.variant_value
                let locale = Locale.getSymbolForCurrencyCode(code: self.currency_code)
                self.lblProductPrice.text! = "\(locale) \(self.price!)"
                
                self.setAddtoCartStatus(isAvailable: variants[indexPath.row - 1].availableForSale)
                
                self.imgURL.removeAll()
                self.imgURL.append(variants[indexPath.row - 1].image!.originalSrc)
                
                self.ImgSliderCollection.reloadData()
                
            }else {
                
                self.setDetailContent()
            }
            
            UIView.animate(withDuration: 0.2, animations: {
                self.tblDropDownView.isHidden = true
                self.visualEffectView.removeFromSuperview()
            })
            
        }else{
            
            self.isAddedToCart = false
            if indexPath.row != 0 {
                
                let variants = self.storefrontProductDetails.variants.edges.map { $0.node }
                self.variant_id = variants[indexPath.row - 1].id.rawValue
                self.variant_title = self.storefrontProductDetails.title
                let prodOptions = self.storefrontProductDetails.options
                if prodOptions.count > 1 {
                    self.variant_type = "\(prodOptions[0].name.uppercased()) / \(prodOptions[1].name.uppercased())"
                }else {
                    self.variant_type = "\(prodOptions[0].name.uppercased())"
                }
                self.variant_value = self.arrVariants[indexPath.row]
                self.price = "\(variants[indexPath.row - 1].price)"
                self.currency_code = self.storefrontProductDetails.priceRange.minVariantPrice.currencyCode.rawValue
                self.quantity = 1
                
                self.lblVariantTitle.text! = self.variant_value
                let locale = Locale.getSymbolForCurrencyCode(code: self.currency_code)
                self.lblProductPrice.text! = "\(locale) \(self.price!)"
                
                self.Img_url = variants[indexPath.row - 1].image!.transformedSrc.absoluteString
                
                UIView.animate(withDuration: 0.2, animations: {
                    self.tblDropDownView.isHidden = true
                    self.visualEffectView.removeFromSuperview()
                    
                })
                self.setAddtoCartStatus(isAvailable: variants[indexPath.row - 1].availableForSale)
                if variants[indexPath.row - 1].availableForSale == true {
                    
                    let isItem = self.Cart_DB.GetCartItemById(variant_Id: self.variant_id!)
                    
                    if isItem.count == 0 {
                        
                        self.Cart_DB.InsertCartItem(variant_id: self.variant_id!, title: self.variant_title!, variant_type: self.variant_type!, variant_value: self.variant_value!, price: self.price!, currency_code: self.currency_code!, quantity: self.quantity!, Img_url: self.Img_url!)
                        
                    }else{
                        
                        var item_qty = isItem[0].quantity
                        item_qty += 1
                        self.Cart_DB.updateCartItem(item_qty, price: self.price!, variant_id: self.variant_id!)
                    }
                    Messaging.messaging().subscribe(toTopic: "cart")
                    FirebaseEventsLog.request.logCartEvent(itemId: self.variant_id!, itemName: self.variant_title!, itemVariant: self.variant_type!, price: self.price!, currency: self.currency_code!)
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    if let detailVC = storyBoard.instantiateViewController(withIdentifier: "CartNavigationController") as? CartNavigationController {
                        self.navigationController?.present(detailVC, animated: true, completion: nil)
                    }
                }
            }else{
                
                self.setDetailContent()
            }
            
            UIView.animate(withDuration: 0.2, animations: {
                self.tblDropDownView.isHidden = true
                self.visualEffectView.removeFromSuperview()
            })
            //Add item to Database and show cart screen
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension ProductDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = ImgSliderCollection.contentOffset
        visibleRect.size = ImgSliderCollection.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = ImgSliderCollection.indexPathForItem(at: visiblePoint) else { return }
        
        if self.imgURL.count > 1 {
            self.pageControl.isHidden = false
            self.pageControl.numberOfPages = self.imgURL.count
            self.pageControl.currentPage = indexPath.row
            self.pageControl.pageIndicatorTintColor = UIColor.gray
            self.pageControl.currentPageIndicatorTintColor = UIColor.black
        }else{
            
            self.pageControl.isHidden = true
        }
        self.ImgSliderCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.imgURL.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = ImgSliderCollection.dequeueReusableCell(withReuseIdentifier:"ProductDetailImageSliderCell", for: indexPath) as! ProductDetailImageSliderCell
        let imgURL = self.imgURL[indexPath.row]
        
        if self.imgURL.count > 1 {
            self.pageControl.isHidden = false
        }else{
            
            self.pageControl.isHidden = true
        }
        cell.ImageSliders.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
            // Your code inside completion block
            if image == nil {
                
            }
        })
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.ImgSliderCollection.frame.width
        let height = self.ImgSliderCollection.frame.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}

extension UILabel {
    func setHTMLFromString(htmlText: String) {
        let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(self.font!.pointSize)\">%@</span>", htmlText)
        
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        self.attributedText = attrStr
    }
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}
