//
//  ProductListVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 11/02/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics


/** This class is providing the products list of collection and features like sort and favourite */

class ProductListVC: UIViewController, CustomButtonAlertVCDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var ProductCollection: UICollectionView!
    @IBOutlet weak var BageView: UIView!
    
    @IBOutlet weak var lblSort: UILabel!
    
    var ImgCollections = [URL]()
    
    fileprivate let columns:  Int = 2
    var collectionObj: Storefront.Collection?
    var CollectionID: String? = ""
    var Cursor: String?
    var ScreenTitle: String! = ""
    var sort_type: Storefront.ProductCollectionSortKeys?
    var reverse: Bool!
    var Cart_DB = CartQueries()
    var Fav_DB = FavouriteQueries()
    var arrFavItems = [FavouriteItems]()
    var storefrontProducts = [Storefront.Product]()
    var visualEffectView: UIVisualEffectView!
    var ImgHeight: Int!
    
    
    //Delegate action method for sorting
    
    func customActionPerformed(itemAction: String) {
        
        if itemAction == "latest".localized {
            self.Cursor = nil
            self.reverse = true
            self.sort_type = Storefront.ProductCollectionSortKeys.created
            self.storefrontProducts.removeAll()
            self.ProductCollection.isHidden = true
            self.getProductsGraphQLQuery(cursor: self.Cursor, products: 10, variants: 50, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.ProductCollection.reloadData()
            
        }else if itemAction == "bestsell".localized {
            self.Cursor = nil
            self.reverse = false
            self.sort_type = Storefront.ProductCollectionSortKeys.bestSelling
            self.storefrontProducts.removeAll()
            self.ProductCollection.isHidden = true
            self.getProductsGraphQLQuery(cursor: self.Cursor, products: 10, variants: 50, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.ProductCollection.reloadData()
            
        }else if itemAction == "pricelowtohigh".localized {
            self.Cursor = nil
            self.reverse = false
            self.sort_type = Storefront.ProductCollectionSortKeys.price
            self.storefrontProducts.removeAll()
            self.ProductCollection.isHidden = true
            self.getProductsGraphQLQuery(cursor: self.Cursor, products: 10, variants: 50, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.ProductCollection.reloadData()
            
        }else if itemAction == "pricehightolow".localized {
            self.Cursor = nil
            self.reverse = true
            self.sort_type = Storefront.ProductCollectionSortKeys.price
            self.storefrontProducts.removeAll()
            self.ProductCollection.isHidden = true
            self.getProductsGraphQLQuery(cursor: self.Cursor, products: 10, variants: 50, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.ProductCollection.reloadData()
            
        }else if itemAction == "titleatoz".localized {
            self.Cursor = nil
            self.reverse = false
            self.sort_type = Storefront.ProductCollectionSortKeys.title
            self.storefrontProducts.removeAll()
            self.ProductCollection.isHidden = true
            self.getProductsGraphQLQuery(cursor: self.Cursor, products: 10, variants: 50, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.ProductCollection.reloadData()
            
        }else if itemAction == "titleztoa".localized {
            self.Cursor = nil
            self.reverse = true
            self.sort_type = Storefront.ProductCollectionSortKeys.title
            self.storefrontProducts.removeAll()
            self.ProductCollection.isHidden = true
            self.getProductsGraphQLQuery(cursor: self.Cursor, products: 10, variants: 50, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.ProductCollection.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWhenAPNSCall(_:)), name: NSNotification.Name(rawValue: "reloadTable"), object: nil)
        
        self.ProductCollection.register(UINib(nibName: "GridCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GridCollectionCell")
        self.ProductCollection.register(UINib(nibName: "EmptyMessageCell", bundle: nil), forCellWithReuseIdentifier: "EmptyMessageCell")
        
        if isFromNotification == true {
            if UserDefaults.standard.object(forKey: MyUserDefaults.NotificationPayload) != nil {
                let userInfo = getMyUserDefaults(key: MyUserDefaults.NotificationPayload) as! [AnyHashable: Any]
                guard let actionType = userInfo["action"] as? String else {
                    return
                }
                if actionType.lowercased() == NotificationActions.open_collection.rawValue.lowercased() {
                guard let collection_id = userInfo["id"] as? String else {
                    return
                }
                self.CollectionID! = collection_id
                }
            }
        }
            
        self.lblScreenTitle.text! = self.ScreenTitle
        self.Cursor = nil
        self.sort_type = Storefront.ProductCollectionSortKeys.created
        self.reverse = true
        self.storefrontProducts.removeAll()
      
        if (UIDevice.current.userInterfaceIdiom == .pad){
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPad_Grid_Portrait_height
            }else{
                self.ImgHeight = GlobalObjects.iPad_Grid_Square_height
            }
        }else{
            
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPhone_Grid_Portrait_height
            }else{
                self.ImgHeight = GlobalObjects.iPhone_Grid_Square_height
            }
        }
        
        self.ProductCollection.isHidden = true
        self.getProductsGraphQLQuery(cursor: self.Cursor, products: 20, variants: 50, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
        
        self.ProductCollection.cr.addHeadRefresh(animator:FastAnimator() as CRRefreshProtocol) {
            [weak self] in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                
//                self?.Cursor = nil
//                self?.sort_type = Storefront.ProductCollectionSortKeys.created
//                self?.reverse = true
//                self?.storefrontProducts.removeAll()
                self?.getProductsGraphQLQuery(cursor: self?.Cursor, products: 20, variants: 50, sort_Type: self?.sort_type, reverse: self?.reverse, ImgHeight: Int32((self?.ImgHeight)!))
            })
        }
    }
    
    @objc func reloadWhenAPNSCall(_ notification: NSNotification) {
        
        self.ProductCollection.reloadData()
        self.view.layoutIfNeeded()
    }
    
    @IBAction func btnSortTapped(_ sender: Any) {
        
        if self.storefrontProducts.count != 0 {
            
            let myAlert = self.storyboard!.instantiateViewController(withIdentifier: "CustomButtonAlertVC") as! CustomButtonAlertVC
            myAlert.delegate = self
            myAlert.SortType = self.sort_type
            myAlert.reverse = self.reverse
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(myAlert, animated: true) {
                myAlert.view.superview?.isUserInteractionEnabled = true
                myAlert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertClose(gesture:))))
            }
            
        }
    }
    
    @objc func alertClose(gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCartTapped(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let detailVC = storyBoard.instantiateViewController(withIdentifier: "CartNavigationController") as? CartNavigationController {
            self.navigationController?.present(detailVC, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        FirebaseEventsLog.request.trackScreen(screenName: "Product list", className: "ProductListVC")
        self.lblScreenTitle.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.lblSort.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPad_Grid_Portrait_height
            }else{
                self.ImgHeight = GlobalObjects.iPad_Grid_Square_height
            }
        }else{
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPhone_Grid_Portrait_height
            }else{
                self.ImgHeight = GlobalObjects.iPhone_Grid_Square_height
            }
        }
        
        let cartitems  = Cart_DB.GetAllCartItems()
        var totalQty: Int! = 0
        if cartitems.count != 0 {
            for item in cartitems {
                
                totalQty += item.quantity
            }
            
            self.BageView.isHidden = false
            self.BageView.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
            self.lblCartCount.text! = String(totalQty)
            self.lblCartCount.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
            
        }else{
            
            self.BageView.isHidden = true
        }
        
        self.arrFavItems.removeAll()
        if UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail) != nil {
            let email_Id = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
            self.arrFavItems = self.Fav_DB.GetFavByUserId(user_email: email_Id!)
        }
        self.ProductCollection.reloadData()
    }
    
    //Method to call GraphQL queries to fetch the list of products from store.
    
    func getProductsGraphQLQuery(cursor: String?, products: Int32,variants: Int32, sort_Type:Storefront.ProductCollectionSortKeys?, reverse: Bool?, ImgHeight: Int32) {
        
//        self.ProductCollection.isHidden = true
        
        ShopifyProductListData.shared.getProductsGraphQLQuery(id: GraphQL.ID(rawValue: "\(String(describing: CollectionID!))"), cursor: cursor, products: products, variants: variants, sort_Type: sort_Type, reverse: reverse, ImgHeight: ImgHeight) { collection, error in
            
            if let collectionObj = collection {
                self.collectionObj = collectionObj
                self.lblScreenTitle.text! = (self.collectionObj?.title)!
                let storeProducts = collectionObj.products.edges.map { $0.node }
                self.storefrontProducts.append(contentsOf: storeProducts)
                
                DispatchQueue.main.async {
                    
                    self.ProductCollection.cr.endHeaderRefresh()
                    self.ProductCollection.cr.resetNoMore()
                    self.ProductCollection.isHidden = false
                    self.ProductCollection.reloadData()
                    
                }
                
                self.storefrontProducts.forEach { product in
                    let images   = product.images.edges.map { $0.node }
                    images.forEach { image in
                        
                        let imageUrl = image.transformedSrc
                        self.ImgCollections.append(imageUrl)
                        
                    }
                }
                
            }else{
                self.Showalert1(message: error!)
            }
        }
    }

    @IBAction func btnBackTapped(_ sender: Any) {
        if isFromNotification == true {
            isFromNotification = false
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let rootView = storyBoard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
            UIApplication.shared.windows.first?.rootViewController = rootView
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }else{
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - FAV BUTTON ACTION
    
    @objc func addToFavorite(sender: AnyObject) -> () {
        print(sender.tag!)
        
        var superview = sender.superview
        while let view = superview, !(view is UICollectionViewCell) {
            superview = view?.superview
        }
        guard let cell = superview as? UICollectionViewCell else {
            print("button is not contained in a table view cell")
            return
        }
        
        guard let indexPath = ProductCollection.indexPath(for: cell) else {
            print("failed to get index path for cell containing button")
            return
        }
        
        if CHECKOUT_CUSTOMER_ACCOUNTS_STATUS.lowercased() == customerAccountStatus.disabled.rawValue.lowercased() {
                   
           let alert = UIAlertController(title: "", message: "msgAccountDisabled".localized, preferredStyle:.alert)
           
           alert.addAction(UIAlertAction(title: "btncancel".localized, style: .default, handler: { (alert) in
           }))
           
           alert.addAction(UIAlertAction(title: "btnOk".localized, style: .default, handler: { (alert) in
           }))
           
           DispatchQueue.main.async {
               
               self.present(alert, animated: true, completion: nil)
           }
       }else{
            if UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken) != nil {
                
                let variant_id = self.storefrontProducts[sender.tag!].id.rawValue
                let prod_title = self.storefrontProducts[sender.tag!].title
                let variant = self.storefrontProducts[sender.tag!].variants.edges.map { $0.node}
                
                let price  = "\(variant[0].price)"
                //            print("variant price:", price)
                
                let currency_code = self.storefrontProducts[sender.tag].priceRange.minVariantPrice.currencyCode.rawValue
                let images = self.storefrontProducts[sender.tag].images.edges.map { $0.node}
                let imgURL = images[0].transformedSrc.absoluteString
                
                let email_Id = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
                
                if self.arrFavItems.contains(where: {$0.variant_id == variant_id}) {
                    self.Fav_DB.deleteFavItem(variant_Id: variant_id)
                }else{
                    self.Fav_DB.InsertFavItems(variant_id: variant_id, user_email: email_Id!, title: prod_title, price: price, currency_code: currency_code, Img_url: imgURL)
                    
                    FirebaseEventsLog.request.logWishListEvent(itemId: variant_id, itemName: prod_title)
                }
                self.arrFavItems = self.Fav_DB.GetFavByUserId(user_email: email_Id!)
                self.ProductCollection.reloadData()
                
            }else{
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let detailVC = storyBoard.instantiateViewController(withIdentifier: "LoginRegistrationVC") as? LoginRegistrationVC
                self.present(detailVC!, animated: true, completion: nil)
            }
        }
    }
}

extension ProductListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.storefrontProducts.count != 0 {
        return self.storefrontProducts.count
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.storefrontProducts.count != 0 {
        let cell = ProductCollection.dequeueReusableCell(withReuseIdentifier:"GridCollectionCell", for: indexPath) as! GridCollectionCell
        cell.lblItemName.text! = self.storefrontProducts[indexPath.row].title
        
        
        let price = self.storefrontProducts[indexPath.row].priceRange.minVariantPrice.amount
        let currency = self.storefrontProducts[indexPath.row].priceRange.minVariantPrice.currencyCode.rawValue
        
        
        let locale = Locale.getSymbolForCurrencyCode(code: currency)
        cell.lblPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
        
        let Id = self.storefrontProducts[indexPath.row].id.rawValue
        
        if self.arrFavItems.contains(where: {$0.variant_id == Id}) {
            
            cell.ImgFav.image = #imageLiteral(resourceName: "fav_filled")
            cell.ImgFav.image = cell.ImgFav.image!.withRenderingMode(.alwaysTemplate)
            cell.ImgFav.tintColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
            
        }else{
            
            cell.ImgFav.image = #imageLiteral(resourceName: "fav_line")
            cell.ImgFav.image = cell.ImgFav.image!.withRenderingMode(.alwaysTemplate)
            cell.ImgFav.tintColor = UIColor.gray
        }
        
        cell.btnFavorite.isHidden = false
        cell.widthFavouritConstraint.constant = 30
        cell.btnFavorite.tag = indexPath.row
        cell.btnFavorite.addTarget(self, action: #selector(self.addToFavorite(sender:)), for: .touchUpInside)
        
        let images = self.storefrontProducts[indexPath.row].images.edges.map { $0.node}
        images.forEach { image in
            
            let imgURL = image.transformedSrc
            
            
            cell.ImgProductGrid.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                // Your code inside completion block
                if image == nil {
                    
                }
            })
            
        }
        
        return cell
        }else{
            let cell = ProductCollection.dequeueReusableCell(withReuseIdentifier:"EmptyMessageCell", for: indexPath) as! EmptyMessageCell
            cell.lblMessage.text! = "empty_search_msg".localized
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyBoard.instantiateViewController(withIdentifier: "ProductDetailVC") as? ProductDetailVC
        detailVC?.Product_Id = self.storefrontProducts[indexPath.row].id.rawValue
        self.navigationController?.pushViewController(detailVC!, animated: true)
        //        self.present(detailVC!, animated: true, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.ProductCollection.frame.width
        if self.storefrontProducts.count != 0 {
        if (UIDevice.current.userInterfaceIdiom == .pad){
            let cellWidth = ((width - 30)/2)
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                return CGSize(width:cellWidth, height: (cellWidth * 1.5) + 50)
            }else{
                return CGSize(width:((width - 30)/2), height: cellWidth + 50)
            }
        }else{
            let cellWidth = ((width - 15)/2)
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                
                return CGSize(width: cellWidth, height: (cellWidth * 1.5) + 50)
            }else{
                return CGSize(width: cellWidth, height: cellWidth + 50)
            }
        }
        }else{
            return CGSize(width: width , height: self.ProductCollection.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if (UIDevice.current.userInterfaceIdiom == .pad){
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }else{
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if self.storefrontProducts.count != 0 {
            
            self.Cursor = self.collectionObj?.products.edges.last?.cursor
            let hasNextPage =  self.collectionObj?.products.pageInfo.hasNextPage
            
            if hasNextPage! && indexPath.row == self.storefrontProducts.count - 1 {
                self.getProductsGraphQLQuery(cursor: self.Cursor, products: 20, variants: 50, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
                
            }
        }
        
    }
}
