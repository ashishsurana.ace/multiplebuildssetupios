//
//  ShopifyProductListData.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/06/21.
//

import Foundation
import MobileBuySDK

/**Class to provide the send product lists data fetched by the GraphQL*/
class ShopifyProductListData {
    
    static let shared = ShopifyProductListData()
    
    
    func getProductsGraphQLQuery(id: GraphQL.ID, cursor: String?, products: Int32,variants: Int32, sort_Type:Storefront.ProductCollectionSortKeys?, reverse: Bool?, ImgHeight: Int32, completion: @escaping (Storefront.Collection?, String?) -> Void) {
        
        QueryFetchProductsLists.shared.fetchProductListData(id: id, cursor: cursor, products: products, variants: variants, sort_Type: sort_Type, reverse: reverse, ImgHeight: ImgHeight) { collection, error in
            if let collection = collection {
                
                completion(collection, nil)
                
            }else{
                
                let error = "errorMsg".localized
                completion(nil, error)
            }
        }
    }
    
}
