//
//  ShopifyProductDetails.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/06/21.
//

import Foundation
import MobileBuySDK

/**Class to provide the send product detail data fetched by the GraphQL*/

class ShopifyProductDetails {
    
    static let shared = ShopifyProductDetails()
    var storefrontProductDetails: Storefront.Product!
    
    func fetchProductDetails(id: GraphQL.ID, images: Int32,variants: Int32, ImgHeight: Int32, variantImgHeight: Int32, completion: @escaping (Storefront.Product?, String?) -> Void){
        QueryFetchProductDetail.shared.fetchProductDetail(id: id, images: images, variants: variants, ImgHeight: ImgHeight, variantImgHeight: variantImgHeight) { product, error in
            if let product = product {
                self.storefrontProductDetails = product
                completion(self.storefrontProductDetails, nil)
            }else{
                let error = "errorMsg".localized
                completion(nil, error)
            }
        }
    }
}
