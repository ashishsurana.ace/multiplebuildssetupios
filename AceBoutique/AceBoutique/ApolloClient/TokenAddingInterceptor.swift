//
//  TokenAddingInterceptor.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 22/04/21.
//

import UIKit
import Apollo

struct Network {
    static var request = Network()
    private(set) lazy var apollo: ApolloClient = {

        
        let store1 = ApolloStore()
        
        let authPayloads = ["X-Shopify-Access-Token": adminToken,"Content-Type": "application/json"]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = authPayloads
        
        let client1 = URLSessionClient(sessionConfiguration: configuration, callbackQueue: nil)
        let provider = NetworkInterceptorProvider(client: client1, shouldInvalidateClientOnDeinit: true, store: store1)
    
        let url = URL(string: apolloGraphQLUrl)!
        
        let requestChainTransport = RequestChainNetworkTransport(interceptorProvider: provider,
                                                                 endpointURL: url)
        store1.clearCache()
        return ApolloClient(networkTransport: requestChainTransport,
                            store: store1)
    }()
}


class NetworkInterceptorProvider: LegacyInterceptorProvider {
    
override func interceptors<Operation: GraphQLOperation>(for operation: Operation) -> [ApolloInterceptor] {
    var interceptors = super.interceptors(for: operation)
    interceptors.insert(CustomInterceptor(), at: 0)
    return interceptors
}
}

class CustomInterceptor: ApolloInterceptor {

    func interceptAsync<Operation: GraphQLOperation>(
        chain: RequestChain,
        request: HTTPRequest<Operation>,
        response: HTTPResponse<Operation>?,
        completion: @escaping (Swift.Result<GraphQLResult<Operation.Data>, Error>) -> Void) {
        request.addHeader(name: "X-Shopify-Access-Token", value: adminToken)
        request.addHeader(name: "Content-Type", value: "application/json")

        request.addHeader(name: "Accept-Language", value: localeId!)
       
        
        print("request :\(request)")
        print("response :\(String(describing: response))")
        
        chain.proceedAsync(request: request,
                           response: response,
                           completion: completion)
    }
}

