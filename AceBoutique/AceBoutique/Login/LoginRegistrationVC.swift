//
//  LoginRegistrationVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 15/03/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics

class LoginRegistrationVC: UIViewController, UITextFieldDelegate, ForgotPasswordVCDelegate {
        
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var heightFirstName: NSLayoutConstraint!
    @IBOutlet weak var heightLastName: NSLayoutConstraint!
    @IBOutlet weak var baseViewSignIn: UIView!
    @IBOutlet weak var heightSignInView: NSLayoutConstraint!
    @IBOutlet weak var lblButtonTitle: UILabel!
    @IBOutlet weak var ImgLogo: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnCreateAccount: UIControl!
    @IBOutlet weak var firstnameView: UIView!
    @IBOutlet weak var lastnameView: UIView!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var createAccountView: UIView!
    
    
    var isShowSignUp: Bool = true
    var isFromMenu: Bool = false
    
    //MARK: - Forgot Password Delegate
    
    func sendLinkActionPerformed(email: String) {
        if email != "" {
            let stringValue:String? = self.emaiValidationForm(emailId: email)
            
            if ((stringValue?.length)! > 0){
                
                let alert = UIAlertController(title: "", message: stringValue!, preferredStyle:.alert)
                
                alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                DispatchQueue.main.async{
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                if CheckNetwork.isConnectedToNetwork() == true {
                let mutation =  Storefront.buildMutation { $0
                    .customerRecover(email: "\(email)") { $0
                        .customerUserErrors { $0
                            .code()
                            .field()
                            .message()
                            
                        }
                    }
                }
                
                let task = client.mutateGraphWith(mutation) { (response, errors) in
                    var message: String
                    let error = response?.customerRecover?.customerUserErrors ?? []
                    if error.count != 0 {
                        message = error[0].message
                    }else {
                        message = "forgotSuccessMsg".localized
                    }
                    
                    let alert = UIAlertController(title: "", message: message, preferredStyle:.alert)
                    
                    alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                    DispatchQueue.main.async{
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }
                    task.resume()
                    
                }else{
                    DispatchQueue.main.async {
                        self.Showalert1(message: "no_internet_msg".localized)
                    }
                }
            }
        }else{
            Showalert1(message: "nilEmailValidation".localized)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupMenuActions(notification:)), name:NSNotification.Name(rawValue: "setupMenuAction"), object: nil)
        self.setupLoginView()
    }
    
    @objc func setupMenuActions(notification: NSNotification) {
        self.isFromMenu = true
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: Any) {
        
        let myAlert = self.storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordAlertVC") as! ForgotPasswordAlertVC
        myAlert.delegate = self
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }
    
    @IBAction func btnShowCreateAccountTapped(_ sender: Any) {
        self.setupCreateAccountView()
    }
    
    @IBAction func btnSignInTapped(_ sender: Any) {
        
        self.setupLoginView()
        
    }
    
    func setupLoginView(){
        
        FirebaseEventsLog.request.trackScreen(screenName: "Login user", className: "LoginRegistrationVC")
        self.lblScreenTitle.text! = "btnsignin".localized
        self.lblButtonTitle.text! = "btnsignin".localized
        self.firstnameView.isHidden = true
        self.lastnameView.isHidden = true
        self.heightFirstName.constant = 0.0
        self.heightLastName.constant = 0.0
        self.btnForgotPassword.isHidden = false
        self.baseViewSignIn.isHidden = true
        self.createAccountView.isHidden = false
        self.view.layoutIfNeeded()
        self.view.setNeedsDisplay()
    }
    
    
    func setupCreateAccountView(){
        
        FirebaseEventsLog.request.trackScreen(screenName: "Register user", className: "LoginRegistrationVC")
        self.lblScreenTitle.text! = "titlecreateaccount".localized
        self.lblButtonTitle.text! = "titlecreateaccount".localized
        self.firstnameView.isHidden = false
        self.lastnameView.isHidden = false
        self.heightFirstName.constant = 48.0
        self.heightLastName.constant = 48.0
        self.btnForgotPassword.isHidden = true
        self.baseViewSignIn.isHidden = false
        self.createAccountView.isHidden = true
        self.view.layoutIfNeeded()
        self.view.setNeedsDisplay()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.btnCreateAccount.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblButtonTitle.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
        
        //        let jsonData = GlobalObjects.jsonString.data(using: .utf8)!
        //        let objConfig = try! JSONDecoder().decode(ConfigResponse.self, from: jsonData)
        
        let imageURL = URL(string: (GlobalObjects.objConfigResponse.globalSettings?.brandLogoURL!)!)
        
        self.ImgLogo.sd_setImage(with: imageURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
            // Your code inside completion block
            if image == nil {
                
                self.ImgLogo.image = #imageLiteral(resourceName: "placeholder")
            }
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCreateAccountTapped(_ sender: Any) {
        self.view.endEditing(true)
        if self.lblButtonTitle.text!  == "titlecreateaccount".localized {
            
            if self.txtFirstName.text! != "" && self.txtLastName.text! != "" && self.txtEmail.text! != "" && self.txtPassword.text! != "" {
                
                let stringValue:String? = self.validationForm()
                
                if ((stringValue?.length)! > 0){
                    
                    let alert = UIAlertController(title: "", message: stringValue!, preferredStyle:.alert)
                    
                    alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                    DispatchQueue.main.async{
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    
                    if (self.txtPassword.text!.length > 4) {
                        if CheckNetwork.isConnectedToNetwork() == true {
                            appDelegateShared.showHud()
                        ClientMutations.shared.registration(email: self.txtEmail.text!, password: self.txtPassword.text!, firstName: self.txtFirstName.text!, lastName: self.txtLastName.text!, phone: "") { (customer , error) in
                            
                            appDelegateShared.dismissHud()
                            
                            if let customer = customer {
                                
                                ClientMutations.shared.login(email: self.txtEmail.text!, password: self.txtPassword.text!) { (accesstoken , error) in
                                    
                                    if let accessToken = accesstoken {
                                        
                                        setMyUserDefaults(value: accessToken, key: MyUserDefaults.CustomerToken)
                                        setMyUserDefaults(value: self.txtEmail.text!, key: MyUserDefaults.UserEmail)
                                        
                                        let alert = UIAlertController(title: "lblcreateaccount".localized, message: "accountCreate".localized, preferredStyle:.alert)
                                        
                                        alert.addAction(UIAlertAction(title: "btnOk".localized, style: .default, handler: { (alert) in
                                            self.getCustomerDetails(accessToken: accesstoken!)
                                            self.getCustomerAddress(accessToken: accesstoken!, addresses: 10)
                                        }))
                                        
                                        DispatchQueue.main.async {
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        
                                    }else{
                                        
                                        let message = error[0].message
                                        let alert = UIAlertController(title: "lblcreateaccount".localized, message: "\(message)", preferredStyle:.alert)
                                        
                                        alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                                        DispatchQueue.main.async {
                                            
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    }
                                }
                                
                            }else{
                                
                                let message = error[0].message
                                let alert = UIAlertController(title: "lblcreateaccount".localized, message: "\(message)", preferredStyle:.alert)
                                
                                alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                                DispatchQueue.main.async {
                                    
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.Showalert1(message: "no_internet_msg".localized)
                        }
                    }
                    }else{
                        
                        let alert = UIAlertController(title: "", message: "passwordValidation".localized, preferredStyle:.alert)
                        
                        alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                        DispatchQueue.main.async {
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }
                    
                }
                
            }else if self.txtFirstName.text! == "" {
                Showalert1(message: "nilFirstnameValidation".localized)
                
            }else if self.txtLastName.text! == "" {
                Showalert1(message: "nilLastnameValidation".localized)
                
            }else if self.txtEmail.text! == "" {
                
                Showalert1(message: "nilEmailValidation".localized)
                
            }else if self.txtPassword.text! == "" {
                
                Showalert1(message: "nilPasswordValidation".localized)
            }
        }else{
            
            if self.txtEmail.text! != "" && self.txtPassword.text! != "" {
                
                let stringValue:String? = self.validationForm()
                
                if ((stringValue?.length)! > 0){
                    
                    let alert = UIAlertController(title: "", message: stringValue!, preferredStyle:.alert)
                    
                    alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                    DispatchQueue.main.async{
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    if CheckNetwork.isConnectedToNetwork() == true {
                        appDelegateShared.showHud()
                    ClientMutations.shared.login(email: self.txtEmail.text!, password: self.txtPassword.text!) { (accesstoken, error) in
                        appDelegateShared.dismissHud()
                        if let accessToken = accesstoken {
                            
                            setMyUserDefaults(value: accessToken, key: MyUserDefaults.CustomerToken)
                            setMyUserDefaults(value: self.txtEmail.text!, key: MyUserDefaults.UserEmail)
                            
                            self.getCustomerDetails(accessToken: accesstoken!)
                            self.getCustomerAddress(accessToken: accesstoken!, addresses: 10)
                            //
                            
                        }else{
                            
                            let message = error[0].message
                            let alert = UIAlertController(title: "signin".localized, message: "\(message)", preferredStyle:.alert)
                            
                            alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                            DispatchQueue.main.async {
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    }else{
                        DispatchQueue.main.async {
                            self.Showalert1(message: "no_internet_msg".localized)
                        }
                    }
                }
                
            }else if self.txtEmail.text! == "" {
                
                Showalert1(message: "nilEmailValidation".localized)
                
            }else if self.txtPassword.text! == "" {
                
                Showalert1(message: "nilPasswordValidation".localized)
            }
            
        }
    }
    
    func getCustomerDetails(accessToken: String){
        
        let query = Storefront.buildQuery { $0
            .customer(customerAccessToken: accessToken) { $0
                .id()
                .firstName()
                .lastName()
                .email()
            }
        }
        
        let task = client.queryGraphWith(query) { response, error in
            if let userId = response?.customer?.id.rawValue {
                FirebaseEventsLog.request.setUserId(userId: userId)
            }
            
        }
        
        task.resume()
    }
    
    func getCustomerAddress(accessToken: String, addresses: Int32){
        
        ShopifyCustomerAddress.shared.getCustomerAddress(accessToken: accessToken, addresses: addresses) { customer, error in
            
            if let result = customer?.addresses {
                GlobalObjects.CustomerAddresses.removeAll()
                let addresses = result.edges.map {$0.node}
                addresses.forEach { address in
                    
                    GlobalObjects.CustomerAddresses.append(CustomerAddress(id: address.id.rawValue, firstName: address.firstName, lastName: address.lastName, address1: address.address1, address2: address.address2, phone: address.phone, city: address.city, state: address.province, country: address.country, zipcode: address.zip))
                }
                
                if let parentVC = self.parent as? UINavigationController {
                    let baseVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardView")
                    let contentViewController = UINavigationController(rootViewController: baseVC!)
                    self.sideMenuViewController?.setContentViewController(contentViewController, animated: true)
                    
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
        
    func validationForm() -> String
    {
        var errorMessage:String? = ""
        
        self.txtEmail.text = appDelegateShared.TrimString(string: self.txtEmail.text!)
        
        if appDelegateShared.isValidURL(self.txtEmail.text!) == false {
            errorMessage = "emailValidation".localized
        }
        
        return errorMessage!
    }
    
    
    func emaiValidationForm(emailId:String) -> String
    {
        var errorMessage:String? = ""
        
        let trimEmail = appDelegateShared.TrimString(string: emailId)
        
        if appDelegateShared.isValidURL(trimEmail) == false {
            errorMessage = "emailValidation".localized
        }
        
        return errorMessage!
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
