//
//  ShopifyCustomerAddress.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 15/06/21.
//

import Foundation
import MobileBuySDK


class ShopifyCustomerAddress {
    
    static let shared = ShopifyCustomerAddress()
    
    func getCustomerAddress(accessToken: String, addresses: Int32, completion: @escaping (Storefront.Customer?, String?) -> Void){
        
        QueryGetCustomerAddress.shared.customerAddress(accessToken: accessToken, addresses: addresses) { response, error in
            
            if let response = response {
                completion(response, nil)
                
            }else{
                
                let error = "errorMsg".localized
                completion(nil, error)
            }
        }
    }
}
