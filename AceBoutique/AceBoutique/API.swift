// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

/// The three-letter currency codes that represent the world currencies used in stores. These include standard ISO 4217 codes, legacy codes,
/// and non-standard codes.
public enum CurrencyCode: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  /// United States Dollars (USD).
  case usd
  /// Euro (EUR).
  case eur
  /// United Kingdom Pounds (GBP).
  case gbp
  /// Canadian Dollars (CAD).
  case cad
  /// Afghan Afghani (AFN).
  case afn
  /// Albanian Lek (ALL).
  case all
  /// Algerian Dinar (DZD).
  case dzd
  /// Angolan Kwanza (AOA).
  case aoa
  /// Argentine Pesos (ARS).
  case ars
  /// Armenian Dram (AMD).
  case amd
  /// Aruban Florin (AWG).
  case awg
  /// Australian Dollars (AUD).
  case aud
  /// Barbadian Dollar (BBD).
  case bbd
  /// Azerbaijani Manat (AZN).
  case azn
  /// Bangladesh Taka (BDT).
  case bdt
  /// Bahamian Dollar (BSD).
  case bsd
  /// Bahraini Dinar (BHD).
  case bhd
  /// Burundian Franc (BIF).
  case bif
  /// Belize Dollar (BZD).
  case bzd
  /// Bermudian Dollar (BMD).
  case bmd
  /// Bhutanese Ngultrum (BTN).
  case btn
  /// Bosnia and Herzegovina Convertible Mark (BAM).
  case bam
  /// Brazilian Real (BRL).
  case brl
  /// Bolivian Boliviano (BOB).
  case bob
  /// Botswana Pula (BWP).
  case bwp
  /// Brunei Dollar (BND).
  case bnd
  /// Bulgarian Lev (BGN).
  case bgn
  /// Burmese Kyat (MMK).
  case mmk
  /// Cambodian Riel.
  case khr
  /// Cape Verdean escudo (CVE).
  case cve
  /// Cayman Dollars (KYD).
  case kyd
  /// Central African CFA Franc (XAF).
  case xaf
  /// Chilean Peso (CLP).
  case clp
  /// Chinese Yuan Renminbi (CNY).
  case cny
  /// Colombian Peso (COP).
  case cop
  /// Comorian Franc (KMF).
  case kmf
  /// Congolese franc (CDF).
  case cdf
  /// Costa Rican Colones (CRC).
  case crc
  /// Croatian Kuna (HRK).
  case hrk
  /// Czech Koruny (CZK).
  case czk
  /// Danish Kroner (DKK).
  case dkk
  /// Dominican Peso (DOP).
  case dop
  /// East Caribbean Dollar (XCD).
  case xcd
  /// Egyptian Pound (EGP).
  case egp
  /// Ethiopian Birr (ETB).
  case etb
  /// CFP Franc (XPF).
  case xpf
  /// Fijian Dollars (FJD).
  case fjd
  /// Gambian Dalasi (GMD).
  case gmd
  /// Ghanaian Cedi (GHS).
  case ghs
  /// Guatemalan Quetzal (GTQ).
  case gtq
  /// Guyanese Dollar (GYD).
  case gyd
  /// Georgian Lari (GEL).
  case gel
  /// Haitian Gourde (HTG).
  case htg
  /// Honduran Lempira (HNL).
  case hnl
  /// Hong Kong Dollars (HKD).
  case hkd
  /// Hungarian Forint (HUF).
  case huf
  /// Icelandic Kronur (ISK).
  case isk
  /// Indian Rupees (INR).
  case inr
  /// Indonesian Rupiah (IDR).
  case idr
  /// Israeli New Shekel (NIS).
  case ils
  /// Iraqi Dinar (IQD).
  case iqd
  /// Jamaican Dollars (JMD).
  case jmd
  /// Japanese Yen (JPY).
  case jpy
  /// Jersey Pound.
  case jep
  /// Jordanian Dinar (JOD).
  case jod
  /// Kazakhstani Tenge (KZT).
  case kzt
  /// Kenyan Shilling (KES).
  case kes
  /// Kuwaiti Dinar (KWD).
  case kwd
  /// Kyrgyzstani Som (KGS).
  case kgs
  /// Laotian Kip (LAK).
  case lak
  /// Latvian Lati (LVL).
  case lvl
  /// Lebanese Pounds (LBP).
  case lbp
  /// Lesotho Loti (LSL).
  case lsl
  /// Liberian Dollar (LRD).
  case lrd
  /// Lithuanian Litai (LTL).
  case ltl
  /// Malagasy Ariary (MGA).
  case mga
  /// Macedonia Denar (MKD).
  case mkd
  /// Macanese Pataca (MOP).
  case mop
  /// Malawian Kwacha (MWK).
  case mwk
  /// Maldivian Rufiyaa (MVR).
  case mvr
  /// Mexican Pesos (MXN).
  case mxn
  /// Malaysian Ringgits (MYR).
  case myr
  /// Mauritian Rupee (MUR).
  case mur
  /// Moldovan Leu (MDL).
  case mdl
  /// Moroccan Dirham.
  case mad
  /// Mongolian Tugrik.
  case mnt
  /// Mozambican Metical.
  case mzn
  /// Namibian Dollar.
  case nad
  /// Nepalese Rupee (NPR).
  case npr
  /// Netherlands Antillean Guilder.
  case ang
  /// New Zealand Dollars (NZD).
  case nzd
  /// Nicaraguan Córdoba (NIO).
  case nio
  /// Nigerian Naira (NGN).
  case ngn
  /// Norwegian Kroner (NOK).
  case nok
  /// Omani Rial (OMR).
  case omr
  /// Panamian Balboa (PAB).
  case pab
  /// Pakistani Rupee (PKR).
  case pkr
  /// Papua New Guinean Kina (PGK).
  case pgk
  /// Paraguayan Guarani (PYG).
  case pyg
  /// Peruvian Nuevo Sol (PEN).
  case pen
  /// Philippine Peso (PHP).
  case php
  /// Polish Zlotych (PLN).
  case pln
  /// Qatari Rial (QAR).
  case qar
  /// Romanian Lei (RON).
  case ron
  /// Russian Rubles (RUB).
  case rub
  /// Rwandan Franc (RWF).
  case rwf
  /// Samoan Tala (WST).
  case wst
  /// Saudi Riyal (SAR).
  case sar
  /// Sao Tome And Principe Dobra (STD).
  case std
  /// Serbian dinar (RSD).
  case rsd
  /// Seychellois Rupee (SCR).
  case scr
  /// Singapore Dollars (SGD).
  case sgd
  /// Sudanese Pound (SDG).
  case sdg
  /// Syrian Pound (SYP).
  case syp
  /// South African Rand (ZAR).
  case zar
  /// South Korean Won (KRW).
  case krw
  /// South Sudanese Pound (SSP).
  case ssp
  /// Solomon Islands Dollar (SBD).
  case sbd
  /// Sri Lankan Rupees (LKR).
  case lkr
  /// Surinamese Dollar (SRD).
  case srd
  /// Swazi Lilangeni (SZL).
  case szl
  /// Swedish Kronor (SEK).
  case sek
  /// Swiss Francs (CHF).
  case chf
  /// Taiwan Dollars (TWD).
  case twd
  /// Thai baht (THB).
  case thb
  /// Tanzanian Shilling (TZS).
  case tzs
  /// Trinidad and Tobago Dollars (TTD).
  case ttd
  /// Tunisian Dinar (TND).
  case tnd
  /// Turkish Lira (TRY).
  case `try`
  /// Turkmenistani Manat (TMT).
  case tmt
  /// Ugandan Shilling (UGX).
  case ugx
  /// Ukrainian Hryvnia (UAH).
  case uah
  /// United Arab Emirates Dirham (AED).
  case aed
  /// Uruguayan Pesos (UYU).
  case uyu
  /// Uzbekistan som (UZS).
  case uzs
  /// Vanuatu Vatu (VUV).
  case vuv
  /// Vietnamese đồng (VND).
  case vnd
  /// West African CFA franc (XOF).
  case xof
  /// Yemeni Rial (YER).
  case yer
  /// Zambian Kwacha (ZMW).
  case zmw
  /// Belarusian Ruble (BYN).
  case byn
  /// Belarusian Ruble (BYR).
  @available(*, deprecated, message: "`BYR` is deprecated. Use `BYN` available from version `2021-01` onwards instead.")
  case byr
  /// Djiboutian Franc (DJF).
  case djf
  /// Eritrean Nakfa (ERN).
  case ern
  /// Falkland Islands Pounds (FKP).
  case fkp
  /// Gibraltar Pounds (GIP).
  case gip
  /// Guinean Franc (GNF).
  case gnf
  /// Iranian Rial (IRR).
  case irr
  /// Kiribati Dollar (KID).
  case kid
  /// Libyan Dinar (LYD).
  case lyd
  /// Mauritanian Ouguiya (MRU).
  case mru
  /// Sierra Leonean Leone (SLL).
  case sll
  /// Saint Helena Pounds (SHP).
  case shp
  /// Somali Shilling (SOS).
  case sos
  /// Tajikistani Somoni (TJS).
  case tjs
  /// Tongan Pa'anga (TOP).
  case top
  /// Venezuelan Bolivares (VEF).
  @available(*, deprecated, message: "`VEF` is deprecated. Use `VES` available from version `2020-10` onwards instead.")
  case vef
  /// Venezuelan Bolivares (VES).
  case ves
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "USD": self = .usd
      case "EUR": self = .eur
      case "GBP": self = .gbp
      case "CAD": self = .cad
      case "AFN": self = .afn
      case "ALL": self = .all
      case "DZD": self = .dzd
      case "AOA": self = .aoa
      case "ARS": self = .ars
      case "AMD": self = .amd
      case "AWG": self = .awg
      case "AUD": self = .aud
      case "BBD": self = .bbd
      case "AZN": self = .azn
      case "BDT": self = .bdt
      case "BSD": self = .bsd
      case "BHD": self = .bhd
      case "BIF": self = .bif
      case "BZD": self = .bzd
      case "BMD": self = .bmd
      case "BTN": self = .btn
      case "BAM": self = .bam
      case "BRL": self = .brl
      case "BOB": self = .bob
      case "BWP": self = .bwp
      case "BND": self = .bnd
      case "BGN": self = .bgn
      case "MMK": self = .mmk
      case "KHR": self = .khr
      case "CVE": self = .cve
      case "KYD": self = .kyd
      case "XAF": self = .xaf
      case "CLP": self = .clp
      case "CNY": self = .cny
      case "COP": self = .cop
      case "KMF": self = .kmf
      case "CDF": self = .cdf
      case "CRC": self = .crc
      case "HRK": self = .hrk
      case "CZK": self = .czk
      case "DKK": self = .dkk
      case "DOP": self = .dop
      case "XCD": self = .xcd
      case "EGP": self = .egp
      case "ETB": self = .etb
      case "XPF": self = .xpf
      case "FJD": self = .fjd
      case "GMD": self = .gmd
      case "GHS": self = .ghs
      case "GTQ": self = .gtq
      case "GYD": self = .gyd
      case "GEL": self = .gel
      case "HTG": self = .htg
      case "HNL": self = .hnl
      case "HKD": self = .hkd
      case "HUF": self = .huf
      case "ISK": self = .isk
      case "INR": self = .inr
      case "IDR": self = .idr
      case "ILS": self = .ils
      case "IQD": self = .iqd
      case "JMD": self = .jmd
      case "JPY": self = .jpy
      case "JEP": self = .jep
      case "JOD": self = .jod
      case "KZT": self = .kzt
      case "KES": self = .kes
      case "KWD": self = .kwd
      case "KGS": self = .kgs
      case "LAK": self = .lak
      case "LVL": self = .lvl
      case "LBP": self = .lbp
      case "LSL": self = .lsl
      case "LRD": self = .lrd
      case "LTL": self = .ltl
      case "MGA": self = .mga
      case "MKD": self = .mkd
      case "MOP": self = .mop
      case "MWK": self = .mwk
      case "MVR": self = .mvr
      case "MXN": self = .mxn
      case "MYR": self = .myr
      case "MUR": self = .mur
      case "MDL": self = .mdl
      case "MAD": self = .mad
      case "MNT": self = .mnt
      case "MZN": self = .mzn
      case "NAD": self = .nad
      case "NPR": self = .npr
      case "ANG": self = .ang
      case "NZD": self = .nzd
      case "NIO": self = .nio
      case "NGN": self = .ngn
      case "NOK": self = .nok
      case "OMR": self = .omr
      case "PAB": self = .pab
      case "PKR": self = .pkr
      case "PGK": self = .pgk
      case "PYG": self = .pyg
      case "PEN": self = .pen
      case "PHP": self = .php
      case "PLN": self = .pln
      case "QAR": self = .qar
      case "RON": self = .ron
      case "RUB": self = .rub
      case "RWF": self = .rwf
      case "WST": self = .wst
      case "SAR": self = .sar
      case "STD": self = .std
      case "RSD": self = .rsd
      case "SCR": self = .scr
      case "SGD": self = .sgd
      case "SDG": self = .sdg
      case "SYP": self = .syp
      case "ZAR": self = .zar
      case "KRW": self = .krw
      case "SSP": self = .ssp
      case "SBD": self = .sbd
      case "LKR": self = .lkr
      case "SRD": self = .srd
      case "SZL": self = .szl
      case "SEK": self = .sek
      case "CHF": self = .chf
      case "TWD": self = .twd
      case "THB": self = .thb
      case "TZS": self = .tzs
      case "TTD": self = .ttd
      case "TND": self = .tnd
      case "TRY": self = .try
      case "TMT": self = .tmt
      case "UGX": self = .ugx
      case "UAH": self = .uah
      case "AED": self = .aed
      case "UYU": self = .uyu
      case "UZS": self = .uzs
      case "VUV": self = .vuv
      case "VND": self = .vnd
      case "XOF": self = .xof
      case "YER": self = .yer
      case "ZMW": self = .zmw
      case "BYN": self = .byn
      case "BYR": self = .byr
      case "DJF": self = .djf
      case "ERN": self = .ern
      case "FKP": self = .fkp
      case "GIP": self = .gip
      case "GNF": self = .gnf
      case "IRR": self = .irr
      case "KID": self = .kid
      case "LYD": self = .lyd
      case "MRU": self = .mru
      case "SLL": self = .sll
      case "SHP": self = .shp
      case "SOS": self = .sos
      case "TJS": self = .tjs
      case "TOP": self = .top
      case "VEF": self = .vef
      case "VES": self = .ves
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .usd: return "USD"
      case .eur: return "EUR"
      case .gbp: return "GBP"
      case .cad: return "CAD"
      case .afn: return "AFN"
      case .all: return "ALL"
      case .dzd: return "DZD"
      case .aoa: return "AOA"
      case .ars: return "ARS"
      case .amd: return "AMD"
      case .awg: return "AWG"
      case .aud: return "AUD"
      case .bbd: return "BBD"
      case .azn: return "AZN"
      case .bdt: return "BDT"
      case .bsd: return "BSD"
      case .bhd: return "BHD"
      case .bif: return "BIF"
      case .bzd: return "BZD"
      case .bmd: return "BMD"
      case .btn: return "BTN"
      case .bam: return "BAM"
      case .brl: return "BRL"
      case .bob: return "BOB"
      case .bwp: return "BWP"
      case .bnd: return "BND"
      case .bgn: return "BGN"
      case .mmk: return "MMK"
      case .khr: return "KHR"
      case .cve: return "CVE"
      case .kyd: return "KYD"
      case .xaf: return "XAF"
      case .clp: return "CLP"
      case .cny: return "CNY"
      case .cop: return "COP"
      case .kmf: return "KMF"
      case .cdf: return "CDF"
      case .crc: return "CRC"
      case .hrk: return "HRK"
      case .czk: return "CZK"
      case .dkk: return "DKK"
      case .dop: return "DOP"
      case .xcd: return "XCD"
      case .egp: return "EGP"
      case .etb: return "ETB"
      case .xpf: return "XPF"
      case .fjd: return "FJD"
      case .gmd: return "GMD"
      case .ghs: return "GHS"
      case .gtq: return "GTQ"
      case .gyd: return "GYD"
      case .gel: return "GEL"
      case .htg: return "HTG"
      case .hnl: return "HNL"
      case .hkd: return "HKD"
      case .huf: return "HUF"
      case .isk: return "ISK"
      case .inr: return "INR"
      case .idr: return "IDR"
      case .ils: return "ILS"
      case .iqd: return "IQD"
      case .jmd: return "JMD"
      case .jpy: return "JPY"
      case .jep: return "JEP"
      case .jod: return "JOD"
      case .kzt: return "KZT"
      case .kes: return "KES"
      case .kwd: return "KWD"
      case .kgs: return "KGS"
      case .lak: return "LAK"
      case .lvl: return "LVL"
      case .lbp: return "LBP"
      case .lsl: return "LSL"
      case .lrd: return "LRD"
      case .ltl: return "LTL"
      case .mga: return "MGA"
      case .mkd: return "MKD"
      case .mop: return "MOP"
      case .mwk: return "MWK"
      case .mvr: return "MVR"
      case .mxn: return "MXN"
      case .myr: return "MYR"
      case .mur: return "MUR"
      case .mdl: return "MDL"
      case .mad: return "MAD"
      case .mnt: return "MNT"
      case .mzn: return "MZN"
      case .nad: return "NAD"
      case .npr: return "NPR"
      case .ang: return "ANG"
      case .nzd: return "NZD"
      case .nio: return "NIO"
      case .ngn: return "NGN"
      case .nok: return "NOK"
      case .omr: return "OMR"
      case .pab: return "PAB"
      case .pkr: return "PKR"
      case .pgk: return "PGK"
      case .pyg: return "PYG"
      case .pen: return "PEN"
      case .php: return "PHP"
      case .pln: return "PLN"
      case .qar: return "QAR"
      case .ron: return "RON"
      case .rub: return "RUB"
      case .rwf: return "RWF"
      case .wst: return "WST"
      case .sar: return "SAR"
      case .std: return "STD"
      case .rsd: return "RSD"
      case .scr: return "SCR"
      case .sgd: return "SGD"
      case .sdg: return "SDG"
      case .syp: return "SYP"
      case .zar: return "ZAR"
      case .krw: return "KRW"
      case .ssp: return "SSP"
      case .sbd: return "SBD"
      case .lkr: return "LKR"
      case .srd: return "SRD"
      case .szl: return "SZL"
      case .sek: return "SEK"
      case .chf: return "CHF"
      case .twd: return "TWD"
      case .thb: return "THB"
      case .tzs: return "TZS"
      case .ttd: return "TTD"
      case .tnd: return "TND"
      case .try: return "TRY"
      case .tmt: return "TMT"
      case .ugx: return "UGX"
      case .uah: return "UAH"
      case .aed: return "AED"
      case .uyu: return "UYU"
      case .uzs: return "UZS"
      case .vuv: return "VUV"
      case .vnd: return "VND"
      case .xof: return "XOF"
      case .yer: return "YER"
      case .zmw: return "ZMW"
      case .byn: return "BYN"
      case .byr: return "BYR"
      case .djf: return "DJF"
      case .ern: return "ERN"
      case .fkp: return "FKP"
      case .gip: return "GIP"
      case .gnf: return "GNF"
      case .irr: return "IRR"
      case .kid: return "KID"
      case .lyd: return "LYD"
      case .mru: return "MRU"
      case .sll: return "SLL"
      case .shp: return "SHP"
      case .sos: return "SOS"
      case .tjs: return "TJS"
      case .top: return "TOP"
      case .vef: return "VEF"
      case .ves: return "VES"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: CurrencyCode, rhs: CurrencyCode) -> Bool {
    switch (lhs, rhs) {
      case (.usd, .usd): return true
      case (.eur, .eur): return true
      case (.gbp, .gbp): return true
      case (.cad, .cad): return true
      case (.afn, .afn): return true
      case (.all, .all): return true
      case (.dzd, .dzd): return true
      case (.aoa, .aoa): return true
      case (.ars, .ars): return true
      case (.amd, .amd): return true
      case (.awg, .awg): return true
      case (.aud, .aud): return true
      case (.bbd, .bbd): return true
      case (.azn, .azn): return true
      case (.bdt, .bdt): return true
      case (.bsd, .bsd): return true
      case (.bhd, .bhd): return true
      case (.bif, .bif): return true
      case (.bzd, .bzd): return true
      case (.bmd, .bmd): return true
      case (.btn, .btn): return true
      case (.bam, .bam): return true
      case (.brl, .brl): return true
      case (.bob, .bob): return true
      case (.bwp, .bwp): return true
      case (.bnd, .bnd): return true
      case (.bgn, .bgn): return true
      case (.mmk, .mmk): return true
      case (.khr, .khr): return true
      case (.cve, .cve): return true
      case (.kyd, .kyd): return true
      case (.xaf, .xaf): return true
      case (.clp, .clp): return true
      case (.cny, .cny): return true
      case (.cop, .cop): return true
      case (.kmf, .kmf): return true
      case (.cdf, .cdf): return true
      case (.crc, .crc): return true
      case (.hrk, .hrk): return true
      case (.czk, .czk): return true
      case (.dkk, .dkk): return true
      case (.dop, .dop): return true
      case (.xcd, .xcd): return true
      case (.egp, .egp): return true
      case (.etb, .etb): return true
      case (.xpf, .xpf): return true
      case (.fjd, .fjd): return true
      case (.gmd, .gmd): return true
      case (.ghs, .ghs): return true
      case (.gtq, .gtq): return true
      case (.gyd, .gyd): return true
      case (.gel, .gel): return true
      case (.htg, .htg): return true
      case (.hnl, .hnl): return true
      case (.hkd, .hkd): return true
      case (.huf, .huf): return true
      case (.isk, .isk): return true
      case (.inr, .inr): return true
      case (.idr, .idr): return true
      case (.ils, .ils): return true
      case (.iqd, .iqd): return true
      case (.jmd, .jmd): return true
      case (.jpy, .jpy): return true
      case (.jep, .jep): return true
      case (.jod, .jod): return true
      case (.kzt, .kzt): return true
      case (.kes, .kes): return true
      case (.kwd, .kwd): return true
      case (.kgs, .kgs): return true
      case (.lak, .lak): return true
      case (.lvl, .lvl): return true
      case (.lbp, .lbp): return true
      case (.lsl, .lsl): return true
      case (.lrd, .lrd): return true
      case (.ltl, .ltl): return true
      case (.mga, .mga): return true
      case (.mkd, .mkd): return true
      case (.mop, .mop): return true
      case (.mwk, .mwk): return true
      case (.mvr, .mvr): return true
      case (.mxn, .mxn): return true
      case (.myr, .myr): return true
      case (.mur, .mur): return true
      case (.mdl, .mdl): return true
      case (.mad, .mad): return true
      case (.mnt, .mnt): return true
      case (.mzn, .mzn): return true
      case (.nad, .nad): return true
      case (.npr, .npr): return true
      case (.ang, .ang): return true
      case (.nzd, .nzd): return true
      case (.nio, .nio): return true
      case (.ngn, .ngn): return true
      case (.nok, .nok): return true
      case (.omr, .omr): return true
      case (.pab, .pab): return true
      case (.pkr, .pkr): return true
      case (.pgk, .pgk): return true
      case (.pyg, .pyg): return true
      case (.pen, .pen): return true
      case (.php, .php): return true
      case (.pln, .pln): return true
      case (.qar, .qar): return true
      case (.ron, .ron): return true
      case (.rub, .rub): return true
      case (.rwf, .rwf): return true
      case (.wst, .wst): return true
      case (.sar, .sar): return true
      case (.std, .std): return true
      case (.rsd, .rsd): return true
      case (.scr, .scr): return true
      case (.sgd, .sgd): return true
      case (.sdg, .sdg): return true
      case (.syp, .syp): return true
      case (.zar, .zar): return true
      case (.krw, .krw): return true
      case (.ssp, .ssp): return true
      case (.sbd, .sbd): return true
      case (.lkr, .lkr): return true
      case (.srd, .srd): return true
      case (.szl, .szl): return true
      case (.sek, .sek): return true
      case (.chf, .chf): return true
      case (.twd, .twd): return true
      case (.thb, .thb): return true
      case (.tzs, .tzs): return true
      case (.ttd, .ttd): return true
      case (.tnd, .tnd): return true
      case (.try, .try): return true
      case (.tmt, .tmt): return true
      case (.ugx, .ugx): return true
      case (.uah, .uah): return true
      case (.aed, .aed): return true
      case (.uyu, .uyu): return true
      case (.uzs, .uzs): return true
      case (.vuv, .vuv): return true
      case (.vnd, .vnd): return true
      case (.xof, .xof): return true
      case (.yer, .yer): return true
      case (.zmw, .zmw): return true
      case (.byn, .byn): return true
      case (.byr, .byr): return true
      case (.djf, .djf): return true
      case (.ern, .ern): return true
      case (.fkp, .fkp): return true
      case (.gip, .gip): return true
      case (.gnf, .gnf): return true
      case (.irr, .irr): return true
      case (.kid, .kid): return true
      case (.lyd, .lyd): return true
      case (.mru, .mru): return true
      case (.sll, .sll): return true
      case (.shp, .shp): return true
      case (.sos, .sos): return true
      case (.tjs, .tjs): return true
      case (.top, .top): return true
      case (.vef, .vef): return true
      case (.ves, .ves): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [CurrencyCode] {
    return [
      .usd,
      .eur,
      .gbp,
      .cad,
      .afn,
      .all,
      .dzd,
      .aoa,
      .ars,
      .amd,
      .awg,
      .aud,
      .bbd,
      .azn,
      .bdt,
      .bsd,
      .bhd,
      .bif,
      .bzd,
      .bmd,
      .btn,
      .bam,
      .brl,
      .bob,
      .bwp,
      .bnd,
      .bgn,
      .mmk,
      .khr,
      .cve,
      .kyd,
      .xaf,
      .clp,
      .cny,
      .cop,
      .kmf,
      .cdf,
      .crc,
      .hrk,
      .czk,
      .dkk,
      .dop,
      .xcd,
      .egp,
      .etb,
      .xpf,
      .fjd,
      .gmd,
      .ghs,
      .gtq,
      .gyd,
      .gel,
      .htg,
      .hnl,
      .hkd,
      .huf,
      .isk,
      .inr,
      .idr,
      .ils,
      .iqd,
      .jmd,
      .jpy,
      .jep,
      .jod,
      .kzt,
      .kes,
      .kwd,
      .kgs,
      .lak,
      .lvl,
      .lbp,
      .lsl,
      .lrd,
      .ltl,
      .mga,
      .mkd,
      .mop,
      .mwk,
      .mvr,
      .mxn,
      .myr,
      .mur,
      .mdl,
      .mad,
      .mnt,
      .mzn,
      .nad,
      .npr,
      .ang,
      .nzd,
      .nio,
      .ngn,
      .nok,
      .omr,
      .pab,
      .pkr,
      .pgk,
      .pyg,
      .pen,
      .php,
      .pln,
      .qar,
      .ron,
      .rub,
      .rwf,
      .wst,
      .sar,
      .std,
      .rsd,
      .scr,
      .sgd,
      .sdg,
      .syp,
      .zar,
      .krw,
      .ssp,
      .sbd,
      .lkr,
      .srd,
      .szl,
      .sek,
      .chf,
      .twd,
      .thb,
      .tzs,
      .ttd,
      .tnd,
      .try,
      .tmt,
      .ugx,
      .uah,
      .aed,
      .uyu,
      .uzs,
      .vuv,
      .vnd,
      .xof,
      .yer,
      .zmw,
      .byn,
      .byr,
      .djf,
      .ern,
      .fkp,
      .gip,
      .gnf,
      .irr,
      .kid,
      .lyd,
      .mru,
      .sll,
      .shp,
      .sos,
      .tjs,
      .top,
      .vef,
      .ves,
    ]
  }
}

/// Represents the shop's customer account requirement preference.
public enum ShopCustomerAccountsSetting: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case `required`
  case `optional`
  case disabled
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "REQUIRED": self = .required
      case "OPTIONAL": self = .optional
      case "DISABLED": self = .disabled
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .required: return "REQUIRED"
      case .optional: return "OPTIONAL"
      case .disabled: return "DISABLED"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: ShopCustomerAccountsSetting, rhs: ShopCustomerAccountsSetting) -> Bool {
    switch (lhs, rhs) {
      case (.required, .required): return true
      case (.optional, .optional): return true
      case (.disabled, .disabled): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [ShopCustomerAccountsSetting] {
    return [
      .required,
      .optional,
      .disabled,
    ]
  }
}

public final class OrderDetailsByIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query OrderDetailsById($id: ID!) {
      node(id: $id) {
        __typename
        ... on Order {
          id
          createdAt
          discountCode
          subtotalPriceSet {
            __typename
            presentmentMoney {
              __typename
              amount
              currencyCode
            }
          }
          totalShippingPriceSet {
            __typename
            presentmentMoney {
              __typename
              amount
              currencyCode
            }
          }
          totalTaxSet {
            __typename
            presentmentMoney {
              __typename
              amount
              currencyCode
            }
          }
          cartDiscountAmountSet {
            __typename
            presentmentMoney {
              __typename
              amount
              currencyCode
            }
          }
          totalPriceSet {
            __typename
            presentmentMoney {
              __typename
              amount
              currencyCode
            }
          }
        }
      }
    }
    """

  public let operationName: String = "OrderDetailsById"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["QueryRoot"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("node", arguments: ["id": GraphQLVariable("id")], type: .object(Node.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(node: Node? = nil) {
      self.init(unsafeResultMap: ["__typename": "QueryRoot", "node": node.flatMap { (value: Node) -> ResultMap in value.resultMap }])
    }

    /// Returns a specific node (any object that implements the
    /// [Node](https://shopify.dev/api/admin-graphql/latest/interfaces/Node)
    /// interface) by ID, in accordance with the
    /// [Relay specification](https://relay.dev/docs/guides/graphql-server-specification/#object-identification).
    /// This field is commonly used for refetching an object.
    public var node: Node? {
      get {
        return (resultMap["node"] as? ResultMap).flatMap { Node(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "node")
      }
    }

    public struct Node: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["App", "AppCredit", "AppInstallation", "AppPurchaseOneTime", "AppRevenueAttributionRecord", "AppSubscription", "AppUsageRecord", "BasicEvent", "BulkOperation", "CalculatedOrder", "Channel", "Collection", "CommentEvent", "Customer", "CustomerPaymentMethod", "CustomerVisit", "DeliveryCarrierService", "DeliveryCondition", "DeliveryCountry", "DeliveryLocationGroup", "DeliveryMethod", "DeliveryMethodDefinition", "DeliveryParticipant", "DeliveryProfile", "DeliveryProvince", "DeliveryRateDefinition", "DeliveryZone", "DiscountAutomaticBxgy", "DiscountAutomaticNode", "DiscountCodeNode", "DiscountRedeemCodeBulkCreation", "Domain", "DraftOrder", "DraftOrderLineItem", "Duty", "ExternalVideo", "Fulfillment", "FulfillmentEvent", "FulfillmentLineItem", "FulfillmentOrder", "FulfillmentOrderDestination", "FulfillmentOrderLineItem", "FulfillmentOrderMerchantRequest", "GiftCard", "InventoryItem", "InventoryLevel", "LineItem", "LineItemMutable", "Location", "MailingAddress", "MarketingActivity", "MarketingEvent", "MediaImage", "Metafield", "MetafieldStorefrontVisibility", "Model3d", "OnlineStoreArticle", "OnlineStoreBlog", "OnlineStorePage", "Order", "OrderDisputeSummary", "OrderTransaction", "PriceList", "PriceRule", "PriceRuleDiscountCode", "PrivateMetafield", "Product", "ProductOption", "ProductVariant", "Publication", "Refund", "SavedSearch", "ScriptTag", "SellingPlan", "SellingPlanGroup", "Shop", "ShopPolicy", "ShopifyPaymentsAccount", "ShopifyPaymentsBankAccount", "ShopifyPaymentsDispute", "ShopifyPaymentsPayout", "ShopifyPaymentsVerification", "StorefrontAccessToken", "SubscriptionBillingAttempt", "SubscriptionContract", "SubscriptionDraft", "TenderTransaction", "TransactionFee", "Video", "WebhookSubscription"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLTypeCase(
            variants: ["Order": AsOrder.selections],
            default: [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            ]
          )
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public static func makeApp() -> Node {
        return Node(unsafeResultMap: ["__typename": "App"])
      }

      public static func makeAppCredit() -> Node {
        return Node(unsafeResultMap: ["__typename": "AppCredit"])
      }

      public static func makeAppInstallation() -> Node {
        return Node(unsafeResultMap: ["__typename": "AppInstallation"])
      }

      public static func makeAppPurchaseOneTime() -> Node {
        return Node(unsafeResultMap: ["__typename": "AppPurchaseOneTime"])
      }

      public static func makeAppRevenueAttributionRecord() -> Node {
        return Node(unsafeResultMap: ["__typename": "AppRevenueAttributionRecord"])
      }

      public static func makeAppSubscription() -> Node {
        return Node(unsafeResultMap: ["__typename": "AppSubscription"])
      }

      public static func makeAppUsageRecord() -> Node {
        return Node(unsafeResultMap: ["__typename": "AppUsageRecord"])
      }

      public static func makeBasicEvent() -> Node {
        return Node(unsafeResultMap: ["__typename": "BasicEvent"])
      }

      public static func makeBulkOperation() -> Node {
        return Node(unsafeResultMap: ["__typename": "BulkOperation"])
      }

      public static func makeCalculatedOrder() -> Node {
        return Node(unsafeResultMap: ["__typename": "CalculatedOrder"])
      }

      public static func makeChannel() -> Node {
        return Node(unsafeResultMap: ["__typename": "Channel"])
      }

      public static func makeCollection() -> Node {
        return Node(unsafeResultMap: ["__typename": "Collection"])
      }

      public static func makeCommentEvent() -> Node {
        return Node(unsafeResultMap: ["__typename": "CommentEvent"])
      }

      public static func makeCustomer() -> Node {
        return Node(unsafeResultMap: ["__typename": "Customer"])
      }

      public static func makeCustomerPaymentMethod() -> Node {
        return Node(unsafeResultMap: ["__typename": "CustomerPaymentMethod"])
      }

      public static func makeCustomerVisit() -> Node {
        return Node(unsafeResultMap: ["__typename": "CustomerVisit"])
      }

      public static func makeDeliveryCarrierService() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryCarrierService"])
      }

      public static func makeDeliveryCondition() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryCondition"])
      }

      public static func makeDeliveryCountry() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryCountry"])
      }

      public static func makeDeliveryLocationGroup() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryLocationGroup"])
      }

      public static func makeDeliveryMethod() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryMethod"])
      }

      public static func makeDeliveryMethodDefinition() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryMethodDefinition"])
      }

      public static func makeDeliveryParticipant() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryParticipant"])
      }

      public static func makeDeliveryProfile() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryProfile"])
      }

      public static func makeDeliveryProvince() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryProvince"])
      }

      public static func makeDeliveryRateDefinition() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryRateDefinition"])
      }

      public static func makeDeliveryZone() -> Node {
        return Node(unsafeResultMap: ["__typename": "DeliveryZone"])
      }

      public static func makeDiscountAutomaticBxgy() -> Node {
        return Node(unsafeResultMap: ["__typename": "DiscountAutomaticBxgy"])
      }

      public static func makeDiscountAutomaticNode() -> Node {
        return Node(unsafeResultMap: ["__typename": "DiscountAutomaticNode"])
      }

      public static func makeDiscountCodeNode() -> Node {
        return Node(unsafeResultMap: ["__typename": "DiscountCodeNode"])
      }

      public static func makeDiscountRedeemCodeBulkCreation() -> Node {
        return Node(unsafeResultMap: ["__typename": "DiscountRedeemCodeBulkCreation"])
      }

      public static func makeDomain() -> Node {
        return Node(unsafeResultMap: ["__typename": "Domain"])
      }

      public static func makeDraftOrder() -> Node {
        return Node(unsafeResultMap: ["__typename": "DraftOrder"])
      }

      public static func makeDraftOrderLineItem() -> Node {
        return Node(unsafeResultMap: ["__typename": "DraftOrderLineItem"])
      }

      public static func makeDuty() -> Node {
        return Node(unsafeResultMap: ["__typename": "Duty"])
      }

      public static func makeExternalVideo() -> Node {
        return Node(unsafeResultMap: ["__typename": "ExternalVideo"])
      }

      public static func makeFulfillment() -> Node {
        return Node(unsafeResultMap: ["__typename": "Fulfillment"])
      }

      public static func makeFulfillmentEvent() -> Node {
        return Node(unsafeResultMap: ["__typename": "FulfillmentEvent"])
      }

      public static func makeFulfillmentLineItem() -> Node {
        return Node(unsafeResultMap: ["__typename": "FulfillmentLineItem"])
      }

      public static func makeFulfillmentOrder() -> Node {
        return Node(unsafeResultMap: ["__typename": "FulfillmentOrder"])
      }

      public static func makeFulfillmentOrderDestination() -> Node {
        return Node(unsafeResultMap: ["__typename": "FulfillmentOrderDestination"])
      }

      public static func makeFulfillmentOrderLineItem() -> Node {
        return Node(unsafeResultMap: ["__typename": "FulfillmentOrderLineItem"])
      }

      public static func makeFulfillmentOrderMerchantRequest() -> Node {
        return Node(unsafeResultMap: ["__typename": "FulfillmentOrderMerchantRequest"])
      }

      public static func makeGiftCard() -> Node {
        return Node(unsafeResultMap: ["__typename": "GiftCard"])
      }

      public static func makeInventoryItem() -> Node {
        return Node(unsafeResultMap: ["__typename": "InventoryItem"])
      }

      public static func makeInventoryLevel() -> Node {
        return Node(unsafeResultMap: ["__typename": "InventoryLevel"])
      }

      public static func makeLineItem() -> Node {
        return Node(unsafeResultMap: ["__typename": "LineItem"])
      }

      public static func makeLineItemMutable() -> Node {
        return Node(unsafeResultMap: ["__typename": "LineItemMutable"])
      }

      public static func makeLocation() -> Node {
        return Node(unsafeResultMap: ["__typename": "Location"])
      }

      public static func makeMailingAddress() -> Node {
        return Node(unsafeResultMap: ["__typename": "MailingAddress"])
      }

      public static func makeMarketingActivity() -> Node {
        return Node(unsafeResultMap: ["__typename": "MarketingActivity"])
      }

      public static func makeMarketingEvent() -> Node {
        return Node(unsafeResultMap: ["__typename": "MarketingEvent"])
      }

      public static func makeMediaImage() -> Node {
        return Node(unsafeResultMap: ["__typename": "MediaImage"])
      }

      public static func makeMetafield() -> Node {
        return Node(unsafeResultMap: ["__typename": "Metafield"])
      }

      public static func makeMetafieldStorefrontVisibility() -> Node {
        return Node(unsafeResultMap: ["__typename": "MetafieldStorefrontVisibility"])
      }

      public static func makeModel3d() -> Node {
        return Node(unsafeResultMap: ["__typename": "Model3d"])
      }

      public static func makeOnlineStoreArticle() -> Node {
        return Node(unsafeResultMap: ["__typename": "OnlineStoreArticle"])
      }

      public static func makeOnlineStoreBlog() -> Node {
        return Node(unsafeResultMap: ["__typename": "OnlineStoreBlog"])
      }

      public static func makeOnlineStorePage() -> Node {
        return Node(unsafeResultMap: ["__typename": "OnlineStorePage"])
      }

      public static func makeOrderDisputeSummary() -> Node {
        return Node(unsafeResultMap: ["__typename": "OrderDisputeSummary"])
      }

      public static func makeOrderTransaction() -> Node {
        return Node(unsafeResultMap: ["__typename": "OrderTransaction"])
      }

      public static func makePriceList() -> Node {
        return Node(unsafeResultMap: ["__typename": "PriceList"])
      }

      public static func makePriceRule() -> Node {
        return Node(unsafeResultMap: ["__typename": "PriceRule"])
      }

      public static func makePriceRuleDiscountCode() -> Node {
        return Node(unsafeResultMap: ["__typename": "PriceRuleDiscountCode"])
      }

      public static func makePrivateMetafield() -> Node {
        return Node(unsafeResultMap: ["__typename": "PrivateMetafield"])
      }

      public static func makeProduct() -> Node {
        return Node(unsafeResultMap: ["__typename": "Product"])
      }

      public static func makeProductOption() -> Node {
        return Node(unsafeResultMap: ["__typename": "ProductOption"])
      }

      public static func makeProductVariant() -> Node {
        return Node(unsafeResultMap: ["__typename": "ProductVariant"])
      }

      public static func makePublication() -> Node {
        return Node(unsafeResultMap: ["__typename": "Publication"])
      }

      public static func makeRefund() -> Node {
        return Node(unsafeResultMap: ["__typename": "Refund"])
      }

      public static func makeSavedSearch() -> Node {
        return Node(unsafeResultMap: ["__typename": "SavedSearch"])
      }

      public static func makeScriptTag() -> Node {
        return Node(unsafeResultMap: ["__typename": "ScriptTag"])
      }

      public static func makeSellingPlan() -> Node {
        return Node(unsafeResultMap: ["__typename": "SellingPlan"])
      }

      public static func makeSellingPlanGroup() -> Node {
        return Node(unsafeResultMap: ["__typename": "SellingPlanGroup"])
      }

      public static func makeShop() -> Node {
        return Node(unsafeResultMap: ["__typename": "Shop"])
      }

      public static func makeShopPolicy() -> Node {
        return Node(unsafeResultMap: ["__typename": "ShopPolicy"])
      }

      public static func makeShopifyPaymentsAccount() -> Node {
        return Node(unsafeResultMap: ["__typename": "ShopifyPaymentsAccount"])
      }

      public static func makeShopifyPaymentsBankAccount() -> Node {
        return Node(unsafeResultMap: ["__typename": "ShopifyPaymentsBankAccount"])
      }

      public static func makeShopifyPaymentsDispute() -> Node {
        return Node(unsafeResultMap: ["__typename": "ShopifyPaymentsDispute"])
      }

      public static func makeShopifyPaymentsPayout() -> Node {
        return Node(unsafeResultMap: ["__typename": "ShopifyPaymentsPayout"])
      }

      public static func makeShopifyPaymentsVerification() -> Node {
        return Node(unsafeResultMap: ["__typename": "ShopifyPaymentsVerification"])
      }

      public static func makeStorefrontAccessToken() -> Node {
        return Node(unsafeResultMap: ["__typename": "StorefrontAccessToken"])
      }

      public static func makeSubscriptionBillingAttempt() -> Node {
        return Node(unsafeResultMap: ["__typename": "SubscriptionBillingAttempt"])
      }

      public static func makeSubscriptionContract() -> Node {
        return Node(unsafeResultMap: ["__typename": "SubscriptionContract"])
      }

      public static func makeSubscriptionDraft() -> Node {
        return Node(unsafeResultMap: ["__typename": "SubscriptionDraft"])
      }

      public static func makeTenderTransaction() -> Node {
        return Node(unsafeResultMap: ["__typename": "TenderTransaction"])
      }

      public static func makeTransactionFee() -> Node {
        return Node(unsafeResultMap: ["__typename": "TransactionFee"])
      }

      public static func makeVideo() -> Node {
        return Node(unsafeResultMap: ["__typename": "Video"])
      }

      public static func makeWebhookSubscription() -> Node {
        return Node(unsafeResultMap: ["__typename": "WebhookSubscription"])
      }

      public static func makeOrder(id: GraphQLID, createdAt: String, discountCode: String? = nil, subtotalPriceSet: AsOrder.SubtotalPriceSet? = nil, totalShippingPriceSet: AsOrder.TotalShippingPriceSet, totalTaxSet: AsOrder.TotalTaxSet? = nil, cartDiscountAmountSet: AsOrder.CartDiscountAmountSet? = nil, totalPriceSet: AsOrder.TotalPriceSet) -> Node {
        return Node(unsafeResultMap: ["__typename": "Order", "id": id, "createdAt": createdAt, "discountCode": discountCode, "subtotalPriceSet": subtotalPriceSet.flatMap { (value: AsOrder.SubtotalPriceSet) -> ResultMap in value.resultMap }, "totalShippingPriceSet": totalShippingPriceSet.resultMap, "totalTaxSet": totalTaxSet.flatMap { (value: AsOrder.TotalTaxSet) -> ResultMap in value.resultMap }, "cartDiscountAmountSet": cartDiscountAmountSet.flatMap { (value: AsOrder.CartDiscountAmountSet) -> ResultMap in value.resultMap }, "totalPriceSet": totalPriceSet.resultMap])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var asOrder: AsOrder? {
        get {
          if !AsOrder.possibleTypes.contains(__typename) { return nil }
          return AsOrder(unsafeResultMap: resultMap)
        }
        set {
          guard let newValue = newValue else { return }
          resultMap = newValue.resultMap
        }
      }

      public struct AsOrder: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Order"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("createdAt", type: .nonNull(.scalar(String.self))),
            GraphQLField("discountCode", type: .scalar(String.self)),
            GraphQLField("subtotalPriceSet", type: .object(SubtotalPriceSet.selections)),
            GraphQLField("totalShippingPriceSet", type: .nonNull(.object(TotalShippingPriceSet.selections))),
            GraphQLField("totalTaxSet", type: .object(TotalTaxSet.selections)),
            GraphQLField("cartDiscountAmountSet", type: .object(CartDiscountAmountSet.selections)),
            GraphQLField("totalPriceSet", type: .nonNull(.object(TotalPriceSet.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, createdAt: String, discountCode: String? = nil, subtotalPriceSet: SubtotalPriceSet? = nil, totalShippingPriceSet: TotalShippingPriceSet, totalTaxSet: TotalTaxSet? = nil, cartDiscountAmountSet: CartDiscountAmountSet? = nil, totalPriceSet: TotalPriceSet) {
          self.init(unsafeResultMap: ["__typename": "Order", "id": id, "createdAt": createdAt, "discountCode": discountCode, "subtotalPriceSet": subtotalPriceSet.flatMap { (value: SubtotalPriceSet) -> ResultMap in value.resultMap }, "totalShippingPriceSet": totalShippingPriceSet.resultMap, "totalTaxSet": totalTaxSet.flatMap { (value: TotalTaxSet) -> ResultMap in value.resultMap }, "cartDiscountAmountSet": cartDiscountAmountSet.flatMap { (value: CartDiscountAmountSet) -> ResultMap in value.resultMap }, "totalPriceSet": totalPriceSet.resultMap])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// A globally-unique identifier.
        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        /// Date and time when the order was created in Shopify.
        public var createdAt: String {
          get {
            return resultMap["createdAt"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdAt")
          }
        }

        /// The discount code used for the order.
        public var discountCode: String? {
          get {
            return resultMap["discountCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "discountCode")
          }
        }

        /// The subtotal of the line items and their discounts, which doesn't contain shipping costs and shipping discounts.
        public var subtotalPriceSet: SubtotalPriceSet? {
          get {
            return (resultMap["subtotalPriceSet"] as? ResultMap).flatMap { SubtotalPriceSet(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "subtotalPriceSet")
          }
        }

        /// The total amount charged for shipping the order. The total isn't adjusted for discounts, returns, or removals.
        public var totalShippingPriceSet: TotalShippingPriceSet {
          get {
            return TotalShippingPriceSet(unsafeResultMap: resultMap["totalShippingPriceSet"]! as! ResultMap)
          }
          set {
            resultMap.updateValue(newValue.resultMap, forKey: "totalShippingPriceSet")
          }
        }

        /// The total of all taxes applied to the order. The total isn't adjusted for returns or removals. For the total with returns or removals, use `currentTotalTaxSet`.
        public var totalTaxSet: TotalTaxSet? {
          get {
            return (resultMap["totalTaxSet"] as? ResultMap).flatMap { TotalTaxSet(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "totalTaxSet")
          }
        }

        /// The total order-level discount amount, before returns, in shop and presentment currencies.
        public var cartDiscountAmountSet: CartDiscountAmountSet? {
          get {
            return (resultMap["cartDiscountAmountSet"] as? ResultMap).flatMap { CartDiscountAmountSet(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "cartDiscountAmountSet")
          }
        }

        /// Total amount of the order, including taxes and discounts. The total isn't adjusted for returns or removals. For the total with returns or removals, use `currentTotalPriceSet`.
        public var totalPriceSet: TotalPriceSet {
          get {
            return TotalPriceSet(unsafeResultMap: resultMap["totalPriceSet"]! as! ResultMap)
          }
          set {
            resultMap.updateValue(newValue.resultMap, forKey: "totalPriceSet")
          }
        }

        public struct SubtotalPriceSet: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["MoneyBag"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("presentmentMoney", type: .nonNull(.object(PresentmentMoney.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(presentmentMoney: PresentmentMoney) {
            self.init(unsafeResultMap: ["__typename": "MoneyBag", "presentmentMoney": presentmentMoney.resultMap])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// Amount in presentment currency.
          public var presentmentMoney: PresentmentMoney {
            get {
              return PresentmentMoney(unsafeResultMap: resultMap["presentmentMoney"]! as! ResultMap)
            }
            set {
              resultMap.updateValue(newValue.resultMap, forKey: "presentmentMoney")
            }
          }

          public struct PresentmentMoney: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["MoneyV2"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("amount", type: .nonNull(.scalar(String.self))),
                GraphQLField("currencyCode", type: .nonNull(.scalar(CurrencyCode.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(amount: String, currencyCode: CurrencyCode) {
              self.init(unsafeResultMap: ["__typename": "MoneyV2", "amount": amount, "currencyCode": currencyCode])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            /// Decimal money amount.
            public var amount: String {
              get {
                return resultMap["amount"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "amount")
              }
            }

            /// Currency of the money.
            public var currencyCode: CurrencyCode {
              get {
                return resultMap["currencyCode"]! as! CurrencyCode
              }
              set {
                resultMap.updateValue(newValue, forKey: "currencyCode")
              }
            }
          }
        }

        public struct TotalShippingPriceSet: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["MoneyBag"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("presentmentMoney", type: .nonNull(.object(PresentmentMoney.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(presentmentMoney: PresentmentMoney) {
            self.init(unsafeResultMap: ["__typename": "MoneyBag", "presentmentMoney": presentmentMoney.resultMap])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// Amount in presentment currency.
          public var presentmentMoney: PresentmentMoney {
            get {
              return PresentmentMoney(unsafeResultMap: resultMap["presentmentMoney"]! as! ResultMap)
            }
            set {
              resultMap.updateValue(newValue.resultMap, forKey: "presentmentMoney")
            }
          }

          public struct PresentmentMoney: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["MoneyV2"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("amount", type: .nonNull(.scalar(String.self))),
                GraphQLField("currencyCode", type: .nonNull(.scalar(CurrencyCode.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(amount: String, currencyCode: CurrencyCode) {
              self.init(unsafeResultMap: ["__typename": "MoneyV2", "amount": amount, "currencyCode": currencyCode])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            /// Decimal money amount.
            public var amount: String {
              get {
                return resultMap["amount"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "amount")
              }
            }

            /// Currency of the money.
            public var currencyCode: CurrencyCode {
              get {
                return resultMap["currencyCode"]! as! CurrencyCode
              }
              set {
                resultMap.updateValue(newValue, forKey: "currencyCode")
              }
            }
          }
        }

        public struct TotalTaxSet: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["MoneyBag"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("presentmentMoney", type: .nonNull(.object(PresentmentMoney.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(presentmentMoney: PresentmentMoney) {
            self.init(unsafeResultMap: ["__typename": "MoneyBag", "presentmentMoney": presentmentMoney.resultMap])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// Amount in presentment currency.
          public var presentmentMoney: PresentmentMoney {
            get {
              return PresentmentMoney(unsafeResultMap: resultMap["presentmentMoney"]! as! ResultMap)
            }
            set {
              resultMap.updateValue(newValue.resultMap, forKey: "presentmentMoney")
            }
          }

          public struct PresentmentMoney: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["MoneyV2"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("amount", type: .nonNull(.scalar(String.self))),
                GraphQLField("currencyCode", type: .nonNull(.scalar(CurrencyCode.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(amount: String, currencyCode: CurrencyCode) {
              self.init(unsafeResultMap: ["__typename": "MoneyV2", "amount": amount, "currencyCode": currencyCode])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            /// Decimal money amount.
            public var amount: String {
              get {
                return resultMap["amount"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "amount")
              }
            }

            /// Currency of the money.
            public var currencyCode: CurrencyCode {
              get {
                return resultMap["currencyCode"]! as! CurrencyCode
              }
              set {
                resultMap.updateValue(newValue, forKey: "currencyCode")
              }
            }
          }
        }

        public struct CartDiscountAmountSet: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["MoneyBag"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("presentmentMoney", type: .nonNull(.object(PresentmentMoney.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(presentmentMoney: PresentmentMoney) {
            self.init(unsafeResultMap: ["__typename": "MoneyBag", "presentmentMoney": presentmentMoney.resultMap])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// Amount in presentment currency.
          public var presentmentMoney: PresentmentMoney {
            get {
              return PresentmentMoney(unsafeResultMap: resultMap["presentmentMoney"]! as! ResultMap)
            }
            set {
              resultMap.updateValue(newValue.resultMap, forKey: "presentmentMoney")
            }
          }

          public struct PresentmentMoney: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["MoneyV2"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("amount", type: .nonNull(.scalar(String.self))),
                GraphQLField("currencyCode", type: .nonNull(.scalar(CurrencyCode.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(amount: String, currencyCode: CurrencyCode) {
              self.init(unsafeResultMap: ["__typename": "MoneyV2", "amount": amount, "currencyCode": currencyCode])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            /// Decimal money amount.
            public var amount: String {
              get {
                return resultMap["amount"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "amount")
              }
            }

            /// Currency of the money.
            public var currencyCode: CurrencyCode {
              get {
                return resultMap["currencyCode"]! as! CurrencyCode
              }
              set {
                resultMap.updateValue(newValue, forKey: "currencyCode")
              }
            }
          }
        }

        public struct TotalPriceSet: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["MoneyBag"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("presentmentMoney", type: .nonNull(.object(PresentmentMoney.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(presentmentMoney: PresentmentMoney) {
            self.init(unsafeResultMap: ["__typename": "MoneyBag", "presentmentMoney": presentmentMoney.resultMap])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// Amount in presentment currency.
          public var presentmentMoney: PresentmentMoney {
            get {
              return PresentmentMoney(unsafeResultMap: resultMap["presentmentMoney"]! as! ResultMap)
            }
            set {
              resultMap.updateValue(newValue.resultMap, forKey: "presentmentMoney")
            }
          }

          public struct PresentmentMoney: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["MoneyV2"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("amount", type: .nonNull(.scalar(String.self))),
                GraphQLField("currencyCode", type: .nonNull(.scalar(CurrencyCode.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(amount: String, currencyCode: CurrencyCode) {
              self.init(unsafeResultMap: ["__typename": "MoneyV2", "amount": amount, "currencyCode": currencyCode])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            /// Decimal money amount.
            public var amount: String {
              get {
                return resultMap["amount"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "amount")
              }
            }

            /// Currency of the money.
            public var currencyCode: CurrencyCode {
              get {
                return resultMap["currencyCode"]! as! CurrencyCode
              }
              set {
                resultMap.updateValue(newValue, forKey: "currencyCode")
              }
            }
          }
        }
      }
    }
  }
}

public final class ShopDetailsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query ShopDetails {
      shopLocales {
        __typename
        locale
        name
        primary
        published
      }
      shop {
        __typename
        customerAccounts
      }
    }
    """

  public let operationName: String = "ShopDetails"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["QueryRoot"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("shopLocales", type: .nonNull(.list(.nonNull(.object(ShopLocale.selections))))),
        GraphQLField("shop", type: .nonNull(.object(Shop.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(shopLocales: [ShopLocale], shop: Shop) {
      self.init(unsafeResultMap: ["__typename": "QueryRoot", "shopLocales": shopLocales.map { (value: ShopLocale) -> ResultMap in value.resultMap }, "shop": shop.resultMap])
    }

    /// List of locales available on a shop.
    public var shopLocales: [ShopLocale] {
      get {
        return (resultMap["shopLocales"] as! [ResultMap]).map { (value: ResultMap) -> ShopLocale in ShopLocale(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: ShopLocale) -> ResultMap in value.resultMap }, forKey: "shopLocales")
      }
    }

    /// Returns the Shop resource corresponding to the access token used in the request. The Shop resource contains
    /// business and store management settings for the shop.
    public var shop: Shop {
      get {
        return Shop(unsafeResultMap: resultMap["shop"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "shop")
      }
    }

    public struct ShopLocale: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ShopLocale"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("locale", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("primary", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("published", type: .nonNull(.scalar(Bool.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(locale: String, name: String, primary: Bool, published: Bool) {
        self.init(unsafeResultMap: ["__typename": "ShopLocale", "locale": locale, "name": name, "primary": primary, "published": published])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// Locale ISO code.
      public var locale: String {
        get {
          return resultMap["locale"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "locale")
        }
      }

      /// Human-readable locale name.
      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      /// Whether or not this is the default locale for the shop.
      public var primary: Bool {
        get {
          return resultMap["primary"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "primary")
        }
      }

      /// Whether or not the locale is visible to buyers.
      public var published: Bool {
        get {
          return resultMap["published"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "published")
        }
      }
    }

    public struct Shop: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Shop"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("customerAccounts", type: .nonNull(.scalar(ShopCustomerAccountsSetting.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(customerAccounts: ShopCustomerAccountsSetting) {
        self.init(unsafeResultMap: ["__typename": "Shop", "customerAccounts": customerAccounts])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// Whether customer accounts are required, optional, or disabled for the shop.
      public var customerAccounts: ShopCustomerAccountsSetting {
        get {
          return resultMap["customerAccounts"]! as! ShopCustomerAccountsSetting
        }
        set {
          resultMap.updateValue(newValue, forKey: "customerAccounts")
        }
      }
    }
  }
}
