//
//  SavedAddressesVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 02/04/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics

/**This class provides the list of customer addresses and allow to select one and add new.*/
class customerAddressCell: UITableViewCell {
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
}

class SavedAddressesVC: UIViewController{
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var tblAddressListing: UITableView!
    @IBOutlet weak var btnAddAddress: UIControl!
    @IBOutlet weak var lblAddAddress: UILabel!
    
    var selectedID: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.lblScreenTitle.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.lblAddAddress.font = UIFont.appSemiBoldFontWith(size: AppFontSize.size17)
        self.tblAddressListing.tableFooterView = UIView()
        self.tblAddressListing.estimatedRowHeight = 80
        self.tblAddressListing.rowHeight = UITableView.automaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        FirebaseEventsLog.request.trackScreen(screenName: "Address list", className: "SavedAddressesVC")
        self.btnAddAddress.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblAddAddress.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
    }
    
    override func viewDidAppear(_ animated: Bool) {
     
        if let accesstoken = UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken) {
            
            self.getCustomerAddress(accessToken: accesstoken, addresses: 10)
            
        }
        self.tblAddressListing.reloadData()
    }
    
    
    func getCustomerAddress(accessToken: String, addresses: Int32){
        ShopifyCustomerAddress.shared.getCustomerAddress(accessToken: accessToken, addresses: addresses) { customer, error in
            
            if let defaultAddress = customer?.defaultAddress {
                self.selectedID = defaultAddress.id.rawValue
            }
            if let result = customer?.addresses {
                GlobalObjects.CustomerAddresses.removeAll()
                let addresses = result.edges.map {$0.node}
                
                addresses.forEach { address in
                    print(address.id.rawValue)
                    GlobalObjects.CustomerAddresses.append(CustomerAddress(id: address.id.rawValue, firstName: address.firstName, lastName: address.lastName!, address1: address.address1, address2: address.address2, phone: address.phone, city: address.city, state: address.province, country: address.country, zipcode: address.zip))
                }
                print(GlobalObjects.CustomerAddresses.count)
                self.tblAddressListing.reloadData()
            }
        }
    }
    
    func mutationToDefaultAddress(accessToken: String, addressID: String){
        ClientMutations.shared.makeDefaultAddress(accessToken: accessToken, addressID: addressID) { errors in
            if errors.count > 0 {
                let message = errors[0].message
                let alert = UIAlertController(title: "", message: "\(message)", preferredStyle:.alert)
                
                alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                DispatchQueue.main.async {
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                self.tblAddressListing.reloadData()
                Timer.scheduledTimer(withTimeInterval: 0.30, repeats: false) { (timer) in
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
        }
    }
    
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAddNewAddress(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyBoard.instantiateViewController(withIdentifier: "CustomerAddressVC") as? CustomerAddressVC
        self.present(detailVC!, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SavedAddressesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return GlobalObjects.CustomerAddresses.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAddressListing.dequeueReusableCell(withIdentifier:"customerAddressCell", for: indexPath) as! customerAddressCell
        
        let address = "\(GlobalObjects.CustomerAddresses[indexPath.row].firstName ?? "") \(GlobalObjects.CustomerAddresses[indexPath.row].lastName ?? "")\n\(GlobalObjects.CustomerAddresses[indexPath.row].address1 ?? ""),\(GlobalObjects.CustomerAddresses[indexPath.row].address2 ?? "")\n\(GlobalObjects.CustomerAddresses[indexPath.row].city ?? ""),\(GlobalObjects.CustomerAddresses[indexPath.row].state ?? ""),\(GlobalObjects.CustomerAddresses[indexPath.row].country ?? "")-\(GlobalObjects.CustomerAddresses[indexPath.row].zipcode ?? "")\n\(GlobalObjects.CustomerAddresses[indexPath.row].phone ?? "")"
        
        cell.lblAddress.text! = address
        cell.lblAddress.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        //        cell.btnDelete.tag = indexPath.row
        //        cell.btnDelete.addTarget(self, action: #selector(self.deleteAddress(sender:)), for: .touchUpInside)
        
        if selectedID == GlobalObjects.CustomerAddresses[indexPath.row].id {
            
            cell.accessoryType = .checkmark
            cell.backgroundColor = .white
            
        }
        
        return cell
    }
    
    func tableView (_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        if let presenter = presentingViewController as? CheckoutVC {
        //            presenter.shippingAddress = GlobalObjects.CustomerAddresses[indexPath.row]
        //        }
        let accessToken =  UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken)
        self.mutationToDefaultAddress(accessToken: accessToken!, addressID: GlobalObjects.CustomerAddresses[indexPath.row].id!)
        let imageDataDict = ["address":GlobalObjects.CustomerAddresses[indexPath.row]]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setAddress"), object: nil, userInfo: imageDataDict)
        
    }
}
