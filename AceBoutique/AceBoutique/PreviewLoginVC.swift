////
////  PreviewLoginVC.swift
////  AceBoutique
////
////  Created by Aashiyana Shaikh on 22/09/21.
////
//
//import UIKit
//import CryptoSwift
//import MobileBuySDK
//
//class PreviewLoginVC: UIViewController, UITextFieldDelegate, ForgotPasswordVCDelegate {
//
//
//    @IBOutlet weak var ImgEmail: UIImageView!
//    @IBOutlet weak var ImgPassword: UIImageView!
//
//    @IBOutlet weak var txtPassword: UITextField!
//    @IBOutlet weak var txtEmail: UITextField!
//
//    @IBOutlet weak var curvedView: UIView!
//    @IBOutlet weak var baseView: UIView!
//
//
//    var arrStoreLists: StoreLists!
//
//
//
//    //MARK: - Forgot Password Delegate -
//
//    func sendLinkActionPerformed(email: String) {
//        if email != "" {
//            let stringValue:String? = self.emaiValidationForm(emailId: email)
//
//            if ((stringValue?.length)! > 0){
//
//                let alert = UIAlertController(title: "", message: stringValue!, preferredStyle:.alert)
//
//                alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
//                DispatchQueue.main.async{
//                    self.present(alert, animated: true, completion: nil)
//                }
//            }
//            else{
//
//                if CheckNetwork.isConnectedToNetwork() == true {
//                    self.callAPIOfStoreLists(email: email) { result, error in
//                        if result != nil {
//                            let dataList = result!.data!
//                            if dataList.count == 1 {
//                                let storeID = dataList[0].storeID!
//                                self.callAPIOfForgotPassword(email: email, storeID: storeID)
//
//                            }else{
//                                DispatchQueue.main.async {
//                                self.presentActiopnSheetStoreLists(email: email, action: "ForgotPassword")
//                                }
//                            }
//                        }
//                    }
//
//                }else{
//                    DispatchQueue.main.async {
//                        self.Showalert1(message: "no_internet_msg".localized)
//                    }
//                }
//            }
//        }else{
//            Showalert1(message: "nilEmailValidation".localized)
//        }
//    }
//
//    
//    func callAPIOfForgotPassword(email: String, storeID: String){
//        if CheckNetwork.isConnectedToNetwork() == true {
//            let session = URLSession.shared
//            let request = NSMutableURLRequest(url: NSURL(string: forgotPasswordURL)! as URL)
//            request.httpMethod = "POST"
//            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//            var params :[String: Any]?
//            params = ["email": email, "store_id":storeID]
//            do{
//                request.httpBody = try JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions())
//                let task = session.dataTask(with: request as URLRequest as URLRequest, completionHandler: {(data, response, error) in
//                    if let response = response {
//                        let nsHTTPResponse = response as! HTTPURLResponse
//                        let statusCode = nsHTTPResponse.statusCode
//                        print ("status code = \(statusCode)")
//                    }
//                    if let error = error {
//                        print ("\(error)")
//                    }
//                    if let jsonData = data {
//                        print(jsonData)
//                        DispatchQueue.main.async {
//                            self.Showalert1(message: "Reset password link sent on your email")
//                        }
//                    }
//                })
//                task.resume()
//            }catch _ {
//                print ("error making request body")
//            }
//        }else{
//            DispatchQueue.main.async {
//                self.Showalert1(message: "no_internet_msg".localized)
//            }
//        }
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        self.txtEmail.text = "kishanradadiya.ace@gmail.com"
//        self.txtPassword.text = "123456"
//        // Do any additional setup after loading the view.
//    }
//
//
//    override func viewWillLayoutSubviews() {
//
//        self.ImgEmail.image = self.ImgEmail.image!.withRenderingMode(.alwaysTemplate)
//        self.ImgEmail.tintColor = UIColor(red: 105/255, green: 124/255, blue: 179/255, alpha: 1)
//        self.ImgPassword.image = self.ImgPassword.image!.withRenderingMode(.alwaysTemplate)
//        self.ImgPassword.tintColor = UIColor(red: 105/255, green: 124/255, blue: 179/255, alpha: 1)
//
//        curvedView.clipsToBounds = true
//        curvedView.layer.cornerRadius = self.curvedView.frame.width/2
//        curvedView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
//    }
//    /*
//     // MARK: - Navigation
//
//     // In a storyboard-based application, you will often want to do a little preparation before navigation
//     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//     // Get the new view controller using segue.destination.
//     // Pass the selected object to the new view controller.
//     }
//     */
//
//
//    @IBAction func btnForgotPasswordTapped(_ sender: Any) {
//
//        let myAlert = self.storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordAlertVC") as! ForgotPasswordAlertVC
//        myAlert.delegate = self
//        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//        self.present(myAlert, animated: true, completion: nil)
//    }
//
//    @IBAction func btnLoginTapped(_ sender: Any) {
//
//        if self.txtEmail.text! != "" && self.txtPassword.text! != "" {
//
//            let stringValue:String? = self.validationForm()
//
//            if ((stringValue?.length)! > 0){
//
//                let alert = UIAlertController(title: "", message: stringValue!, preferredStyle:.alert)
//
//                alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
//                DispatchQueue.main.async{
//
//                    self.present(alert, animated: true, completion: nil)
//
//                }
//
//            }else{
//                if CheckNetwork.isConnectedToNetwork() == true {
//                    appDelegateShared.showHud()
//
//                    let session = URLSession.shared
//                    let url = "\(loginURL)"
//                    let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
//                    request.httpMethod = "POST"
//                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//                    var params :[String: Any]?
//                    params = ["email": "\(self.txtEmail.text!)",
//                              "password": "\(self.txtPassword.text!)"]
//                    do{
//                        request.httpBody = try JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions())
//                        let task = session.dataTask(with: request as URLRequest as URLRequest, completionHandler: {(data, response, error) in
//                            appDelegateShared.dismissHud()
//                            if let response = response {
//                                let nsHTTPResponse = response as! HTTPURLResponse
//                                let statusCode = nsHTTPResponse.statusCode
//                                print ("status code = \(statusCode)")
//                            }
//                            if let error = error {
//                                print ("\(error)")
//                            }
//                            if let jsonData = data {
//
//                                do {
//                                    let decodedData = try JSONDecoder().decode(StoreLists.self, from: jsonData)
//                                    let encoder = JSONEncoder()
//                                    encoder.outputFormatting = .prettyPrinted
//
//                                    do {
//                                        let jsonData = try encoder.encode(decodedData)
//
//                                        if let jsonString = String(data: jsonData, encoding: .utf8) {
//
//                                            print("Json Response String1: ",jsonString)
//
//                                            let jsonData = jsonString.data(using: .utf8)!
//                                            let objConfig = try! JSONDecoder().decode(StoreLists.self, from: jsonData)
//                                            self.arrStoreLists = nil
//                                            self.arrStoreLists = objConfig
//                                            let dataList = self.arrStoreLists.data!
//                                            DispatchQueue.main.async {
//                                            setMyUserDefaults(value: self.txtEmail.text!, key: MyUserDefaults.LoginUser)
//
//                                            }
//                                            if dataList.count == 1 {
//
//                                                configPathURL = dataList[0].configURL!
//                                                self.callAPISetConfigLayout()
//                                            }else{
//                                                DispatchQueue.main.async {
//                                                self.presentActiopnSheetStoreLists(email: "", action: "Login")
//                                                }
//                                            }
//
//                                        }
//                                    } catch {
//                                        print(error.localizedDescription)
//                                    }
//
//                                } catch {
//
//                                    print("decode error")
//                                }
//                            }
//                        })
//                        task.resume()
//                    }catch _ {
//                        appDelegateShared.dismissHud()
//                        print ("error making request body")
//                    }
//
//                }else{
//                    DispatchQueue.main.async {
//                        self.Showalert1(message: "no_internet_msg".localized)
//                    }
//                }
//            }
//
//        }else if self.txtEmail.text! == "" {
//
//            Showalert1(message: "nilEmailValidation".localized)
//
//        }else if self.txtPassword.text! == "" {
//
//            Showalert1(message: "nilPasswordValidation".localized)
//        }
//
//    }
//
//    func callAPISetConfigLayout(){
//        if CheckNetwork.isConnectedToNetwork() == true {
//
//            callAPI.shared.loadJson(fromURLString: appCountriesURL) { [self] result in
//                switch result {
//                case .success(let data):
//                    print(data)
//                    self.parseCountries(jsonData: data)
//
//                case .failure(let error):
//                    print("Loading countries Json error",error.localizedDescription)
//                }
//            }
//
//            self.callAPIOfAppLayout { completed,error  in
//                if completed {
//
//                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                    DispatchQueue.main.async {
//                        if let detailVC = storyBoard.instantiateViewController(withIdentifier: "rootController") as? RootViewController {
//                            self.present(detailVC, animated: true, completion: nil)
//                        }
//                    }
//                }else{
//                    DispatchQueue.main.async {
//                        self.Showalert1(message: error)
//                    }
//                }
//            }
//
//        }else{
//            self.Showalert1(message: "no_internet_msg".localized)
//        }
//    }
//    //MARK:- -
//
//    //Method that calls the API to fetch the config.json data and do encode/decode json
//    func callAPIOfAppLayout(completion: @escaping (Bool, String) -> Void){
//
//        //        if CheckNetwork.isConnectedToNetwork() == true {
//        callAPI.shared.loadJson(fromURLString: appConfigURL) { [self] result in
//            switch result {
//            case .success(let data):
//
//                let jsonString = callAPI.shared.parse(jsonData: data)
//                if jsonString != "" {
//
//                    print("Json Response String1: ",jsonString)
//
//                    let jsonData = jsonString.data(using: .utf8)!
//                    let objConfig = try! JSONDecoder().decode(ConfigResponse.self, from: jsonData)
//
//                    GlobalObjects.objConfigResponse = nil
//                    GlobalObjects.objConfigResponse = objConfig
//
//                    shopDomain = decryptMessage(encryptedMessage: (GlobalObjects.objConfigResponse.shopProperties?.ShopDomain!)!, encryptionKey: encyptionKey, iv: IV)!
//                    storeAPIKey = decryptMessage(encryptedMessage: (GlobalObjects.objConfigResponse.shopProperties?.APIKEY!)!, encryptionKey: encyptionKey, iv: IV)!
//                    adminToken = decryptMessage(encryptedMessage: (GlobalObjects.objConfigResponse.shopProperties?.adminToken!)!, encryptionKey: encyptionKey, iv: IV)!
//                    graphql_version = (GlobalObjects.objConfigResponse.shopProperties?.GraphQLVersion!)!
//
//                    if let language = getMyUserDefaults(key: MyUserDefaults.LANGUAGE) as? String, language != "" {
//
//                        let locale = GetCurrentLanguage()
//                        appLocale = Locale(identifier: locale)
//
//                    }
//
//                    client = Graph.Client(shopDomain: shopDomain, apiKey: storeAPIKey, locale: appLocale)
//                    //Get Left Menu settings
//                    GlobalObjects.arrMenuItems = objConfig.menu!.items!
//                    // GlobalObjects.arrMenuItems = GlobalObjects.arrMenuItems.sorted { $0.order  < $1.order }
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAPICall"), object: nil)
//                    completion(true, "")
//                }
//
//            case .failure(let error):
//                print("Loading Json error",error.localizedDescription)
//                completion(false, error.localizedDescription)
//            }
//        }
//        //        }else{
//        //            self.Showalert1(message: "no_internet_msg".localized)
//        //        }
//    }
//
//    func callAPIOfStoreLists(email:String, completion: @escaping (StoreLists?, String) -> Void) {
//
//        if CheckNetwork.isConnectedToNetwork() == true {
//            appDelegateShared.showHud()
//
//            let session = URLSession.shared
//            let request = NSMutableURLRequest(url: NSURL(string: storeListsURL)! as URL)
//            request.httpMethod = "POST"
//            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//            var params :[String: Any]?
//            params = ["email": "\(self.txtEmail.text!)"]
//            do{
//                request.httpBody = try JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions())
//                let task = session.dataTask(with: request as URLRequest as URLRequest, completionHandler: {(data, response, error) in
//                    appDelegateShared.dismissHud()
//                    if let response = response {
//                        let nsHTTPResponse = response as! HTTPURLResponse
//                        let statusCode = nsHTTPResponse.statusCode
//                        print ("status code = \(statusCode)")
//                    }
//                    if let error = error {
//                        print ("\(error)")
//                    }
//                    if let jsonData = data {
//
//                        do {
//                            let decodedData = try JSONDecoder().decode(StoreLists.self, from: jsonData)
//                            let encoder = JSONEncoder()
//                            encoder.outputFormatting = .prettyPrinted
//
//                            do {
//                                let jsonData = try encoder.encode(decodedData)
//
//                                if let jsonString = String(data: jsonData, encoding: .utf8) {
//
//                                    print("Json Response String1: ",jsonString)
//
//                                    let jsonData = jsonString.data(using: .utf8)!
//                                    let objConfig = try! JSONDecoder().decode(StoreLists.self, from: jsonData)
//                                    self.arrStoreLists = nil
//                                    self.arrStoreLists = objConfig
//                                    completion(self.arrStoreLists, "")
//                                }
//                            } catch {
//                                print(error.localizedDescription)
//                                completion(nil, error.localizedDescription)
//                            }
//
//                        } catch {
//
//                            print("decode error")
//                            completion(nil, error.localizedDescription)
//                        }
//                    }
//                })
//                task.resume()
//            }catch _ {
//                appDelegateShared.dismissHud()
//                completion(nil, "error making request body")
//                print ("error making request body")
//            }
//
//        }else{
//            DispatchQueue.main.async {
//                self.Showalert1(message: "no_internet_msg".localized)
//            }
//        }
//
//    }
//
//
//    func presentActiopnSheetStoreLists(email: String, action:String ){
//        let actionSheetAlertController: UIAlertController = UIAlertController(title: "Select your store", message: nil, preferredStyle: .actionSheet)
//        let data = self.arrStoreLists.data
//        for item in data! {
//            let actionClicked = UIAlertAction(title: item.storeURL, style: .default) { (action1) in
//                if action == "Login" {
//                configPathURL = item.configURL!
//                setMyUserDefaults(value: configPathURL, key: MyUserDefaults.ConfigPathURL)
//                self.callAPISetConfigLayout()
//                }else{
//                    //call reset Password API
//                    let storeID = item.storeID!
//                    self.callAPIOfForgotPassword(email: email, storeID: storeID)
//                }
//            }
//            actionSheetAlertController.addAction(actionClicked)
//        }
//
//        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//        actionSheetAlertController.addAction(cancelActionButton)
//
//        self.present(actionSheetAlertController, animated: true, completion: nil)
//    }
//
//
//    private func parseCountries(jsonData: Data) {
//
//        do {
//            let decodedData = try! JSONDecoder().decode(countries.self, from: jsonData)
//
//            let encoder = JSONEncoder()
//            encoder.outputFormatting = .prettyPrinted
//
//            do {
//                let jsonData = try encoder.encode(decodedData)
//
//                if let jsonString = String(data: jsonData, encoding: .utf8) {
//
//                    let jsonData = jsonString.data(using: .utf8)!
//                    let objConfig = try! JSONDecoder().decode(countries.self, from: jsonData)
//                    //                    GlobalObjects.arrCountries.removeAll()
//                    arrCountries = objConfig
//                    print("total countries: \(arrCountries.count)")
//
//                }
//            } catch {
//                print(error.localizedDescription)
//            }
//
//        } catch {
//            print("decode error")
//        }
//    }
//
//
//    func validationForm() -> String
//    {
//        var errorMessage:String? = ""
//
//        self.txtEmail.text = appDelegateShared.TrimString(string: self.txtEmail.text!)
//
//        if appDelegateShared.isValidURL(self.txtEmail.text!) == false {
//            errorMessage = "emailValidation".localized
//        }
//
//        return errorMessage!
//    }
//
//
//    func emaiValidationForm(emailId:String) -> String
//    {
//        var errorMessage:String? = ""
//
//        let trimEmail = appDelegateShared.TrimString(string: emailId)
//
//        if appDelegateShared.isValidURL(trimEmail) == false {
//            errorMessage = "emailValidation".localized
//        }
//
//        return errorMessage!
//    }
//
//}
//
//
//
