//
//  CustomButtonAlertVC.swift
//  eCentraDocs
//
//  Created by Aashiyana Shaikh on 19/03/19.
//  Copyright © 2019 Aashiyana Shaikh. All rights reserved.
//

import UIKit
import MobileBuySDK


@objc protocol CustomButtonAlertVCDelegate {
    @objc func customActionPerformed(itemAction: String)
}

class CustomButtonAlertVC: UIViewController {
    
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var viewLatestFirst: UIView!
    @IBOutlet weak var btnLatestFirst: UIButton!
    @IBOutlet weak var viewBestSelling: UIView!
    @IBOutlet weak var btnBestSelling: UIButton!
    @IBOutlet weak var viewPriceLowHigh: UIView!
    @IBOutlet weak var btnPriceLowHigh: UIButton!
    @IBOutlet weak var viewPriceHighLow: UIView!
    @IBOutlet weak var btnHighLow: UIButton!
    @IBOutlet weak var viewAtoZ: UIView!
    @IBOutlet weak var btnAtoZ: UIButton!
    @IBOutlet weak var viewZtoA: UIView!
    @IBOutlet weak var btnZtoA: UIButton!
    
    
    var Title:String!
    var Image:UIImage!
    var clientActionTitle:String!
    var operation:String!
    var itemAction:String!
    var delegate: CustomButtonAlertVCDelegate?
    var btnTintColour: UIColor!
    var SortType: Storefront.ProductCollectionSortKeys?
    var Search_Sort: Storefront.ProductSortKeys?
    var reverse: Bool!
    var visualEffectView: UIVisualEffectView!
    
    @objc override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.btnLatestFirst.titleLabel?.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        self.btnBestSelling.titleLabel?.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        self.btnPriceLowHigh.titleLabel?.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        self.btnHighLow.titleLabel?.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        self.btnAtoZ.titleLabel?.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        self.btnZtoA.titleLabel?.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
        if #available(iOS 13.0, *) {
            self.visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .systemChromeMaterialDark))
        } else {
            // Fallback on earlier versions
            self.visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .prominent))
        }
        self.visualEffectView.backgroundColor = UIColor.black
        self.visualEffectView.alpha = 0.2
        self.visualEffectView.frame = self.view.bounds
        self.visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //        visualEffectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundViewTapped)))
        self.view.insertSubview(self.visualEffectView, at: 0)
        
        alertView.layer.cornerRadius  = 2
        alertView.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        alertView.layer.shadowColor   = UIColor(white: 0.0, alpha: 1.0).cgColor
        alertView.layer.shadowOpacity = 0.3
        alertView.layer.shadowRadius  = 3.0
        
        
        if self.SortType == Storefront.ProductCollectionSortKeys.created {
            self.btnLatestFirst.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewLatestFirst.isHidden = false
            
        }
        if self.SortType == Storefront.ProductCollectionSortKeys.bestSelling {
            self.btnBestSelling.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewBestSelling.isHidden = false
            
        }
        if self.SortType == Storefront.ProductCollectionSortKeys.price && self.reverse == false {
            self.btnPriceLowHigh.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewPriceLowHigh.isHidden = false
            
        }
        if self.SortType == Storefront.ProductCollectionSortKeys.price && self.reverse == true {
            self.btnHighLow.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewPriceHighLow.isHidden = false
           
        }
        if self.SortType == Storefront.ProductCollectionSortKeys.title && self.reverse == false {
            self.btnAtoZ.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewAtoZ.isHidden = false
            
        }
        if self.SortType == Storefront.ProductCollectionSortKeys.title && self.reverse == true {
            self.btnZtoA.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewZtoA.isHidden = false
            
        }
        
        
        if self.Search_Sort == Storefront.ProductSortKeys.createdAt {
            self.btnLatestFirst.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewLatestFirst.isHidden = false
        }
        if self.Search_Sort == Storefront.ProductSortKeys.bestSelling {
            self.btnBestSelling.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewBestSelling.isHidden = false
        }
        if self.Search_Sort == Storefront.ProductSortKeys.price && self.reverse == false {
            self.btnPriceLowHigh.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewPriceLowHigh.isHidden = false
        }
        if self.Search_Sort == Storefront.ProductSortKeys.price && self.reverse == true {
            self.btnHighLow.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewPriceHighLow.isHidden = false
        }
        if self.Search_Sort == Storefront.ProductSortKeys.title && self.reverse == false {
            self.btnAtoZ.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewAtoZ.isHidden = false
        }
        if self.Search_Sort == Storefront.ProductSortKeys.title && self.reverse == true {
            self.btnZtoA.setTitleColor(UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!), for: .normal)
            self.viewZtoA.isHidden = false
        }
        
        
    }
    
    
    @IBAction func btnLatestFirstTapped(_ sender: Any) {
        hide()
        self.itemAction = "latest".localized
        if delegate != nil {
            self.viewLatestFirst.isHidden = false
            self.delegate?.customActionPerformed(itemAction: self.itemAction)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnBestSellingTapped(_ sender: Any) {
        hide()
        self.itemAction = "bestsell".localized
        if delegate != nil {
            self.viewBestSelling.isHidden = false
            self.delegate?.customActionPerformed(itemAction: self.itemAction)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnPriceLowHighTapped(_ sender: Any) {
        hide()
        self.itemAction = "pricelowtohigh".localized
        if delegate != nil {
            self.viewPriceLowHigh.isHidden = false
            self.delegate?.customActionPerformed(itemAction: self.itemAction)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnPriceHighLowTapped(_ sender: Any) {
        hide()
        self.itemAction = "pricehightolow".localized
        
        if delegate != nil {
            self.viewPriceHighLow.isHidden = false
            self.delegate?.customActionPerformed(itemAction: self.itemAction)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnTitleAtoZTapped(_ sender: Any) {
        hide()
        self.itemAction = "titleatoz".localized
        if delegate != nil {
            self.viewAtoZ.isHidden = false
            self.delegate?.customActionPerformed(itemAction: self.itemAction)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnTitleZtoATapped(_ sender: Any) {
        hide()
        self.itemAction = "titleztoa".localized
        if delegate != nil {
            self.viewZtoA.isHidden = false
            self.delegate?.customActionPerformed(itemAction: self.itemAction)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    /// Hide Alert Controller
    @objc private func hide()
    {
        self.view.endEditing(true)
        self.alertView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            self.alertView.alpha = 0.0
            self.alertView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            self.alertView.center    = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height/2.0)-5)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.25, delay: 0.05, options: .curveEaseIn, animations: { () -> Void in
            self.view.alpha = 0.0
            
        }) { (completed) -> Void in
            
            self.view.removeFromSuperview()
            self.removeFromParent()
            
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
