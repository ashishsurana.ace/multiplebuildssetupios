//
//  ForgotPasswordAlertVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 25/03/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics


@objc protocol ForgotPasswordVCDelegate {
    @objc func sendLinkActionPerformed(email: String)
}

class ForgotPasswordAlertVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSendLink: UIControl!
    @IBOutlet weak var btnCloseAlert: UIControl!
    @IBOutlet weak var lblSendLink: UILabel!
    
    var delegate: ForgotPasswordVCDelegate?
    var visualEffectView: UIVisualEffectView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if #available(iOS 13.0, *) {
            self.visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .systemChromeMaterialDark))
        } else {
            // Fallback on earlier versions
            self.visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .prominent))
        }
        visualEffectView.backgroundColor = UIColor.black
        visualEffectView.alpha = 0.3
        visualEffectView.frame = self.view.bounds
        visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //        visualEffectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundViewTapped)))
        self.view.insertSubview(visualEffectView, at: 0)
        
        alertView.layer.cornerRadius  = 5
        alertView.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        alertView.layer.shadowColor   = UIColor(white: 0.0, alpha: 1.0).cgColor
        alertView.layer.shadowOpacity = 0.3
        alertView.layer.shadowRadius  = 3.0
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtEmail.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        FirebaseEventsLog.request.trackScreen(screenName: "Forgot password", className: "ForgotPasswordAlertVC")
        self.txtEmail.becomeFirstResponder()
    }
    
    @IBAction func btnCloseAlertTapped(_ sender: Any) {
        self.view.endEditing(true)
        self.hide()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSendLinkTapped(_ sender: Any) {
        self.hide()
        //        if txtEmail.text! != "" {
        if delegate != nil {
            self.delegate?.sendLinkActionPerformed(email: self.txtEmail.text!)
            self.dismiss(animated: true, completion: nil)
        }
        //        }
    }
    
    
    /// Hide Alert Controller
    @objc private func hide()
    {
        self.view.endEditing(true)
        self.alertView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            self.alertView.alpha = 0.0
            self.alertView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            self.alertView.center    = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height/2.0)-5)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.25, delay: 0.05, options: .curveEaseIn, animations: { () -> Void in
            self.view.alpha = 0.0
            
        }) { (completed) -> Void in
            
            self.view.removeFromSuperview()
            self.removeFromParent()
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
