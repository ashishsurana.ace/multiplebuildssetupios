//
//  CustomLanguagePickerVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 12/07/21.
//

import Foundation
import UIKit


@objc protocol CustomLanguagePickerVCDelegate {
    
    @objc func changeSelectedLanguage(locale: shopLocales)
}


class languagesCell: UITableViewCell {
    
    @IBOutlet weak var lblLanguages: UILabel!
    @IBOutlet weak var btnSelectedLanguage: UIView!
}

class CustomLanguagePickerVC: UIViewController {
    
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var btnCloseAlert: UIButton!
    @IBOutlet weak var tblLanguages: UITableView!
    @IBOutlet weak var heightconstraint: NSLayoutConstraint!
    
    var visualEffectView: UIVisualEffectView!
    var delegate: CustomLanguagePickerVCDelegate?
    var selectedLanguage:String! = ""
    
    
    @objc override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCloseAlert(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //        self.btnCloseAlert.titleLabel?.font = UIFont.appSemiBoldFontWith(size: AppFontSize.size19)
        
        if #available(iOS 13.0, *) {
            self.visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .systemChromeMaterialDark))
        } else {
            // Fallback on earlier versions
            self.visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .prominent))
        }
        self.visualEffectView.backgroundColor = UIColor.black
        self.visualEffectView.alpha = 0.2
        self.visualEffectView.frame = self.view.bounds
        self.visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //        visualEffectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundViewTapped)))
        self.view.insertSubview(self.visualEffectView, at: 0)
        
        alertView.layer.cornerRadius  = 2
        alertView.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        alertView.layer.shadowColor   = UIColor(white: 0.0, alpha: 1.0).cgColor
        alertView.layer.shadowOpacity = 0.3
        alertView.layer.shadowRadius  = 3.0
        self.tblLanguages.tableFooterView = UIView()
        
        self.heightconstraint.constant = self.tblLanguages.contentSize.height + 86
        self.tblLanguages.reloadData()
    }
    
    
    /// Hide Alert Controller
    @objc private func hide()
    {
        self.view.endEditing(true)
        self.alertView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            self.alertView.alpha = 0.0
            self.alertView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            self.alertView.center    = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height/2.0)-5)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.25, delay: 0.05, options: .curveEaseIn, animations: { () -> Void in
            self.view.alpha = 0.0
            
        }) { (completed) -> Void in
            
            self.view.removeFromSuperview()
            self.removeFromParent()
            
        }
    }
}


extension CustomLanguagePickerVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        
        return GlobalObjects.arrLocales.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblLanguages.dequeueReusableCell(withIdentifier:"languagesCell", for: indexPath) as! languagesCell
        
        cell.lblLanguages.text! = GlobalObjects.arrLocales[indexPath.row].name
        
        let loc = GetCurrentLanguage()
        if loc == GlobalObjects.arrLocales[indexPath.row].locale {
            cell.lblLanguages.textColor =  UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
            cell.btnSelectedLanguage.isHidden = false
            cell.btnSelectedLanguage.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        }else{
            
            cell.lblLanguages.textColor =  .black
            cell.btnSelectedLanguage.isHidden = true
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            let objSelected = GlobalObjects.arrLocales[indexPath.row]
            setMyUserDefaults(value: GlobalObjects.arrLocales[indexPath.row].name, key: MyUserDefaults.LANGUAGE)
            self.delegate?.changeSelectedLanguage(locale: objSelected)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
