//
//  WebViewCheckoutVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 18/08/21.
//

import UIKit
import WebKit
import MobileBuySDK
import FirebaseAnalytics
import Apollo
import Firebase

class WebViewCheckoutVC: UIViewController {

    
    var url: URL!
    var accessToken: String?
    var checkoutId: String = ""
    var Cart_DB = CartQueries()
    var OrderDetail: OrderDetailsByIdQuery.Data.Node.AsOrder!
    var isOrderCompleted: Bool = false
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if isFromNotification == true {
            if UserDefaults.standard.object(forKey: MyUserDefaults.NotificationPayload) != nil {
                let userInfo = getMyUserDefaults(key: MyUserDefaults.NotificationPayload) as! [AnyHashable: Any]
//                guard let apnsData = userInfo["data"] as? [String: Any] else {
//                    return
//                }
                
                guard let checkout_url = userInfo["checkout_url"] as? String else {
                    return
                }
                
                self.url = URL(string: checkout_url)
                self.load(url: self.url)
            }
        }else{
        self.load(url: self.url)
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnBackTapped(_ sender: Any) {
        if isFromNotification == true {
            isFromNotification = false
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let rootView = storyBoard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
            UIApplication.shared.windows.first?.rootViewController = rootView
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }else{
//            if UserDefaults.standard.object(forKey: MyUserDefaults.CheckoutTime) != nil && CheckNetwork.isConnectedToNetwork() == true{
//                if let accessToken = UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken) {
//                    self.getLastOrder(accessToken: accessToken)
//                }else{
                    self.dismiss(animated: true, completion: nil)
//                }
            }
    }
  
    
//    func getLastOrder(accessToken: String) {
//        if CheckNetwork.isConnectedToNetwork() == true {
//        QueryLastOrder.shared.getLastOrder(accessToken: accessToken) { response, error in
//            let orders = response!.edges.map {$0.node}
//            if orders.count != 0 {
//            orders.forEach { items in
//
//                let orderDate = items.processedAt
//                let checkoutDateTime = UserDefaults.standard.value(forKey: MyUserDefaults.CheckoutTime) as! Date
//                if orderDate > checkoutDateTime  {
//                    let issucess = self.Cart_DB.deleteAllCartItem()
//                    if  issucess == true {
//                        Messaging.messaging().unsubscribe(fromTopic: "cart")
//                        removeMyUserDefaults(key: MyUserDefaults.CheckoutTime)
//                        DispatchQueue.global(qos: .userInitiated).async {
//                            self.getDataFromApollo(id: items.id.rawValue)
//                            DispatchQueue.main.async {
//                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                let rootView = storyBoard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
//                                UIApplication.shared.windows.first?.rootViewController = rootView
//                                UIApplication.shared.windows.first?.makeKeyAndVisible()
//                            }
//                        }
//
//                    }
//                }else{
//
//                    self.dismiss(animated: true, completion: nil)
//                }
//            }
//            }else{
//                self.dismiss(animated: true, completion: nil)
//            }
//        }
//    }
//    }
    
    // ----------------------------------
    //  MARK: - Request -
    //
    private func load(url: URL) {
        let request = URLRequest(url: self.url)
        
        
        self.webView.load(request)
        
        //MARK: - Check Checkout Update -
        if isFromNotification == false {
            QueryCompleteCheckout.shared.callCheckoutAPI(checkoutId: self.checkoutId) { response, error in
                if let checkout = response {
                    
                    let orderID  = checkout.order?.id
                    print("ORDER ID:", orderID!)
                    
                    let issucess = self.Cart_DB.deleteAllCartItem()
                    if  issucess == true {
                        Messaging.messaging().unsubscribe(fromTopic: "cart")
//                        removeMyUserDefaults(key: MyUserDefaults.CheckoutTime)
                        DispatchQueue.global(qos: .userInitiated).async {
                            self.getDataFromApollo(id: orderID!.rawValue)
                        }
                    }
                    
                    let orderNumber = checkout.order?.name
                    
                    let stralert1 = "msgOrderComplete1".localized
                    let stralert2 = "msgOrderComplete2".localized
                    let alert = UIAlertController(title: "", message: "\(stralert1)"+" \(orderNumber!) "+"\(stralert2)", preferredStyle:.alert)
                    
                    alert.addAction(UIAlertAction(title: "btnOk".localized, style: .default, handler: { (alert) in
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let rootView = storyBoard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
                        UIApplication.shared.windows.first?.rootViewController = rootView
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
                    }))
                    
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    print("Order response not available.")
                }
            }
        }
    }
    
    func getDataFromApollo(id:String) {
        localeId = GetCurrentLanguage()
        Network.request.apollo.fetch(query: OrderDetailsByIdQuery(id: GraphQL.ID(rawValue:"\(String(describing:id))").rawValue)) { result in
//            print(result)
            switch result {
            case .success(let graphQLResult):
                guard let data = try? result.get().data else { return }
                
                if graphQLResult.data != nil {
                    print("Oreder Id",graphQLResult.data!.node!.asOrder!)
                    
                    self.OrderDetail = graphQLResult.data!.node!.asOrder!
                    //                    DispatchQueue.global(qos: .userInitiated).async {
                    self.getOrderDetails(accessToken: self.accessToken!, orderId: id, lineItems: 250, ImgHeight: 600)
                    //                    }
                }
                
            case .failure(let error):
                print("WebViewVC Error loading data \(error.localizedDescription)")
            }
        }
    }
        
    func getOrderDetails(accessToken:String, orderId:String, lineItems:Int32, ImgHeight: Int32){
        
        ShopifyOrderDetail.shared.orderHistoryList(orderId: orderId, lineItems: lineItems, ImgHeight: ImgHeight) { result, error in
            
            if let lineItems = result?.lineItems.edges.map({$0.node}) {
                
                var variant_name: String = ""
                var variant_value: String = ""
                var orderItems: Int = 0
                
                var cartItems = [String: Any]()
                
                var orderedItems = [Any]()
                lineItems.forEach { items in
                    
                    orderItems += Int(items.quantity)
                    
                    let prodOptions = items.variant?.selectedOptions
                    if prodOptions!.count > 1 {
                        
                        variant_name = "\(prodOptions![0].name.uppercased()) / \(prodOptions![1].name.uppercased())"
                        variant_value = "\(prodOptions![0].value.uppercased()) / \(prodOptions![1].value.uppercased())"
                    }else {
                        
                        variant_name = "\(prodOptions![0].name.uppercased())"
                        variant_value = "\(prodOptions![0].value.uppercased())"
                    }
                    let itemDetails: [String: Any] = [
                        AnalyticsParameterItemName: items.title,
                        AnalyticsParameterItemVariant: variant_value,
                        AnalyticsParameterQuantity : items.quantity,
                        AnalyticsParameterCurrency:items.variant!.priceV2.currencyCode
                        
                    ]
                    orderedItems.append(itemDetails)                    
                }
                
                var discountCode: String?
                
                if self.OrderDetail.cartDiscountAmountSet != nil {
                    if self.OrderDetail.discountCode != nil {
                        discountCode = self.OrderDetail.discountCode!
                    }else{
                        discountCode = "AUTODISCOUNT"
                    }
                }
                
                var totalTax: Float!
                if (self.OrderDetail.totalTaxSet!.presentmentMoney.amount as NSString).floatValue <= 0{
                    totalTax = 0.0
                }else{
                    totalTax = (self.OrderDetail.totalTaxSet!.presentmentMoney.amount as NSString).floatValue
                }
                
                var shippingPrice: Float!
                if (self.OrderDetail.totalShippingPriceSet.presentmentMoney.amount as NSString).floatValue <= 0{
                    shippingPrice = 0.0
                }else{
                    shippingPrice = (self.OrderDetail.totalShippingPriceSet.presentmentMoney.amount as NSString).floatValue
                }
                
                cartItems = [
                    AnalyticsParameterTransactionID: result!.id.rawValue,
                    AnalyticsParameterQuantity: orderItems,
                    AnalyticsParameterCoupon: discountCode as Any,
                    AnalyticsParameterCurrency: self.OrderDetail.totalShippingPriceSet.presentmentMoney.currencyCode,
                    AnalyticsParameterTax: totalTax!,
                    AnalyticsParameterShipping: shippingPrice!,
                    AnalyticsParameterValue: self.OrderDetail.totalPriceSet.presentmentMoney.amount,
                    "order_date": self.OrderDetail.createdAt,
                    "sub_total":self.OrderDetail.subtotalPriceSet?.presentmentMoney.amount,
                    "discount_amount": self.OrderDetail.cartDiscountAmountSet?.presentmentMoney.amount,
                    AnalyticsParameterItems: orderedItems
                ]
                
                FirebaseEventsLog.request.logPurchaseEvent(items: cartItems)
                
            }else{
                self.Showalert1(message: error!)
            }
        }
    }

}
