//
//  SearchVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 17/03/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics

/**This class shows the reasults of user searches
 - Sorting
 - Favourite*/

class SearchVC: UIViewController, CustomButtonAlertVCDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var BadgeView: UIView!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var searchCollection: UICollectionView!
    @IBOutlet weak var searchView: UIControl!
    @IBOutlet weak var heightSeachViewContraint: NSLayoutConstraint!
    
    var searchText: String!
    var Cursor: String?
    var ScreenTitle: String!
    var sort_type: Storefront.ProductSortKeys?
    var reverse: Bool!
    var Cart_DB = CartQueries()
    var products: Storefront.ProductConnection?
    var ImgHeight: Int!
    var storefrontProducts = [Storefront.Product]()
    var StorefrontProductsEdge = [Storefront.ProductEdge]()
    
    //MARK: - Custom alert Action Delegate
    
    func customActionPerformed(itemAction: String) {
        
        if itemAction == "latest".localized {
            
            self.Cursor = nil
            self.reverse = true
            self.sort_type = Storefront.ProductSortKeys.createdAt
            self.storefrontProducts.removeAll()
            self.searchProductsGraphQLQuery(products: 10, query: self.searchText!, cursor: self.Cursor, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.searchCollection.reloadData()
            
        }else if itemAction == "bestsell".localized {
            
            self.Cursor = nil
            self.reverse = false
            self.sort_type = Storefront.ProductSortKeys.bestSelling
            self.storefrontProducts.removeAll()
            self.searchProductsGraphQLQuery(products: 10, query: self.searchText!, cursor: self.Cursor, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.searchCollection.reloadData()
            
        }else if itemAction == "pricelowtohigh".localized {
            
            self.Cursor = nil
            self.reverse = false
            self.sort_type = Storefront.ProductSortKeys.price
            self.storefrontProducts.removeAll()
            self.searchProductsGraphQLQuery(products: 10, query: self.searchText!, cursor: self.Cursor, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.searchCollection.reloadData()
            
        }else if itemAction == "pricehightolow".localized {
            
            self.Cursor = nil
            self.reverse = true
            self.sort_type = Storefront.ProductSortKeys.price
            self.storefrontProducts.removeAll()
            self.searchProductsGraphQLQuery(products: 10, query: self.searchText!, cursor: self.Cursor, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.searchCollection.reloadData()
            
        }else if itemAction == "titleatoz".localized {
            
            self.Cursor = nil
            self.reverse = false
            self.sort_type = Storefront.ProductSortKeys.title
            self.storefrontProducts.removeAll()
            self.searchProductsGraphQLQuery(products: 10, query: self.searchText!, cursor: self.Cursor, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.searchCollection.reloadData()
            
        }else if itemAction == "titleztoa".localized {
            
            self.Cursor = nil
            self.reverse = true
            self.sort_type = Storefront.ProductSortKeys.title
            self.storefrontProducts.removeAll()
            self.searchProductsGraphQLQuery(products: 10, query: self.searchText!, cursor: self.Cursor, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
            self.searchCollection.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.searchCollection.register(UINib(nibName: "GridCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GridCollectionCell")
        self.searchCollection.register(UINib(nibName: "EmptyMessageCell", bundle: nil), forCellWithReuseIdentifier: "EmptyMessageCell")
        self.searchCollection.isHidden = true
        self.heightSeachViewContraint.constant = 0.0
        self.searchView.isHidden = true
        self.lblScreenTitle.text! = "\(String(describing: self.searchText!))"
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWhenAPNSCall(_:)), name: NSNotification.Name(rawValue: "reloadTable"), object: nil)
        
    }
    
    @objc func reloadWhenAPNSCall(_ notification: NSNotification) {
        
        self.searchCollection.reloadData()
        self.view.layoutIfNeeded()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        FirebaseEventsLog.request.trackScreen(screenName: "Search result", className: "SearchVC")
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPad_Grid_Portrait_height
            }else{
                self.ImgHeight = GlobalObjects.iPad_Grid_Square_height
            }
        }else{
            if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                self.ImgHeight = GlobalObjects.iPhone_Grid_Portrait_height
            }else{
                self.ImgHeight = GlobalObjects.iPhone_Grid_Square_height
            }
        }
        
        let cartitems  = Cart_DB.GetAllCartItems()
        var totalQty: Int! = 0
        if cartitems.count != 0 {
            for item in cartitems {
                
                totalQty += item.quantity
            }
            
            self.BadgeView.isHidden = false
            self.BadgeView.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
            self.lblCartCount.text! = String(totalQty)
            self.lblCartCount.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
        }else{
            
            self.BadgeView.isHidden = true
        }
        
        self.storefrontProducts.removeAll()
        self.products = nil
        self.StorefrontProductsEdge.removeAll()
        self.Cursor = nil
        self.sort_type = Storefront.ProductSortKeys.createdAt
        self.reverse = true
        self.searchProductsGraphQLQuery(products: 10, query: self.searchText!, cursor: self.Cursor, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
        self.searchCollection.reloadData()
    }
    
    //Method to fetch search results from store
    func searchProductsGraphQLQuery(products: Int32, query: String, cursor: String?, sort_Type:Storefront.ProductSortKeys?, reverse: Bool?, ImgHeight: Int32) {
        
        self.searchCollection.isHidden = true
        
        ShopifySearchProducts.shared.searchProducts(products: products, query: query, cursor: cursor, sort_Type: sort_Type, reverse: reverse, ImgHeight: ImgHeight) { response, error in
            if let response = response {
                self.products = response
                
                let products = response.edges
                self.StorefrontProductsEdge = products
                
                print(self.StorefrontProductsEdge.count)
                self.StorefrontProductsEdge.forEach { product in
                    self.storefrontProducts.append(product.node)
                    
                }
                if self.storefrontProducts.count > 0 {
                    self.searchView.isHidden = false
                    self.heightSeachViewContraint.constant = 40
                }
                DispatchQueue.main.async {
                    self.searchCollection.isHidden = false
                    self.searchCollection.reloadData()
                }
                
            }else{
                self.Showalert1(message: error!)
            }
        }
    }
    
    
    @IBAction func btnCartTapped(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let detailVC = storyBoard.instantiateViewController(withIdentifier: "CartNavigationController") as? CartNavigationController {
            self.navigationController?.present(detailVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnSortTapped(_ sender: Any) {
        
        if self.storefrontProducts.count != 0 {
            let myAlert = self.storyboard!.instantiateViewController(withIdentifier: "CustomButtonAlertVC") as! CustomButtonAlertVC
            myAlert.delegate = self
            myAlert.Search_Sort = self.sort_type
            myAlert.reverse = self.reverse
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(myAlert, animated: true) {
                myAlert.view.superview?.isUserInteractionEnabled = true
                myAlert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertClose(gesture:))))
            }
        }
    }
    
    @objc func alertClose(gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SearchVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.storefrontProducts.count != 0 {
            return self.storefrontProducts.count
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.storefrontProducts.count != 0 {
            let cell = searchCollection.dequeueReusableCell(withReuseIdentifier:"GridCollectionCell", for: indexPath) as! GridCollectionCell
            cell.lblItemName.text! = self.storefrontProducts[indexPath.row].title //"\(title)"
            
            let price = self.storefrontProducts[indexPath.row].priceRange.minVariantPrice.amount
            let currency = self.storefrontProducts[indexPath.row].priceRange.minVariantPrice.currencyCode.rawValue
            
            let locale = Locale.getSymbolForCurrencyCode(code: currency)
            cell.lblPrice.text! = "\(locale)\(String(format: "%.2f", Double(truncating: price as NSNumber)))"
            
            let images = self.storefrontProducts[indexPath.row].images.edges.map { $0.node}
            images.forEach { image in
                
                let imgURL = image.transformedSrc
                
                cell.ImgProductGrid.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                    // Your code inside completion block
                    if image == nil {
                        
                    }
                })
            }
            
            return cell
        }else {
            let cell = searchCollection.dequeueReusableCell(withReuseIdentifier:"EmptyMessageCell", for: indexPath) as! EmptyMessageCell
            cell.lblMessage.text! = "empty_search_msg".localized
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.storefrontProducts.count != 0 {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let detailVC = storyBoard.instantiateViewController(withIdentifier: "ProductDetailVC") as? ProductDetailVC
            detailVC?.Product_Id = self.storefrontProducts[indexPath.row].id.rawValue
            self.navigationController?.pushViewController(detailVC!, animated: true)
            //        self.present(detailVC!, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.searchCollection.frame.width
        
        if self.storefrontProducts.count != 0 {
            
            if (UIDevice.current.userInterfaceIdiom == .pad){
                if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                    return CGSize(width:((width - 30)/2), height: 565)
                }else{
                    return CGSize(width:((width - 30)/2), height: 455)
                }
            }else{
                if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                    return CGSize(width: ((width - 15)/2), height: 365)
                }else{
                    return CGSize(width: ((width - 15)/2), height: 255)
                }
            }
        }else{
            return CGSize(width: width , height: self.searchCollection.frame.height)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if (UIDevice.current.userInterfaceIdiom == .pad){
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }else{
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.storefrontProducts.count != 0 {
            self.Cursor = self.StorefrontProductsEdge.last?.cursor
            let hasNextPage =  self.products!.pageInfo.hasNextPage
            
            if hasNextPage && indexPath.row == self.storefrontProducts.count - 1 {
                self.searchProductsGraphQLQuery(products: 10, query: self.searchText!, cursor: self.Cursor, sort_Type: self.sort_type, reverse: self.reverse, ImgHeight: Int32(self.ImgHeight))
                //            self.ProductCollection.reloadData()
            }
        }
    }
}

