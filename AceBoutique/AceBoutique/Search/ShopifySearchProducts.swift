//
//  ShopifySearchProducts.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 10/06/21.
//

import Foundation
import MobileBuySDK

//Class to provides the results fetched from the store

class ShopifySearchProducts {
    
    static let shared = ShopifySearchProducts()
    
    func searchProducts(products: Int32, query: String, cursor: String?, sort_Type:Storefront.ProductSortKeys?, reverse: Bool?, ImgHeight: Int32, completion: @escaping (Storefront.ProductConnection?, String?) -> Void) {
        QuerySearchProducts.shared.searchShopifyProducts(products: products, query: query, cursor: cursor, sort_Type: sort_Type, reverse: reverse, ImgHeight: ImgHeight) { product, error in
            if let product = product{
                
                completion(product,nil)
            }else{
                let error = "errorMsg".localized
                completion(nil, error)
            }
        }
    }
}
