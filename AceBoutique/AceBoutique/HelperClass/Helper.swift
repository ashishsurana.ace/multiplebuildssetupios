//
//  Helper.swift
//  eCentraDocs
//
//  Created by Aashiyana Shaikh on 17/01/19.
//  Copyright © 2019 Aashiyana Shaikh. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import SystemConfiguration
import MobileBuySDK
import CryptoSwift


struct GlobalObjects {
    
    
    static var objConfigResponse: ConfigResponse!
    static var arrMenuItems = [Item]()
    static var arrShowMenuItems = [Item]()
    
    //    static var jsonString: String = String()
    
    static var isUpdateAvailable = false
    
    static let iPhone_Grid_Square_height = 400
    static let iPhone_Grid_Portrait_height = 600
    
    
    static let iPad_Grid_Square_height = 760
    static let iPad_Grid_Portrait_height = 1024
    
    /*  1. DISABLED
     Customers will only be able to check out as guests.
     
     2. OPTIONAL
     Customers will be able to check out with a customer account or as a guest.
     
     3. REQUIRED
     Customers will only be able to check out if they have a customer account.
     */
    
    static var storeFrontCollectionLists = NSMutableArray() // collectionlistVC
    
    static var CustomerAddresses = [CustomerAddress]()
    static var arrLocales = [shopLocales]()
    static var OrderLists = [OrderHistory]()
    static var tokenExpiresAt: Date!
    static let currentDate = Date()
    
}


//MARK:- Alert Messages

struct alertDefault {
    
    static let webServiceFail:String = "Please try again later."
    static let connectionFail:String = "Please enable your internet connection."
}

//MARK: - AppDelagate Object
var appDelegateShared = UIApplication.shared.delegate as! AppDelegate

//MARK: - Show Alert

func showAlert(msg:String) {
    let alert = UIAlertController(title: nil, message: msg, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
    appDelegateShared.window?.rootViewController?.present(alert, animated: true, completion: nil)
}

func showAlertWithDelegate(_ title:String, msg:String, button1:String, withBlocks block: @escaping (_ status: Int) -> Void)
{
    let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: button1, style: .default, handler: { (Finish) in
        block(1)
    }))
    appDelegateShared.window?.rootViewController?.present(alert, animated: true, completion: nil)
}

func updateAlertWithDelegate(_ title:String, msg:String, button1: String, button2:String, withBlocks block: @escaping (_ status: Int) -> Void)
{
    let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: button1, style: .default, handler: { (Finish) in
        block(1)
    }))
    alert.addAction(UIAlertAction(title: button2, style: .default, handler: { (Finish) in
        block(2)
    }))
    appDelegateShared.window?.rootViewController?.present(alert, animated: true, completion: nil)
}

// MARK: - AES Encryption Decryption -

func encryptMessage(message: String, encryptionKey: String, iv: String) -> String? {
       if let aes = try? AES(key: encryptionKey, iv: iv),
           let encrypted = try? aes.encrypt(Array<UInt8>(message.utf8)) {
           return encrypted.toBase64()
       }
       return nil
   }

// Dycript any string
func decryptMessage(encryptedMessage: String, encryptionKey: String, iv: String) -> String? {
   if let aes = try? AES(key: encryptionKey, iv: iv),
          //to decrypt use (hex: if msg is in hexString and base64: for Base64 String)
           let decrypted = try? aes.decrypt(Array<UInt8>(base64: encryptedMessage)) {
           return String(data: Data(bytes: decrypted), encoding: .utf8)
       }
       return nil
   }

