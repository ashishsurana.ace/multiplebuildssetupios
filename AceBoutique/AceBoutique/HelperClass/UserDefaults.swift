//
//  UserDefaults.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 06/06/21.
//

import Foundation
import UIKit

//MARK: - UserDefault Structure
struct MyUserDefaults {
    
    static let TokenExpire = "accessTokenExpiry"
    static let CustomerToken = "customerAccessToken"
    static let UserEmail = "UserEmail"
    static let CheckoutTime = "CheckoutTime"
    static let firebaseToken = "FCMToken"
    static let LANGUAGE = "AppLanguage"
    static let LOCALE = "AppLocale"
    static let AppStoreUrl = "AppStoreURL"
    static let PlayStoreURL = "PlayStoreURL"
    static let NotificationPayload = "NotificationPayload"
    static let LoginUser = "LoginUser"
    static let ConfigPathURL = "configPathURL"
}

//MARK: - Get/Set UserDefaults
func setMyUserDefaults(value:Any, key:String){
    UserDefaults.standard.set(value, forKey: key)
    UserDefaults.standard.synchronize()
}

func getMyUserDefaults(key:String)->Any {
    return UserDefaults.standard.value(forKey: key) ?? ""
}

func clearMyUserDefaluts(){
    let appDomain = Bundle.main.bundleIdentifier!
    UserDefaults.standard.removePersistentDomain(forName: appDomain)
    UserDefaults.standard.synchronize()
}

func removeMyUserDefaults(key:String){
    UserDefaults.standard.removeObject(forKey: key)
    UserDefaults.standard.synchronize()
}

func GetCurrentLanguage() -> String {
    var language:String? = nil
    if let key = UserDefaults.standard.object(forKey: MyUserDefaults.LOCALE) as? String {
        language = key
    }
    
    if language != nil {
        return UserDefaults.standard.value(forKey: MyUserDefaults.LOCALE) as! String
    }else{
        return Locale.current.languageCode!
    }
}

func SetCurrentLanguage(_ language: String) {
    UserDefaults.standard.set(language, forKey: MyUserDefaults.LOCALE)
    UserDefaults.standard.synchronize()
}
