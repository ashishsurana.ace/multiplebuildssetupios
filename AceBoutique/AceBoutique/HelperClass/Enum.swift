//
//  ClientHelper.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 07/06/21.
//

import Foundation
import UIKit

/**Apps all Messages, string variables are defined here */
enum AppLayout: String {
    case portrait = "Portrait";
    case square = "Square";
}

enum UpdateOption: String {
    
    case compulsory = "Compulsory";
    case optional = "Optional";
}

enum HomeLayoutType: String {
    
    case collectionSlider = "COLLECTION_SLIDER";
    case rotatingImageSlider = "ROTATING_IMAGE_SLIDER";
    case productSlider = "PRODUCT_SLIDER";
    case announcementBar = "ANNOUNCEMENT_BAR";
    case productGrid = "PRODUCT_GRID";
    case imageBanner = "IMAGE_BANNER";
}

enum AppPlans: Int {
    
    case basic =  1; //"BASIC";
    case premium = 2; //"PREMIUM";
    case enterprise = 3; //"ENTERPRISE";
}

enum MenuItems: String {
    
    case home = "Home";
    case collection = "Collections";
    case cart = "Cart";
    case wishlist = "Wishlist";
    case orders = "Orders";
    case language = "Language";
    case logout = "Logout";
    case share = "Share";
    case login = "Login";
    case exitPreview = "Exit from Preview";
}


enum FirebaseSubscription: String {
    
    case general = "general";
    case cart = "cart";
}

enum customerAccountStatus: String {
    
    case optional = "OPTIONAL";
    case disabled = "DISABLED";
    case required = "REQUIRED";
}

enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}

enum NotificationActions: String {
    case general = "general";
    case abandoned_checkout = "abandoned_checkout";
    case abandoned_cart = "abandoned_cart";
    case open_collection = "open_collection";
    case open_product = "open_product";
}
