//
//  AppThemeHelper.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 07/06/21.
//

import Foundation
import UIKit

/**Manages the App layout theme like color and fonts.*/
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    convenience init?(hexaARGB: String) {
        
        var chars = Array(hexaARGB.hasPrefix("#") ? hexaARGB.dropFirst() : hexaARGB[...])
        switch chars.count {
        case 3: chars = chars.flatMap { [$0, $0] }; fallthrough
        case 6: chars.append(contentsOf: ["F","F"])
        case 8: break
        default: return nil
        }
        self.init(red: .init(strtoul(String(chars[2...3]), nil, 16)) / 255,
                  green: .init(strtoul(String(chars[4...5]), nil, 16)) / 255,
                  blue: .init(strtoul(String(chars[6...7]), nil, 16)) / 255,
                  alpha: .init(strtoul(String(chars[0...1]), nil, 16)) / 255)
    }
}

struct AppFontName {
    
    static let regular = "Poppins-Regular"
    static let bold = "Poppins-Bold"
    static let semiBold = "Poppins-SemiBold"
    static let medium = "Poppins-Medium"
    static let italic = "Poppins-Italic"
    static let mediumItalic = "Poppins-MediumItalic"
    
}

struct AppFontSize {
    
    static let size12: CGFloat = 12.0
    static let size13: CGFloat = 13.0
    static let size14: CGFloat = 14.0
    static let size15: CGFloat = 15.0
    static let size16: CGFloat = 16.0
    static let size17: CGFloat = 17.0
    static let size18: CGFloat = 18.0
    static let size19: CGFloat = 19.0
    static let size20: CGFloat = 20.0
    static let size21: CGFloat = 21.0
    static let size22: CGFloat = 22.0
}

//MARK: - UIFont Extension

extension UIFont {
    class func appRegularFontWith( size:CGFloat ) -> UIFont{
        return  UIFont(name: AppFontName.regular, size: size)!
    }
    class func appBoldFontWith( size:CGFloat ) -> UIFont{
        return  UIFont(name: AppFontName.bold, size: size)!
    }
    class func appSemiBoldFontWith( size:CGFloat ) -> UIFont{
        return  UIFont(name: AppFontName.semiBold, size: size)!
    }
    class func appMediumFontWith( size:CGFloat ) -> UIFont{
        return  UIFont(name: AppFontName.medium, size: size)!
    }
    class func appItalicFontWith( size:CGFloat ) -> UIFont{
        return  UIFont(name: AppFontName.italic, size: size)!
    }
    class func appMediumItalicFontWith( size:CGFloat ) -> UIFont{
        return  UIFont(name: AppFontName.medium, size: size)!
    }
}

extension UIColor {
    
    /// Return the better contrasting color, white or black
    
    func contrastColor(color: UIColor) -> UIColor {
        
        var d = CGFloat(0)
        var r = CGFloat(0)
        var g = CGFloat(0)
        var b = CGFloat(0)
        var a = CGFloat(0)
        
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        // Counting the perceptive luminance - human eye favors green color...
        let luminance = 1 - ((0.299 * r) + (0.587 * g) + (0.114 * b))
        
        if luminance < 0.5 {
            d = CGFloat(0) // bright colors - black font
        } else {
            d = CGFloat(1) // dark colors - white font
        }
        
        return UIColor( red: d, green: d, blue: d, alpha: a)
    }
}

