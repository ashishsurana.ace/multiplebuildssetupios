//
//  Localizable.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 08/07/21.
//

import Foundation
import UIKit


public protocol Localizable {
    var localized: String {get}
}

extension String: Localizable{
    
    public var localized: String {
        let loc = GetCurrentLanguage()
        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
        let bundle = Bundle(path: path!)!
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
    }
}

public protocol XIBLocalizable {
    
    var xibLocKey: String? {get set}
}

extension UILabel: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil}
        set(key) {
            text = key?.localized
        }
    }
}

extension UITextField: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get {return nil}
        set(key) {
            text = key?.localized
        }
    }
    
    @IBInspectable var xibPlaceholder: String? {
        get { return nil }
        set(key) {
            placeholder = key?.localized
        }
    }
}

extension UIButton: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localized, for: .normal)
        }
    }
}
