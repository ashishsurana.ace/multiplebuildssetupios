//
//  StringHelper.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 08/06/21.
//

import Foundation
import UIKit


/**This helper class provides methods for strings formattings.*/
//MARK: - String Extension
extension String {
    //Get string length
    var length: Int {
        return self.count
    }
    
    //Remove white space in string
    func removeWhiteSpace() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}


//MARK:- String Trim func
extension String {
    
    public func TrimString(_ mystring: String) -> String {
        
        return mystring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
}

extension String {
    static func format(strings: [String],
                       boldFont: UIFont = UIFont(name:"Montserrat-Regular", size:14)!,
                       boldColor: UIColor = UIColor.blue,
                       inString string: String,
                       font: UIFont = UIFont(name:"Montserrat-Regular", size:14)!,
                       color: UIColor = UIColor.black) -> NSAttributedString {
        
        let attributedString =
            NSMutableAttributedString(string: string,
                                      attributes: [
                                        NSAttributedString.Key.font: font,
                                        NSAttributedString.Key.foregroundColor: color])
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont, NSAttributedString.Key.foregroundColor: boldColor]
        //        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        for bold in strings {
            attributedString.addAttributes(boldFontAttribute, range: (string as NSString).range(of: bold))
            //             attributedString.addAttributes(underlineAttribute, range: (string as NSString).range(of: bold))
        }
        return attributedString
    }
}

