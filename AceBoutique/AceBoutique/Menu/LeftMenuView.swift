//
//  LeftMenuView.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 05/03/21.
//

import Foundation
import UIKit

/**Custom UI class to present table menu in the app.*/
final class LeftMenuView: UITableView {
    
    // MARK: - Properties
    
    var didTouchIndex: ((_ index: Int, _ title: String) -> Void)?
    
    // MARK: - Life Cycle
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.delegate = self
        self.dataSource = self
        self.isOpaque = true
        self.backgroundColor = .clear
        self.alpha = 1.0
        //        self.backgroundView = nil
        self.separatorStyle = .none
        self.separatorColor = .clear
        self.bounces = false
        self.tableFooterView = UIView()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateContentInset()
    }
    
    
    // MARK: - Private Methods
    
    private func updateContentInset() {
        let tableViewHeight = self.bounds.height
        let contentHeight = self.contentSize.height
        
        let centeringInset = (tableViewHeight - contentHeight) / 2.0
        let topInset = max(centeringInset, 0.0)
        
        self.contentInset = UIEdgeInsets(top: topInset, left: 0.0, bottom: 0.0, right: 0.0)
    }
}

extension LeftMenuView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var title  = String()
        if  indexPath.row  > (GlobalObjects.arrShowMenuItems.count-1) {
            if UserDefaults.standard.object(forKey: MyUserDefaults.CustomerToken) != nil {
                title = MenuItems.logout.rawValue
            }else{
                title = MenuItems.login.rawValue
            }
        }else{
            title = GlobalObjects.arrShowMenuItems[indexPath.row].title!
        }
        
        didTouchIndex?(indexPath.row, title)
    }
}

extension LeftMenuView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        
        return GlobalObjects.arrShowMenuItems.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier: String = "Cell"
        
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil {
            
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
            cell?.backgroundColor = .clear
            
            cell?.textLabel?.font = UIFont.appRegularFontWith(size: AppFontSize.size17)
            cell?.textLabel?.textColor = .black
            cell?.textLabel?.highlightedTextColor = .lightGray
            cell?.selectedBackgroundView = UIView()
        }
        
        let localizedMenu =  (GlobalObjects.arrShowMenuItems[indexPath.row].title!).localized
        cell?.textLabel!.text = "   \(localizedMenu)"
        return cell ?? UITableViewCell()
    }
}
