//
//  RootViewController.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 05/03/21.

import AKSideMenu
import Foundation
import UIKit
import MobileBuySDK

/** Custom class  for side menu it initialise very first when app is launching.*/
final class RootViewController: AKSideMenu, AKSideMenuDelegate {
    
    
    var shortcutPath:String = ""
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.menuPreferredStatusBarStyle = .lightContent
        self.contentViewShadowColor = .black
        self.contentViewShadowOffset = CGSize(width: 0, height: 0)
        self.contentViewShadowOpacity = 0.6
        self.contentViewShadowRadius = 12
        self.contentViewShadowEnabled = true
        self.delegate = self
        
        print("shortcut path",self.shortcutPath)
        
        if UserDefaults.standard.object(forKey: MyUserDefaults.CustomerToken) != nil {
            let accesstoken = UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken)
            self.getCustomerDetails(accessToken: accesstoken!)
        }
        
        self.callAPIOfShopLocale()
        self.setContentView()
        
    }
    
    func setContentView(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        if isFromNotification == false {
            if UserDefaults.standard.object(forKey: "shortcutItem") != nil {
                
                if UserDefaults.standard.object(forKey: MyUserDefaults.CustomerToken) != nil {
                    let objShortcut = getMyUserDefaults(key: "shortcutItem") as! String
                    
                    if objShortcut == "com.shopifyapp.Orders" {
                        
                        removeMyUserDefaults(key: "shortcutItem")
                        self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "OrderNavVC")
                        
                    }else if objShortcut == "com.shopifyapp.Favourite" {
                        
                        removeMyUserDefaults(key: "shortcutItem")
                        self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "FavMenuNavVC")
                        
                    }else if objShortcut == "com.shopifyapp.Cart" {
                        
                        removeMyUserDefaults(key: "shortcutItem")
                        self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "MenuMyCartNav")
                    }
                }else{
                    
                    removeMyUserDefaults(key: "shortcutItem")
                    self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "MenuLoginNavVC")
                }
            }else{
                
                self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "contentViewController")
            }
        }else{
            
            if UserDefaults.standard.object(forKey: MyUserDefaults.NotificationPayload) != nil {
                let userInfo = getMyUserDefaults(key: MyUserDefaults.NotificationPayload) as! [AnyHashable: Any]

                guard let actionType = userInfo["action"] as? String else {
                    return
                }
                
                if actionType.lowercased() == NotificationActions.general.rawValue.lowercased() {
                    self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "contentViewController")
                    
                }else if actionType.lowercased() == NotificationActions.abandoned_checkout.rawValue.lowercased() {
                    self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "WebViewCheckoutVC")
                    
                }else if actionType.lowercased() == NotificationActions.abandoned_cart.rawValue.lowercased() {
                    self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "MenuMyCartNav")
                    
                }else if actionType.lowercased() == NotificationActions.open_collection.rawValue.lowercased() {
                    self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "ProductListVC")
                    
                }else if actionType.lowercased() == NotificationActions.open_product.rawValue.lowercased() {
                    self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "contentViewController")
                    
                }else{
                    self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "contentViewController")
                }
                
            }else{
                
                self.contentViewController = storyBoard.instantiateViewController(withIdentifier: "contentViewController")
            }
        }
        self.leftMenuViewController = storyBoard.instantiateViewController(withIdentifier: "LeftMenuViewController")
    }
    

    func callAPIOfShopLocale(){
        
//        if CheckNetwork.isConnectedToNetwork() == true {
        localeId = GetCurrentLanguage()
        Network.request.apollo.fetch(query: ShopDetailsQuery()){
            result in
            switch result {
            case .success(let graphQLResult):
                
                guard (try? result.get().data) != nil else { return }
                
                if graphQLResult.data != nil {
                    
//                    print(graphQLResult.data)
                    let arrLocales = graphQLResult.data?.shopLocales
//                    print(arrLocales)
                    GlobalObjects.arrLocales.removeAll()
                    
                    arrLocales!.forEach { items in
                        
                        let published = items.published
                        
                        if published == true {
                            
                            GlobalObjects.arrLocales.append(shopLocales(locale: items.locale, name: items.name, primary: items.primary, published: items.published))
                        }
                        
                        if let language = getMyUserDefaults(key: MyUserDefaults.LANGUAGE) as? String, language != "" {
                            
                            let locale = GetCurrentLanguage()
                            appLocale = Locale(identifier: locale)
                            if items.primary == true && locale == items.locale {
                                isPrimaryLanguageSelected = true
                            }
                        }else{
                            
                            if items.primary == true {
                                SetCurrentLanguage(items.locale)
                                isPrimaryLanguageSelected = true
                                appLocale = Locale(identifier: items.locale)
                            }
                        }
                    }
                    client = Graph.Client(shopDomain: shopDomain, apiKey: storeAPIKey, locale: appLocale)
                    if graphQLResult.data?.shop != nil {
                        CHECKOUT_CUSTOMER_ACCOUNTS_STATUS = graphQLResult.data!.shop.customerAccounts.rawValue
                    }
                }
                
            case .failure(let error):
                print("RootVC Error loading data \(error.localizedDescription)")
                DispatchQueue.main.async {
                self.Showalert1(message: error.localizedDescription)
                }
            }
        }
//        }else{
//            self.Showalert1(message: "no_internet_msg".localized)
//        }
    }
    
    
    func getCustomerDetails(accessToken: String){
        let query = Storefront.buildQuery { $0
            .customer(customerAccessToken: accessToken) { $0
                .id()
                .firstName()
                .lastName()
                .email()
            }
        }
        
        let task = client.queryGraphWith(query) { response, error in
            print(response as Any)
            
            if let userId = response?.customer?.id.rawValue {
                FirebaseEventsLog.request.setUserId(userId: userId)
            }
            
        }
        
        task.resume()
    }
    
    // MARK: - <AKSideMenuDelegate>
    
    public func sideMenu(_ sideMenu: AKSideMenu, willShowMenuViewController menuViewController: UIViewController) {
        debugPrint("willShowMenuViewController")
        
    }
    
    public func sideMenu(_ sideMenu: AKSideMenu, didShowMenuViewController menuViewController: UIViewController) {
        debugPrint("didShowMenuViewController")
        
    }
    
    public func sideMenu(_ sideMenu: AKSideMenu, willHideMenuViewController menuViewController: UIViewController) {
        debugPrint("willHideMenuViewController")
    }
    
    public func sideMenu(_ sideMenu: AKSideMenu, didHideMenuViewController menuViewController: UIViewController) {
        debugPrint("didHideMenuViewController")
    }
}


