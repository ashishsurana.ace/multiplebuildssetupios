//
//  LeftMenuViewController.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 05/03/21.
//

import AKSideMenu
import UIKit
import MobileBuySDK
import Firebase

/**This class holds the menu table, background image of menu and actions of menu items.*/

final class LeftMenuViewController: UIViewController, CustomLanguagePickerVCDelegate {
    
    
    
    //MARK: - Delegate of LanguagePicker -
    
    func changeSelectedLanguage(locale: shopLocales) {
        SetCurrentLanguage(locale.locale)
        appLocale = Locale(identifier: locale.locale)
        client = Graph.Client(shopDomain: shopDomain, apiKey: storeAPIKey, locale: appLocale)
        if locale.primary == true {
            isPrimaryLanguageSelected = true
        }else{
            isPrimaryLanguageSelected = false
        }
        
        self.callAPIOfShopLocale { isCompleted in
            
            let baseVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardView")
            let contentViewController = UINavigationController(rootViewController: baseVC!)
            self.sideMenuViewController?.setContentViewController(contentViewController, animated: true)
            self.sideMenuViewController?.hideMenuViewController()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAPICall"), object: nil)
            
//            if isCompleted == true {
//                let baseVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardView")
//                let contentViewController = UINavigationController(rootViewController: baseVC!)
//                self.sideMenuViewController?.setContentViewController(contentViewController, animated: true)
//                self.sideMenuViewController?.hideMenuViewController()
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAPICall"), object: nil)
//            }else{
//
//            }
        }
    }
    
    func callAPIOfShopLocale(completion: @escaping (Bool) -> Void){
        localeId = GetCurrentLanguage()
        appDelegateShared.showHud()
    
        Network.request.apollo.fetch(query: ShopDetailsQuery(), cachePolicy: .fetchIgnoringCacheCompletely){
            result in
            switch result {
            case .success(let graphQLResult):
                appDelegateShared.dismissHud()
                guard (try? result.get().data) != nil else { return }
                
                if graphQLResult.data != nil {
                    
                    let arrLocales = graphQLResult.data?.shopLocales
                    print(arrLocales)
                    GlobalObjects.arrLocales.removeAll()
                    
                    arrLocales!.forEach { items in
                        
                        let published = items.published
                        
                        if published == true{
                            
                            GlobalObjects.arrLocales.append(shopLocales(locale: items.locale, name: items.name, primary: items.primary, published: items.published))
                        }
                        
                        
                        if let language = getMyUserDefaults(key: MyUserDefaults.LANGUAGE) as? String, language != "" {

                            let locale = GetCurrentLanguage()
                            appLocale = Locale(identifier: locale)
                            if items.primary == true && locale == items.locale{
                                isPrimaryLanguageSelected = true
                            }
                        }else{
                            if items.primary == true {
                                SetCurrentLanguage(items.locale)
                                isPrimaryLanguageSelected = true
                                appLocale = Locale(identifier: items.locale)

                            }
                        }
                    }
                    client = Graph.Client(shopDomain: shopDomain, apiKey: storeAPIKey, locale: appLocale)
                    if graphQLResult.data?.shop != nil {
                        CHECKOUT_CUSTOMER_ACCOUNTS_STATUS = graphQLResult.data!.shop.customerAccounts.rawValue
                    }
                    completion(true)
                }
                
            case .failure(let error):
                appDelegateShared.dismissHud()
                print("LeftMenuVC Error loading data \(error.localizedDescription)")
                completion(false)
            }
        }
    }
    
    // MARK: - Properties
    
    let leftMenuView = LeftMenuView()
    var ImgView = UIImageView()
    var Cart_DB = CartQueries()
    // MARK: - Life Cycle
    
    override func loadView() {
        view = leftMenuView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        if GlobalObjects.objConfigResponse != nil {
           
            self.leftMenuView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "backgroundSplash"))
            self.leftMenuView.backgroundView?.contentMode = .scaleAspectFill
            self.leftMenuView.backgroundView?.alpha = 0.2
            self.leftMenuView.backgroundView?.clipsToBounds = true
            self.leftMenuView.backgroundView?.isOpaque = true
        
        GlobalObjects.arrShowMenuItems.removeAll()
        
        if UserDefaults.standard.object(forKey: MyUserDefaults.CustomerToken) != nil {
            // menu items + logout
            
            GlobalObjects.arrShowMenuItems.append(contentsOf: GlobalObjects.arrMenuItems)
            if AppPlans.enterprise.rawValue == GlobalObjects.objConfigResponse.globalSettings!.planID && GlobalObjects.arrLocales.count > 1 {
                GlobalObjects.arrShowMenuItems.append(Item(title: MenuItems.language.rawValue))
            }
            GlobalObjects.arrShowMenuItems.append(Item(title: MenuItems.logout.rawValue))
            
        }else{
            
            for item in GlobalObjects.arrMenuItems {
                
                if item.title != MenuItems.wishlist.rawValue && item.title != MenuItems.orders.rawValue {
                    
                    GlobalObjects.arrShowMenuItems.append(Item(title: item.title))
                }
            }
            
            if AppPlans.enterprise.rawValue == GlobalObjects.objConfigResponse.globalSettings!.planID && GlobalObjects.arrLocales.count > 1 {
                GlobalObjects.arrShowMenuItems.append(Item(title: MenuItems.language.rawValue))
            }
            GlobalObjects.arrShowMenuItems.append(Item(title: MenuItems.login.rawValue))
        }
        }
        leftMenuView.reloadData()
        leftMenuView.didTouchIndex = { [weak self] index,title in
            self?.handleTouch(at: index, Title: title)
        }
        
    }
    
    
    // MARK: - Actions
    
    private func handleTouch(at index: Int, Title: String) {
        print("Menu Item:",Title)
        switch Title {
        case MenuItems.home.rawValue:
            
            if let baseVC = storyboard?.instantiateViewController(withIdentifier: "DashboardView") {
                let contentViewController = UINavigationController(rootViewController: baseVC)
                sideMenuViewController?.setContentViewController(contentViewController, animated: true)
                sideMenuViewController?.hideMenuViewController()
            }
            
        case MenuItems.collection.rawValue:
            
            if let baseVC = storyboard?.instantiateViewController(withIdentifier: "CollectionsListingVC") {
                let contentViewController = UINavigationController(rootViewController: baseVC)
                sideMenuViewController?.setContentViewController(contentViewController, animated: true)
                sideMenuViewController?.hideMenuViewController()
            }
            
        case MenuItems.cart.rawValue:
            
            if let baseVC = storyboard?.instantiateViewController(withIdentifier: "CartVC") {
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MenuCartSetup"), object: nil)
                let contentViewController = UINavigationController(rootViewController: baseVC)
                sideMenuViewController?.setContentViewController(contentViewController, animated: true)
                sideMenuViewController?.hideMenuViewController()
            }
            
        case MenuItems.wishlist.rawValue:
            
            if let baseVC = storyboard?.instantiateViewController(withIdentifier: "FavouritesVC") {
                let contentViewController = UINavigationController(rootViewController: baseVC)
                sideMenuViewController?.setContentViewController(contentViewController, animated: true)
                sideMenuViewController?.hideMenuViewController()
            }
            
        case MenuItems.orders.rawValue:
            
            if let baseVC = storyboard?.instantiateViewController(withIdentifier: "OrderHistoryListingVC") {
                let contentViewController = UINavigationController(rootViewController: baseVC)
                sideMenuViewController?.setContentViewController(contentViewController, animated: true)
                sideMenuViewController?.hideMenuViewController()
            }
            
        case MenuItems.share.rawValue:
            
            self.showShareAppDialogue()
            
        case MenuItems.language.rawValue:
            
            let myAlert = self.storyboard!.instantiateViewController(withIdentifier: "CustomLanguagePickerVC") as! CustomLanguagePickerVC
            myAlert.delegate = self
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(myAlert, animated: true, completion: nil)
            
        case MenuItems.logout.rawValue:
            
            let alert = UIAlertController(title: "", message: "logoutConfirm".localized, preferredStyle:.alert)
            
            alert.addAction(UIAlertAction(title: "btncancel".localized, style: .default, handler: { (alert) in
            }))
            
            alert.addAction(UIAlertAction(title: "btnOk".localized, style: .default, handler: { (alert) in
                if UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken) != nil {
                    guard let accessToken = UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken) else {
                        return
                    }
                                        
                    ClientMutations.shared.logout(accessToken: accessToken) { (success, error) in
                        if success {
                            
                            let issucess = self.Cart_DB.deleteAllCartItem()
                            if  issucess == true {
                                
                                Messaging.messaging().unsubscribe(fromTopic: "cart")
                                removeMyUserDefaults(key: MyUserDefaults.CustomerToken)
                                removeMyUserDefaults(key: MyUserDefaults.UserEmail)
                                let baseVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardView")
                                let contentViewController = UINavigationController(rootViewController: baseVC!)
                                self.sideMenuViewController?.setContentViewController(contentViewController, animated: true)
                                self.sideMenuViewController?.hideMenuViewController()
                            }
                            
                        }else{
                            
                            self.Showalert1(message: error)
                        }
                    }
                }
            }))
            
            DispatchQueue.main.async {
                
                self.present(alert, animated: true, completion: nil)
            }
            
             
        case MenuItems.login.rawValue:
            
            if CHECKOUT_CUSTOMER_ACCOUNTS_STATUS.lowercased() == customerAccountStatus.disabled.rawValue.lowercased() {
                
                let alert = UIAlertController(title: "", message: "msgAccountDisabled".localized, preferredStyle:.alert)
                
                alert.addAction(UIAlertAction(title: "btncancel".localized, style: .default, handler: { (alert) in
                }))
                
                alert.addAction(UIAlertAction(title: "btnOk".localized, style: .default, handler: { (alert) in
                }))
                
                DispatchQueue.main.async {
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }else{

                if let baseVC = storyboard?.instantiateViewController(withIdentifier: "MenuLoginRegistration") {
                    let contentViewController = UINavigationController(rootViewController: baseVC)
                    sideMenuViewController?.setContentViewController(contentViewController, animated: true)
                    sideMenuViewController?.hideMenuViewController()
                }
            }
            
        default:
            break
        }
    }
    
    
    func showShareAppDialogue(){
        
        let app_store_url = getMyUserDefaults(key: MyUserDefaults.AppStoreUrl) as! String
        let play_store_url = getMyUserDefaults(key: MyUserDefaults.PlayStoreURL) as! String
        
        let shareText = "Download Stashing Fashion \n\niOS: \n\(app_store_url)\n\nAndroid: \n\(play_store_url)"
        
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        //            present(vc, animated: true, completion: nil)
        
        if let popoverController = vc.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func alertClose(gesture: UITapGestureRecognizer) {
        //        self.dismiss(animated: true, completion: nil)
    }
}

