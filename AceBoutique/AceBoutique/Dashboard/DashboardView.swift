//
//  DashboardView.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 08/02/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics


/** Dashboard is the home screen which displays dynamic layout content from the config.json */

class DefaultCell: UITableViewCell {
    @IBOutlet weak var lblDefaultMsg: UILabel!
}

class DashboardView: UIViewController, CollectionProductSliderDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var BadgeView: UIView!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var ImgBrandLogo: UIImageView!
    @IBOutlet weak var tblHomeLists: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var Cart_DB = CartQueries()
    var storeFrontCollections = NSMutableArray()
    var arrHomeSettingOrder = [HomeScreenSetting]()
    var ImgProductGridURLs = [URL]()
    var ImgProductsSlider = [URL]()
    var GridProducts = NSMutableArray()
    var SliderProducts = NSMutableArray()
    var productSliderTitle:String! = ""
    var productGridTitle:String! = ""
    var objArrImageSlider = [RotatingImage]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tblHomeLists.keyboardDismissMode = .onDrag
        self.tblHomeLists.tableFooterView = UIView()
        self.tblHomeLists.estimatedRowHeight = 50
        self.tblHomeLists.rowHeight = UITableView.automaticDimension
        self.tblHomeLists.isHidden = true
        self.tblHomeLists.register(UINib(nibName: "rotatingImageSliderCell", bundle: nil), forCellReuseIdentifier: "rotatingImageSliderCell")
        self.tblHomeLists.register(UINib(nibName: "CollectionTableCell", bundle: nil), forCellReuseIdentifier: "CollectionTableCell")
        self.tblHomeLists.register(UINib(nibName: "AnnouncementBarCell", bundle: nil), forCellReuseIdentifier: "AnnouncementBarCell")
        self.tblHomeLists.register(UINib(nibName: "GridLayoutTableCell", bundle: nil), forCellReuseIdentifier: "GridLayoutTableCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToProductList(notification:)), name: NSNotification.Name(rawValue: "showProductLists"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToProductDetail(notification:)), name: NSNotification.Name(rawValue: "showProductDetail"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWhenAPNSCall(_:)), name: NSNotification.Name(rawValue: "reloadTable"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateLayout(_:)), name: NSNotification.Name(rawValue: "reloadAPICall"), object: nil)
        
        self.callAPIOfAppLayout()
        let marks = 40
        let rawStr = #"""
            The answer to life,
            the "universe",
            and everything is \#(marks).
            """#
        print("new raw string:",rawStr)
    }
    
    
    @objc func updateLayout(_ notification: NSNotification){
        self.callAPIOfAppLayout()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Logs the number of screen views by the user in firebase
        FirebaseEventsLog.request.trackScreen(screenName: "Home", className: "DashboardView")
        self.searchBar.placeholder = "searchProducts".localized
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        self.searchBar.text = ""
        let cartitems  = Cart_DB.GetAllCartItems()
        var totalQty: Int! = 0
        if cartitems.count != 0 {
            for item in cartitems {
                
                totalQty += item.quantity
            }
            
            self.BadgeView.isHidden = false
            self.BadgeView.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
            self.lblCartCount.text! = String(totalQty)
            self.lblCartCount.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
            
        }else{
            
            self.BadgeView.isHidden = true
            
        }
        
        if GlobalObjects.objConfigResponse != nil {
            self.ImgBrandLogo.sd_setImage(with: URL(string: (GlobalObjects.objConfigResponse.globalSettings?.brandLogoURL!)!), placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                // Your code inside completion block
                if image == nil {
                    
                }
            })
        }
        
        self.tblHomeLists.reloadData()
    }
    
    
    //Function to read the settings and set it up
    func callAPIOfAppLayout(){
        
        if CheckNetwork.isConnectedToNetwork() == true {
            
            if GlobalObjects.objConfigResponse != nil {
                
                GlobalObjects.arrMenuItems = (GlobalObjects.objConfigResponse.menu!.items!)
                
                let appStoreUrl = GlobalObjects.objConfigResponse.globalSettings?.appStoreURL!
                let playStoreUrl = GlobalObjects.objConfigResponse.globalSettings?.playStoreURL!
                
                setMyUserDefaults(value: appStoreUrl, key: MyUserDefaults.AppStoreUrl)
                setMyUserDefaults(value: playStoreUrl, key: MyUserDefaults.PlayStoreURL)
                
                self.ImgBrandLogo.sd_setImage(with: URL(string: (GlobalObjects.objConfigResponse.globalSettings?.brandLogoURL!)!), placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                    // Your code inside completion block
                    if image == nil {
                        
                    }
                })
                
                self.arrHomeSettingOrder.removeAll()
                self.arrHomeSettingOrder = GlobalObjects.objConfigResponse.homeScreenSettings!
                //        self.arrHomeSettingOrder = self.arrHomeSettingOrder.sorted { $0.order  < $1.order }
                
                self.fetchShopifyCollectionSliderData()
                
                if (UIDevice.current.userInterfaceIdiom == .pad){
                    if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                        self.fetchShopifyProductGridData(imgMaxHeight: Int32(GlobalObjects.iPad_Grid_Portrait_height))
                        self.fetchShopifyProductSliderData(imgHeight: 650)
                        self.fetchRotatingImageURL()
                    }else{
                        self.fetchShopifyProductGridData(imgMaxHeight: Int32(GlobalObjects.iPad_Grid_Square_height))
                        self.fetchShopifyProductSliderData(imgHeight: 500)
                        self.fetchRotatingImageURL()
                    }
                }else{
                    if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                        self.fetchShopifyProductGridData(imgMaxHeight: Int32(GlobalObjects.iPhone_Grid_Portrait_height))
                        self.fetchShopifyProductSliderData(imgHeight: 480)
                        self.fetchRotatingImageURL()
                    }else{
                        self.fetchShopifyProductGridData(imgMaxHeight: Int32(GlobalObjects.iPhone_Grid_Square_height))
                        self.fetchShopifyProductSliderData(imgHeight: 300)
                        self.fetchRotatingImageURL()
                    }
                }
                
                let planExpDate = GlobalObjects.objConfigResponse.globalSettings?.planExpDate!
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let exp_Date = dateFormatter.date(from:planExpDate!)!
                let currentDate = Date()
                if exp_Date < currentDate {
                    
                    DispatchQueue.main.async {
                                                
                        let alert = UIAlertController(title: "plan_exp_title".localized, message: "plan_exp_msg".localized, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "btnOk".localized, style: .default, handler: { (Finish) in
                          
                            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
                            Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
                                exit(0)
                            }
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                if GlobalObjects.isUpdateAvailable {
                    if GlobalObjects.objConfigResponse.globalSettings?.appUpdate! == UpdateOption.compulsory.rawValue {
                        //alert for compulsory
                        DispatchQueue.main.async {
                            updateAlertWithDelegate("app_update_title".localized, msg: "app_update_msg".localized, button1: "Later", button2: "Update") { finish in
                                if finish == 1 {
                                    print("Later clicked")
                                }else{
                                    if let url = URL(string: "https://apps.apple.com/us/app/ecentradocs/id1465888439?mt=8"){
                                        UIApplication.shared.open(url)
                                    }
                                }
                            }
                        }
                    }else{
                        //alert for optional
                        DispatchQueue.main.async {
                            showAlertWithDelegate("app_update_title".localized, msg: "app_update_msg".localized, button1: "btnOk".localized) { finish in
                                if finish == 1 {
                                    
                                }
                            }
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.tblHomeLists.isHidden = false
                    self.tblHomeLists.reloadData()
                }
            }
            
        }else{
            DispatchQueue.main.async {
                self.Showalert1(message: "no_internet_msg".localized)
            }
        }
    }
    
    @objc func reloadWhenAPNSCall(_ notification: NSNotification) {
        
        DispatchQueue.main.async {
            self.tblHomeLists.reloadData()
            self.view.layoutIfNeeded()
        }
        
        
    }
    
    func didcellTapped(collectionID: String, CollectionName: String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let detailVC = storyBoard.instantiateViewController(withIdentifier: "ProductListVC") as? ProductListVC {
            detailVC.CollectionID = collectionID
            detailVC.ScreenTitle = CollectionName
            self.navigationController?.pushViewController(detailVC, animated: true)
            //            self.present(detailVC, animated: true, completion: nil)
        }
    }
    
    
    @objc func goToProductList(notification: NSNotification){
        
        if let object = notification.userInfo as NSDictionary? {
            let receivedID = object["id"] as? String
            let receivedName = object["name"] as? String
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let detailVC = storyBoard.instantiateViewController(withIdentifier: "ProductListVC") as? ProductListVC {
                detailVC.CollectionID = receivedID!
                detailVC.ScreenTitle = receivedName!
                self.navigationController?.pushViewController(detailVC, animated: true)
                //            self.present(detailVC, animated: true, completion: nil)
            }
        }
        
    }
    
    @objc func goToProductDetail(notification: NSNotification){
        
        if let object = notification.userInfo as NSDictionary? {
            let receivedID = object["id"] as? String
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let detailVC = storyBoard.instantiateViewController(withIdentifier: "ProductDetailVC") as? ProductDetailVC {
                detailVC.Product_Id = receivedID!
                self.navigationController?.pushViewController(detailVC, animated: true)
            }
        }
        
    }
    
    func fetchShopifyCollectionSliderData(){
        
        ShopifyCollectionSliderData.shared.fetchShopifyCollectionSliderData { collectionData, error in
            if  collectionData.count != 0 {
                self.storeFrontCollections.removeAllObjects()
                self.storeFrontCollections = collectionData.mutableCopy() as! NSMutableArray
                
                DispatchQueue.main.async {
                    self.tblHomeLists.reloadData()
                }
            }else{
                self.Showalert1(message:error!)
            }
        }
    }
    
    func fetchShopifyProductGridData(imgMaxHeight: Int32){
        
        ShopifyProductGridData.shared.fetchShopifyProductGridData(imgMaxHeight: imgMaxHeight) { products,titleheader, error in
            self.productGridTitle = titleheader
            
            if products.count != 0 {
                self.ImgProductGridURLs.removeAll()
                self.GridProducts.removeAllObjects()
                var dictProduct = [String: Any]()
                
                products.forEach { product in
                    
                    dictProduct = [
                        "Id": product.id.rawValue,
                        "title": product.title,
                        "min_price": product.priceRange.minVariantPrice.amount,
                        "max_price": product.priceRange.maxVariantPrice.amount,
                        "Min_Currency": product.priceRange.minVariantPrice.currencyCode.rawValue,
                        "Max_Currency": product.priceRange.maxVariantPrice.currencyCode.rawValue
                    ]
                    
                    self.GridProducts.add(dictProduct)
                    
                    let images   = product.images.edges.map { $0.node }
                    images.forEach { image in
                        
                        let imageUrl = image.transformedSrc
                        self.ImgProductGridURLs.append(imageUrl)
                    }
                }
                
                DispatchQueue.main.async {
                    self.tblHomeLists.reloadData()
                }
                
            }else{
                self.Showalert1(message:error!)
            }
        }
    }
    
    func fetchShopifyProductSliderData(imgHeight: Int32){
        ShopifyProductSliderData.shared.fetchShopifyProductSliderData(imgMaxHeight: imgHeight) { products,titleheader, error  in
            
            self.productSliderTitle = titleheader
            if products.count != 0 {
                
                var dictProduct = [String: Any]()
                self.SliderProducts.removeAllObjects()
                self.ImgProductsSlider.removeAll()
                
                products.forEach { product in
                    
                    dictProduct = [
                        
                        "Id": product.id.rawValue,
                        "title": product.title,
                        "min_price": product.priceRange.minVariantPrice.amount,
                        "max_price": product.priceRange.maxVariantPrice.amount,
                        "Min_Currency": product.priceRange.minVariantPrice.currencyCode.rawValue,
                        "Max_Currency": product.priceRange.maxVariantPrice.currencyCode.rawValue
                        
                    ]
                    
                    self.SliderProducts.add(dictProduct)
                    
                    let images   = product.images.edges.map { $0.node }
                    images.forEach { image in
                        
                        let imageUrl = image.transformedSrc
                        self.ImgProductsSlider.append(imageUrl)
                    }
                    
                }
                DispatchQueue.main.async {
                    self.tblHomeLists.reloadData()
                }
            }else{
                self.Showalert1(message:error!)
            }
        }
    }
    
    func fetchRotatingImageURL(){
        
        var arrObj: [Image]!
        var objProduct = [RotatingImage]()
        var objCollection = [RotatingImage]()
        
        for i in 0..<GlobalObjects.objConfigResponse.homeScreenSettings!.count {
            let itemName = GlobalObjects.objConfigResponse.homeScreenSettings![i].layoutType
            if itemName == HomeLayoutType.rotatingImageSlider.rawValue {
                arrObj = (GlobalObjects.objConfigResponse.homeScreenSettings![i].data?.imageSlider!)!
            }
        }
        self.objArrImageSlider.removeAll()
        for item in arrObj{
            self.objArrImageSlider.append(RotatingImage(id: item.id, bannerImageURL: item.bannerImageURL, platformCollectionID: item.platformCollectionID, platformProductID: item.platformProductID, isClickable: item.isClickable, isCollection: item.isCollection, isProduct: item.isProduct, onClickRedirection: item.onClickRedirection, imageURL: item.imageURL))
        }
        
        for item in self.objArrImageSlider {
            if item.imageURL == nil {
                if item.platformCollectionID == nil {
                    objProduct.append(item)
                }else if item.platformProductID == nil {
                    objCollection.append(item)
                }
            }
        }
        
        ShopifyRotatingImageData.shared.fetchShopifyProductsData(objProducts: objProduct) { productsObj, error in
            productsObj.forEach { product in
                
                if let row = self.objArrImageSlider.firstIndex(where: {$0.platformProductID == product.platformProductID}) {
                    self.objArrImageSlider[row] = product
                }
            }
        }
        
        ShopifyRotatingImageData.shared.fetchShopifyCollectionsData(objCollections: objCollection) { collectionObj, error in
            collectionObj.forEach { collection in
                if let row = self.objArrImageSlider.firstIndex(where: {$0.platformCollectionID == collection.platformCollectionID}) {
                    self.objArrImageSlider[row] = collection
                }
            }
        }
        
        DispatchQueue.main.async {
            self.tblHomeLists.reloadData()
        }
        
        
    }
    
    @IBAction func btnCartTapped(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let detailVC = storyBoard.instantiateViewController(withIdentifier: "CartNavigationController") as? CartNavigationController {
            self.navigationController?.present(detailVC, animated: true, completion: nil)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        
        self.searchBar.endEditing(true)
        if searchBar.text! != "" {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let detailVC = storyBoard.instantiateViewController(withIdentifier: "SearchVC") as? SearchVC {
                detailVC.searchText = searchBar.text!
                self.navigationController?.pushViewController(detailVC, animated: true)
            }
        }
    }
    
    @objc func viewAllCollectionSliderItems(sender: Any){
        
        let indexTag = (sender as AnyObject).tag!
        let collection_ID = GlobalObjects.objConfigResponse.homeScreenSettings![indexTag].data!.product!.platformCollectionID!
        let title = GlobalObjects.objConfigResponse.homeScreenSettings![indexTag].title ?? self.productSliderTitle
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let detailVC = storyBoard.instantiateViewController(withIdentifier: "ProductListVC") as? ProductListVC {
            detailVC.CollectionID = collection_ID
            detailVC.ScreenTitle = title
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    @objc func viewAllProductSliderItems(sender: Any){
        
        let indexTag = (sender as AnyObject).tag!
        let collection_ID = GlobalObjects.objConfigResponse.homeScreenSettings![indexTag].data!.product!.platformCollectionID!
        let title = GlobalObjects.objConfigResponse.homeScreenSettings![indexTag].title ?? self.productGridTitle
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let detailVC = storyBoard.instantiateViewController(withIdentifier: "ProductListVC") as? ProductListVC {
            detailVC.CollectionID = collection_ID
            detailVC.ScreenTitle = title
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension DashboardView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrHomeSettingOrder.count != 0 {
            return self.arrHomeSettingOrder.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.arrHomeSettingOrder.count != 0 {
            let itemName = self.arrHomeSettingOrder[indexPath.row].layoutType
            
            if itemName == HomeLayoutType.collectionSlider.rawValue {
                
                let cell = tblHomeLists.dequeueReusableCell(withIdentifier:"CollectionSliderTableCell", for: indexPath) as! CollectionSliderTableCell
                
                //                if self.storeFrontCollections.count != 0 {
                cell.storeFrontCollections = self.storeFrontCollections
                cell.collectionCategories.collectionViewLayout.invalidateLayout()
                cell.collectionCategories.reloadData()
                //                }
                return cell
                
            }else if itemName == HomeLayoutType.imageBanner.rawValue {
                
                let cell = tblHomeLists.dequeueReusableCell(withIdentifier:"BannerImageCell", for: indexPath) as! BannerImageCell
                
                let imgURL = URL(string: (GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].data?.imageBanner?.bannerImageURL)!)
                
                cell.ImgBanner.sd_setImage(with: imgURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), completed: { (image, error, cacheType, url) -> Void in
                    // Your code inside completion block
                    self.tblHomeLists.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
                    if image != nil {
                        cell.setCustomImage(image: image!)
                    }
                })
                
                return cell
                
            }else if itemName == HomeLayoutType.rotatingImageSlider.rawValue {
                
                let cell = tblHomeLists.dequeueReusableCell(withIdentifier:"rotatingImageSliderCell", for: indexPath) as! rotatingImageSliderCell
                
                cell.objArrImageSlider = self.objArrImageSlider
                let arrObj = self.objArrImageSlider
                cell.ImgRotatingUrls = arrObj.map {$0.imageURL}
                cell.collectionSlider.isUserInteractionEnabled = true
                DispatchQueue.main.async {
                    cell.collectionSlider.reloadData()
                    cell.collectionSlider.layoutIfNeeded()
                }
                return cell
                
            }else if itemName == HomeLayoutType.productSlider.rawValue {
                
                let cell = tblHomeLists.dequeueReusableCell(withIdentifier:"CollectionTableCell", for: indexPath) as! CollectionTableCell
                cell.delegate = self
                
                if GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].title == nil {
                    cell.lblProductSliderTitle.text! =  self.productSliderTitle
                }else{
                    cell.lblProductSliderTitle.text! = GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].title!
                }
                
                cell.lblProductSliderTitle.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
                cell.btnViewAll.tag = indexPath.row
                cell.btnViewAll.addTarget(self, action: #selector(self.viewAllCollectionSliderItems(sender:)), for: .touchUpInside)
                cell.ImgProductsList = self.ImgProductsSlider
                cell.arrProductSliderList = self.SliderProducts.mutableCopy() as! NSMutableArray
                cell.Collection_ID = GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].data!.product!.platformCollectionID!
                cell.CollectionName = "\(cell.lblProductSliderTitle.text!)"
                
                DispatchQueue.main.async {
                    cell.ProductCollection.reloadData()
                    cell.heightCollectionViewConstant.constant =
                    cell.ProductCollection.collectionViewLayout.collectionViewContentSize.height
                    cell.ProductCollection.layoutIfNeeded()
                }
                
                return cell
                
                
            }else if itemName == HomeLayoutType.productGrid.rawValue {
                
                let cell = tblHomeLists.dequeueReusableCell(withIdentifier:"GridLayoutTableCell", for: indexPath) as! GridLayoutTableCell
                
                let homeObject = GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row]
                
                cell.lblGridTitle.text! = homeObject.title ?? self.productGridTitle
                cell.lblGridTitle.font = UIFont.appRegularFontWith(size: AppFontSize.size16)
                
                cell.btnViewAll.tag = indexPath.row
                cell.btnViewAll.addTarget(self, action: #selector(self.viewAllProductSliderItems(sender:)), for: .touchUpInside)
                
                cell.arrProductDetail = self.GridProducts.mutableCopy() as! NSMutableArray
                cell.CollectionName = cell.lblGridTitle.text!
                cell.ImgProductGridURLs = self.ImgProductGridURLs
                
                DispatchQueue.main.async {
                    cell.GridCollection.reloadData()
                    cell.heightCollectionview.constant = cell.GridCollection.collectionViewLayout.collectionViewContentSize.height
                    cell.GridCollection.layoutIfNeeded()
                }
                
                return cell
                
            }else{
                
                if GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].data?.announcement != nil {
                    
                    let endDateTime = GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].data?.announcement!.endDateTime!
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let end_Date = dateFormatter.date(from:endDateTime!)!
                    let currentDate = Date()
                    if end_Date > currentDate {
                        let cell = tblHomeLists.dequeueReusableCell(withIdentifier:"AnnouncementBarCell", for: indexPath) as! AnnouncementBarCell
                        
                        cell.lblAnnouncement.text! = (GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].data?.announcement!.announcementTitle!)!
                        cell.lblAnnouncement.font = UIFont.appRegularFontWith(size: AppFontSize.size15)
                        cell.lblAnnouncement.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].data?.announcement!.textColor!)!)
                        cell.BaseView.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].data?.announcement!.backgroundColor!)!)
                        
                        return cell
                    }else {
                        return UITableViewCell()
                    }
                }else{
                    return UITableViewCell()
                }
                
            }
        }else{
            
            let cell = tblHomeLists.dequeueReusableCell(withIdentifier:"DefaultCell", for: indexPath) as! DefaultCell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.arrHomeSettingOrder.count != 0 {
            let itemName = self.arrHomeSettingOrder[indexPath.row].layoutType
            if itemName ==  HomeLayoutType.collectionSlider.rawValue {
                if (UIDevice.current.userInterfaceIdiom == .pad){
                    return 105
                }else{
                    return 85
                }
            }
            else if itemName == HomeLayoutType.imageBanner.rawValue {
                if (UIDevice.current.userInterfaceIdiom == .pad){
                    
                    //                return 230
                    return UITableView.automaticDimension
                }else{
                    
                    //            return 130
                    return UITableView.automaticDimension
                }
                
            }else if itemName == HomeLayoutType.rotatingImageSlider.rawValue {
                if self.objArrImageSlider.count != 0 {
                if (UIDevice.current.userInterfaceIdiom == .pad) {
                    if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                        
                        return ScreenSize.WIDTH * (0.66)
                    }else{
                        return ScreenSize.WIDTH * (0.66)
                    }
                    
                }else{
                    if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                        return ScreenSize.WIDTH * (0.66)
                        
                    }else{
                        return ScreenSize.WIDTH * (0.66)
                    }
                    
                }
                }else{
                    return 0
                }
            }else if itemName == HomeLayoutType.productSlider.rawValue {
                if (UIDevice.current.userInterfaceIdiom == .pad){
                    if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                        return 370
                    }else{
                        return 300
                    }
                    
                }else{
                    if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                        //                    return 320
                        return UITableView.automaticDimension
                    }else{
                        //                    return 280
                        return UITableView.automaticDimension
                    }
                    
                }
            }else if itemName == HomeLayoutType.productGrid.rawValue {
                if self.ImgProductGridURLs.count != 0 {
                    if (UIDevice.current.userInterfaceIdiom == .pad){
                        if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                            
                            return UITableView.automaticDimension
                        }else{
                            return UITableView.automaticDimension
                        }
                        
                    }else{
                        if GlobalObjects.objConfigResponse.globalSettings?.appLayoutType == AppLayout.portrait.rawValue {
                            
                            return UITableView.automaticDimension
                        }else{
                            return UITableView.automaticDimension
                        }
                        
                    }
                }else{
                    if (UIDevice.current.userInterfaceIdiom == .pad){
                        
                        return 0
                    }else{
                        
                        return 0
                    }
                }
            }
            else{
                if GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].data?.announcement != nil {
                    let endDateTime = GlobalObjects.objConfigResponse.homeScreenSettings![indexPath.row].data?.announcement!.endDateTime!
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let end_Date = dateFormatter.date(from:endDateTime!)!
                    let currentDate = Date()
                    if end_Date > currentDate {
                        return UITableView.automaticDimension
                    }else{
                        return 0
                    }
                }else{
                    return 0
                }
            }
        }else{
            return 50
        }
    }
    
}

