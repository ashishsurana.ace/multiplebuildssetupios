//
//  ShopifyProductGridData.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation
import MobileBuySDK


class ShopifyProductGridData {
    
    static let shared = ShopifyProductGridData()
    var objArrProductGrid: Product!
    var storefrontProducts = [Storefront.Product]()
    var isTitleGiven:Bool = true
    
    func fetchShopifyProductGridData(imgMaxHeight: Int32, completion: @escaping ([Storefront.Product], String, String?) -> Void){
        
        for i in 0..<GlobalObjects.objConfigResponse.homeScreenSettings!.count {
            let itemName = GlobalObjects.objConfigResponse.homeScreenSettings![i].layoutType
            if itemName == HomeLayoutType.productGrid.rawValue {
                self.objArrProductGrid = GlobalObjects.objConfigResponse.homeScreenSettings![i].data?.product!
                if GlobalObjects.objConfigResponse.homeScreenSettings![i].title == nil {
                    self.isTitleGiven = false
                }
            }
        }
        
        self.storefrontProducts.removeAll()
        
        QueryFetchProducts.shared.fetchProductGridData(isTitlePresent: self.isTitleGiven, id: GraphQL.ID(rawValue: "\(String(describing: self.objArrProductGrid!.platformCollectionID!))"), total: Int32(self.objArrProductGrid!.showTotalProducts), imgMaxHeight: imgMaxHeight) { products, title, error in
            
            if products.count != 0 {
                self.storefrontProducts = products
                completion(self.storefrontProducts, title, nil)
                
            }else{
                
                let error = "errorMsg".localized
                completion([], "", error)
            }
        }
    }
    
    
}
