//
//  ShopifyCollectionSliderData.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation
import MobileBuySDK


class ShopifyCollectionSliderData {
    
    static let shared = ShopifyCollectionSliderData()
    var objArrCollectionSlider = [Collection]()
    var storeFrontCollections = NSMutableArray()
    
    func fetchShopifyCollectionSliderData(completion: @escaping (NSMutableArray, String?) -> Void){
        
        for i in 0..<GlobalObjects.objConfigResponse.homeScreenSettings!.count {
            let itemName = GlobalObjects.objConfigResponse.homeScreenSettings![i].layoutType
            if itemName == HomeLayoutType.collectionSlider.rawValue {
                
                //            let jsonData = GlobalObjects.jsonString.data(using: .utf8)!
                //            let objConfig = try! JSONDecoder().decode(ConfigResponse.self, from: jsonData)
                self.objArrCollectionSlider = (GlobalObjects.objConfigResponse.homeScreenSettings![i].data?.collections!)!
            }
        }
        
        var ids = [GraphQL.ID]()
        self.storeFrontCollections.removeAllObjects()
        
        for item in self.objArrCollectionSlider {
            ids.append(GraphQL.ID(rawValue: "\(String(describing: item.platformCollectionID!))"))
        }
        
        ClientQueries.Queries.fetchCollections(ids: ids) { collectionObj, error in
            if  collectionObj.count != 0  {
                
                for item in self.objArrCollectionSlider {
                    collectionObj.forEach { collection in
                        
                        if item.platformCollectionID == collection.id.rawValue {
                            //                            item.shopifyImageUrl = collection.image!.originalSrc
                            let arrCollections: [String: Any] = ["id":item.id,
                                                                 "shopifyCollectionID": item.platformCollectionID!,
                                                                 "ShopifyCollectionName": collection.title,
                                                                 "customTitle":item.customTitle ?? "",
                                                                 "isShowOverlay":item.isShowOverlay,
                                                                 "isShowCustomTitle": item.isShowCustomTitle,
                                                                 "customImageURL": item.customImageURL as Any,
                                                                 "shopifyImageUrl":collection.image?.originalSrc]
                            
                            if !self.storeFrontCollections.contains(arrCollections) {
                                self.storeFrontCollections.add(arrCollections)
                            }
                        }
                    }
                }
                completion(self.storeFrontCollections, nil)
            }else{
                
                let error = "errorMsg".localized
                completion([], error)
            }
        }
        
    }
}
