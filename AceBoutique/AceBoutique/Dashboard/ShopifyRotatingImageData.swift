//
//  ShopifyRotatingImageData.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 04/08/21.
//

import Foundation
import MobileBuySDK

class ShopifyRotatingImageData {
    
    static let shared = ShopifyRotatingImageData()
    var objArrCollections = [RotatingImage]()
    var objArrProducts = [RotatingImage]()
    
    func fetchShopifyProductsData(objProducts:[RotatingImage], completion: @escaping ([RotatingImage], String?) -> Void){
        
        var ids = [GraphQL.ID]()
        self.objArrProducts.removeAll()
        
        for item in objProducts {
            ids.append(GraphQL.ID(rawValue: "\(String(describing: item.platformProductID!))"))
        }
        
        for id in ids{
            ProductSliderQueries.shared.fetchProductSliderImg(id: id) { productObj, error in
                if let product = productObj {
                    for item in objProducts {
                        if item.platformProductID == product.id.rawValue {
                            let imageNode = product.images.edges.map { $0.node }
                            self.objArrProducts.append(RotatingImage(id: item.id, bannerImageURL: item.bannerImageURL, platformCollectionID: item.platformCollectionID, platformProductID: item.platformProductID, isClickable: item.isClickable, isCollection: item.isCollection, isProduct: item.isProduct, onClickRedirection: item.onClickRedirection, imageURL: imageNode[0].transformedSrc.absoluteString))
                        }
                    }
                    completion(self.objArrProducts,nil)
                }else{
                    completion([],error.debugDescription)
                }
                
            }
        }
        
    }
    
    func fetchShopifyCollectionsData(objCollections:[RotatingImage], completion: @escaping ([RotatingImage], String?) -> Void){
        
        var ids = [GraphQL.ID]()
        self.objArrCollections.removeAll()
        
        for item in objCollections {
            ids.append(GraphQL.ID(rawValue: "\(String(describing: item.platformCollectionID!))"))
        }
        
        CollectionSliderQueries.Queries.fetchCollectionSliderImg(ids: ids) { collectionObj, error in
            
            if  collectionObj.count != 0  {
                for item in objCollections {
                    collectionObj.forEach { collection in
                        if item.platformCollectionID! == collection.id.rawValue {
                            
                            self.objArrCollections.append(RotatingImage(id: item.id, bannerImageURL: item.bannerImageURL, platformCollectionID: item.platformCollectionID, platformProductID: item.platformProductID, isClickable: item.isClickable, isCollection: item.isCollection, isProduct: item.isProduct, onClickRedirection: item.onClickRedirection, imageURL: collection.image?.transformedSrc.absoluteString))
                        }
                    }
                }
                completion(self.objArrCollections, nil)
            }else{
                
                let error = "errorMsg".localized
                completion([], error)
            }
            
        }
    }
}
