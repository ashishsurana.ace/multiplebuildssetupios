//
//  ShopifyProductSliderData.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 09/06/21.
//

import Foundation
import MobileBuySDK


class ShopifyProductSliderData {
    
    static let shared = ShopifyProductSliderData()
    var storefrontProductSlider = [Storefront.Product]()
    var objArrProductSlider: Product!
    var isTitleGiven:Bool = true
    
    func fetchShopifyProductSliderData(imgMaxHeight: Int32, completion: @escaping ([Storefront.Product],String, String?) -> Void){
        for i in 0..<GlobalObjects.objConfigResponse.homeScreenSettings!.count {
            let itemName = GlobalObjects.objConfigResponse.homeScreenSettings![i].layoutType
            if itemName == HomeLayoutType.productSlider.rawValue {
                self.objArrProductSlider = GlobalObjects.objConfigResponse.homeScreenSettings![i].data?.product!
                if GlobalObjects.objConfigResponse.homeScreenSettings![i].title == nil {
                    self.isTitleGiven = false
                }
            }
        }
        
        self.storefrontProductSlider.removeAll()
        
        QueryFetchProducts.shared.fetchProductGridData(isTitlePresent: self.isTitleGiven, id: GraphQL.ID(rawValue: "\(String(describing: self.objArrProductSlider!.platformCollectionID!))"), total: Int32(self.objArrProductSlider!.showTotalProducts), imgMaxHeight: imgMaxHeight) { products,title,error  in
            
            if products.count != 0 {
                self.storefrontProductSlider = products
                
                completion(self.storefrontProductSlider,title, nil)
                
            }else{
                
                let error = "errorMsg".localized
                completion([],"", error)
            }
        }
        
        
    }
}
