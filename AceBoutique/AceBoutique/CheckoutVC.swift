//
//  CheckoutVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 24/03/21.
//

import UIKit
import MobileBuySDK
import FirebaseAnalytics

class CheckoutNavController: UINavigationController {
    // Exists to provide type-safe instantiation from storyboard
}


class CheckoutVC: UIViewController {
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var tblCheckout: UITableView!
    @IBOutlet weak var buttonBaseView: UIView!
    @IBOutlet weak var btnContinuePayment: UIControl!
    @IBOutlet weak var lblTotalItems: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblMakePayment: UILabel!
    
    
    var subtotal: Int! = 0
    var shipping: Int! = 0
    var total:Int! = 0
    var totalItem: String!
    
    var arrCartItems = [MyCartItems]()
    var ImgProductsList = [URL]()
    var Cart_DB = CartQueries()
    var totalQty: Int! = 0
    var totalPrice: Float! = 0.0
    var checkoutEmail: String!
    var strOrderNotes: String! = ""
    var shippingAddress: CustomerAddress?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tblCheckout.rowHeight = UITableView.automaticDimension
        self.tblCheckout.estimatedRowHeight = UITableView.automaticDimension
        
        self.tblCheckout.tableFooterView = UIView()
        
        self.tblCheckout.register(UINib(nibName: "checkoutLoginSessionCell", bundle: nil), forCellReuseIdentifier: "checkoutLoginSessionCell")
        self.tblCheckout.register(UINib(nibName: "CheckoutUserSessionCell", bundle: nil), forCellReuseIdentifier: "CheckoutUserSessionCell")
        self.tblCheckout.register(UINib(nibName: "shippingAddressCell", bundle: nil), forCellReuseIdentifier: "shippingAddressCell")
        self.tblCheckout.register(UINib(nibName: "OrderSummaryCell", bundle: nil), forCellReuseIdentifier: "OrderSummaryCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateOrderNotes(notification:)), name: NSNotification.Name(rawValue: "showOrderNotes"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCustomerAddress(notification:)), name: NSNotification.Name(rawValue: "setAddress"), object: nil)
        
        
    }
    
    @objc func updateOrderNotes(notification: NSNotification){
        
        if let object = notification.userInfo as NSDictionary? {
            let strNotes = object["notes"] as? String
            self.strOrderNotes = strNotes!
            self.tblCheckout.reloadData()
            
        }
        
    }
    
    @objc func updateCustomerAddress(notification: NSNotification){
        
        if let object = notification.userInfo as NSDictionary? {
            let strAddress = object["address"] as? CustomerAddress
            self.shippingAddress = strAddress!
            self.tblCheckout.reloadData()
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.lblScreenTitle.font = UIFont.appMediumFontWith(size: AppFontSize.size18)
        self.view.isUserInteractionEnabled = true
        FirebaseEventsLog.request.trackScreen(screenName: "Checkout", className: "CheckoutVC")
        self.btnContinuePayment.backgroundColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!)
        self.lblMakePayment.textColor = UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!).contrastColor(color: UIColor(hexString: (GlobalObjects.objConfigResponse.globalSettings?.themeColor!)!))
        self.arrCartItems.removeAll()
        self.ImgProductsList.removeAll()
        self.totalQty = 0
        self.totalPrice = 0.0
        
        self.arrCartItems = Cart_DB.GetAllCartItems()
        self.arrCartItems = self.arrCartItems.sorted { $0.id  > $1.id }
        var currencyCode = String()
        
        if self.arrCartItems.count != 0 {
            
            for item in self.arrCartItems {
                
                self.totalQty += item.quantity
                let price = ((item.price as NSString).floatValue)*Float(item.quantity)
                self.totalPrice += price
                self.ImgProductsList.append(URL(string: item.Img_url)!)
                currencyCode = item.currency_code
            }
        }
        
        if self.totalQty > 1 {
            self.lblItems.text! = "items".localized
        }else {
            self.lblItems.text! = "item".localized
        }
        
        self.lblTotalItems.text! = "\(String(totalQty!))"
        
        let locale = Locale.getSymbolForCurrencyCode(code: currencyCode)
        self.lblTotalPrice.text! = "\(locale)\(String(format: "%.2f",totalPrice!))"
        
        
        GlobalObjects.CustomerAddresses.removeAll()
        
        
        if  UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail) != nil {
            
            let accesstoken = UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken)
            //            if  self.shippingAddress == nil {
            self.getCustomerAddress(accessToken: accesstoken!, addresses: 10)
            //            }
            
        }else {
            
            self.btnContinuePayment.alpha = 0.5
            self.btnContinuePayment.isUserInteractionEnabled = false
            self.tblCheckout.reloadData()
        }
    }
    
    func getCustomerAddress(accessToken: String, addresses: Int32){
        
        ShopifyCustomerAddress.shared.getCustomerAddress(accessToken: accessToken, addresses: addresses) { customer, error in
            //            if let customer = customer {
            
            GlobalObjects.CustomerAddresses.removeAll()
            self.shippingAddress = nil
            
            
            if let defaultAddress = customer?.defaultAddress {
                
                GlobalObjects.CustomerAddresses.append(CustomerAddress(id: defaultAddress.id.rawValue, firstName: defaultAddress.firstName, lastName: defaultAddress.lastName, address1: defaultAddress.address1, address2: defaultAddress.address2, phone: defaultAddress.phone, city: defaultAddress.city, state: defaultAddress.province, country: defaultAddress.country, zipcode: defaultAddress.zip))
                
                
                if GlobalObjects.CustomerAddresses.count == 0 {
                    
                    self.btnContinuePayment.alpha = 0.5
                    self.btnContinuePayment.isUserInteractionEnabled = false
                    
                }else{
                    
                    self.btnContinuePayment.alpha = 1.0
                    self.btnContinuePayment.isUserInteractionEnabled = true
                }
                self.shippingAddress = GlobalObjects.CustomerAddresses[0]
                self.tblCheckout.reloadData()
                
            }
            
            else if let result = customer?.addresses {
                let addresses = result.edges.map {$0.node}
                
                addresses.forEach { address in
                    if GlobalObjects.CustomerAddresses.filter({$0.id == address.id.rawValue}).count == 0 {
                        
                        GlobalObjects.CustomerAddresses.append(CustomerAddress(id: address.id.rawValue, firstName: address.firstName!, lastName: address.lastName!, address1: address.address1!, address2: address.address2!, phone: address.phone, city: address.city!, state: address.province!, country: address.country!, zipcode: address.zip!))
                    }
                    
                }
                
                if GlobalObjects.CustomerAddresses.count == 0 {
                    self.btnContinuePayment.alpha = 0.5
                    self.btnContinuePayment.isUserInteractionEnabled = false
                }else{
                    self.btnContinuePayment.alpha = 1.0
                    self.btnContinuePayment.isUserInteractionEnabled = true
                }
                
                DispatchQueue.main.async {
                self.tblCheckout.reloadData()
                self.view.layoutIfNeeded()
                self.view.setNeedsDisplay()
                }
            }
            //            }
            else{
                self.Showalert1(message: error!)
            }
        }
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPaymentTapped(_ sender: Any) {
        
        if CheckNetwork.isConnectedToNetwork() == true {
            
        self.view.isUserInteractionEnabled = false
        
        var cartItems = [Storefront.CheckoutLineItemInput]()
        for item in self.arrCartItems {
            cartItems.append(Storefront.CheckoutLineItemInput.create(quantity: Int32(item.quantity), variantId: GraphQL.ID(rawValue: "\(String(describing: item.variant_id))")))
        }
        
        let address1 = shippingAddress?.address1
        let address2 = shippingAddress?.address2
        let city = shippingAddress?.city
        let country = shippingAddress?.country
        let firstName = shippingAddress?.firstName
        let lastName = shippingAddress?.lastName
        let phone = shippingAddress?.phone
        let province = shippingAddress?.state
        let zipcode = shippingAddress?.zipcode
        
        let shippingAddress = Storefront.MailingAddressInput.create(
            
            address1:  .value("\(String(describing: address1 ?? ""))"),
            address2:  .value("\(String(describing: address2 ?? ""))"),
            city:      .value("\(String(describing: city ?? ""))"),
            country:   .value("\(String(describing: country ?? ""))"),
            firstName: .value("\(String(describing: firstName ?? ""))"),
            lastName:  .value("\(String(describing: lastName ?? ""))"),
            phone:     .value("\(String(describing: phone ?? ""))"),
            province:  .value("\(String(describing: province ?? ""))"),
            zip:       .value("\(String(describing: zipcode ?? ""))")
            
        )
        
        var customAttributes = [Storefront.AttributeInput]()
        if UserDefaults.standard.object(forKey: MyUserDefaults.firebaseToken) != nil {
        let fcmToken = UserDefaults.standard.string(forKey: MyUserDefaults.firebaseToken)!
        customAttributes.append(Storefront.AttributeInput(key: "pid", value: "2"))
        customAttributes.append(Storefront.AttributeInput(key: "did", value: fcmToken))
        }
        let userEmail = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
        let checkout = Storefront.CheckoutCreateInput.create(
            email: .value(userEmail!),
            lineItems: .value(cartItems),
            shippingAddress: .value(shippingAddress),
            note: .value(self.strOrderNotes),
            customAttributes: .value(customAttributes)
        )
        
        let mutation = Storefront.buildMutation { $0
            .checkoutCreate(input: checkout) { $0
                .checkout { $0
                    .id()
                    .webUrl()
                }
                .checkoutUserErrors { $0
                    .code()
                    .field()
                    .message()
                    
                }
            }
        }
        
        let task = client.mutateGraphWith(mutation) { response, error in
            
            var message: String
            let error = response?.checkoutCreate?.checkoutUserErrors ?? []
            if error.count != 0 {
                self.view.isUserInteractionEnabled = true
                message = error[0].message
                let alert = UIAlertController(title: "", message: message, preferredStyle:.alert)
                
                alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                DispatchQueue.main.async{
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }else {
                
                let checkout = response?.checkoutCreate?.checkout
                
                
                var cartItems = [String: Any]()
                var selectedItems = [Any]()
                for items in self.arrCartItems {
                    let itemDetails: [String: Any] = [
                        AnalyticsParameterItemID: items.id,
                        AnalyticsParameterItemName: items.title,
                        AnalyticsParameterItemVariant: items.variant_value,
                        AnalyticsParameterQuantity : items.quantity,
                        AnalyticsParameterCurrency: items.currency_code
                        
                    ]
                    selectedItems.append(itemDetails)
                    
                }
                
                cartItems = [AnalyticsParameterValue: self.totalPrice!,
                             AnalyticsParameterItems: selectedItems]
                FirebaseEventsLog.request.logCheckoutEvent(items: cartItems)
                
                self.openSafariFor(checkout)
            }
            
        }
        
        task.resume()
        }else{
            
            Showalert1(message: "no_internet_msg".localized)
        }
    }
    
    func openSafariFor(_ checkout: Storefront.Checkout?) {
       
            let accessToken =  UserDefaults.standard.string(forKey: MyUserDefaults.CustomerToken)
            let webURL = checkout!.webUrl as URL
            let checkoutId = checkout!.id.rawValue

            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let detailVC = storyBoard.instantiateViewController(withIdentifier: "WebViewCheckoutVC") as? WebViewCheckoutVC {
                detailVC.accessToken = accessToken ?? ""
                detailVC.url = webURL
                detailVC.checkoutId = checkoutId
                self.present(detailVC, animated: true, completion: nil)
            }
    }
    
    @objc func btnSignInTapped(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyBoard.instantiateViewController(withIdentifier: "LoginRegistrationVC") as? LoginRegistrationVC
        self.present(detailVC!, animated: true, completion: nil)
    }
    
    @objc func btnCheckoutAsGuestTapped() {
        self.view.isUserInteractionEnabled = false
        var cartItems = [Storefront.CheckoutLineItemInput]()
        for item in self.arrCartItems {
            cartItems.append(Storefront.CheckoutLineItemInput.create(quantity: Int32(item.quantity), variantId: GraphQL.ID(rawValue: "\(String(describing: item.variant_id))")))
        }
        
        var customAttributes = [Storefront.AttributeInput]()
        if UserDefaults.standard.object(forKey: MyUserDefaults.firebaseToken) != nil {
        let fcmToken = UserDefaults.standard.string(forKey: MyUserDefaults.firebaseToken)!
        customAttributes.append(Storefront.AttributeInput(key: "pid", value: "2"))
        customAttributes.append(Storefront.AttributeInput(key: "did", value: fcmToken))
        }
        let checkout = Storefront.CheckoutCreateInput.create(
            email: .value("guest@gmail.com"),
            lineItems: .value(cartItems),
            customAttributes: .value(customAttributes)
            
        )
        
        let mutation = Storefront.buildMutation { $0
            .checkoutCreate(input: checkout) { $0
                .checkout { $0
                    .id()
                    .webUrl()
                }
                .checkoutUserErrors { $0
                    .code()
                    .field()
                    .message()
                    
                }
            }
        }
        
        let task = client.mutateGraphWith(mutation) { response, error in
            
            var message: String
            let error = response?.checkoutCreate?.checkoutUserErrors ?? []
            if error.count != 0 {
                self.view.isUserInteractionEnabled = true
                message = error[0].message
                let alert = UIAlertController(title: "", message: message, preferredStyle:.alert)
                
                alert.addAction(UIAlertAction(title: "btnOk".localized, style: UIAlertAction.Style.default, handler: nil))
                DispatchQueue.main.async{
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }else {
                
                let checkout = response?.checkoutCreate?.checkout
                

//                let currentDate = Date()
//                setMyUserDefaults(value: currentDate, key: MyUserDefaults.CheckoutTime)
                self.openSafariFor(checkout)
                
            }
            
        }
        task.resume()
    }
    
    @objc func addCustomerAddress(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyBoard.instantiateViewController(withIdentifier: "CustomerAddressVC") as? CustomerAddressVC
        self.present(detailVC!, animated: true, completion: nil)
        
    }
    
    @objc func showCustomerAddresses(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyBoard.instantiateViewController(withIdentifier: "SavedAddressesVC") as? SavedAddressesVC
        //        detailVC?.selectedID = shippingAddress!.id
        self.present(detailVC!, animated: true, completion: nil)
    }
    
    @objc func addOrderNotes(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyBoard.instantiateViewController(withIdentifier: "AddOrderNotesVC") as? AddOrderNotesVC
        detailVC!.strOrderNotes = self.strOrderNotes!
        self.present(detailVC!, animated: true, completion: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CheckoutVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if UserDefaults.standard.string(forKey: "UserEmail") != nil {
        return 3
        //        }else{
        //            return 2
        //        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            if UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail) != nil {
                
                let cell = tblCheckout.dequeueReusableCell(withIdentifier:"CheckoutUserSessionCell", for: indexPath) as! CheckoutUserSessionCell
                
                return cell
                
            }else{
                
                let cell = tblCheckout.dequeueReusableCell(withIdentifier:"checkoutLoginSessionCell", for: indexPath) as! checkoutLoginSessionCell
                
                if CHECKOUT_CUSTOMER_ACCOUNTS_STATUS.lowercased() == customerAccountStatus.required.rawValue.lowercased() {
                    cell.btnCheckoutAsGuest.isHidden = true
                    cell.heightbtnCheckoutAsGuestConstraint.constant = 0.0
                    cell.btnSignIn.addTarget(self, action: #selector(self.btnSignInTapped), for: .touchUpInside)
                }else{
                    cell.btnCheckoutAsGuest.isHidden = false
                    cell.heightbtnCheckoutAsGuestConstraint.constant = 28.0
                    cell.btnSignIn.addTarget(self, action: #selector(self.btnSignInTapped), for: .touchUpInside)
                    cell.btnCheckoutAsGuest.addTarget(self, action: #selector(self.btnCheckoutAsGuestTapped), for: .touchUpInside)
                }
                
                return cell
            }
            
        }else if indexPath.row  == 1 {
            
            let cell = tblCheckout.dequeueReusableCell(withIdentifier:"shippingAddressCell", for: indexPath) as! shippingAddressCell
            let userEmail = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
            if  userEmail == nil {
                
                cell.heightFullAddressConstant.constant = 0.0
                cell.fullAddressBaseView.isHidden = true
                cell.addressBaseView.isHidden = false
                cell.heightAddressViewConstant.constant = 50.0
                cell.addressBaseView.isUserInteractionEnabled = false
                cell.addressBaseView.alpha = 0.5
                cell.lblShippingLabel.alpha = 0.5
                
            }else{
                
                if self.shippingAddress == nil {
                    
                    cell.heightFullAddressConstant.constant = 0
                    cell.fullAddressBaseView.isHidden = true
                    cell.addressBaseView.isHidden = false
                    cell.heightAddressViewConstant.constant = 50.0
                    cell.addressBaseView.isUserInteractionEnabled = true
                    cell.addressBaseView.alpha = 1.0
                    cell.lblShippingLabel.alpha = 1.0
                    cell.addressBaseView.addTarget(self, action: #selector(self.addCustomerAddress), for: .touchUpInside)
                    cell.btnAddAddress.addTarget(self, action: #selector(self.addCustomerAddress), for: .touchUpInside)
                    
                }else{
                    
                    cell.heightFullAddressConstant.constant = 72.0
                    cell.fullAddressBaseView.isHidden = false
                    cell.addressBaseView.isHidden = true
                    cell.addressBaseView.isUserInteractionEnabled = false
                    cell.addressBaseView.alpha = 0.5
                    cell.lblShippingLabel.alpha = 1.0
                    cell.heightAddressViewConstant.constant = 0.0
                    cell.btnViewAllAddresses.addTarget(self, action: #selector(self.showCustomerAddresses), for: .touchUpInside)
                    
                    let address = "\(String(describing: self.shippingAddress!.firstName ?? ""))"+" \(String(describing: self.shippingAddress!.lastName ?? ""))\n\(String(describing: self.shippingAddress!.address1 ?? "")),\(String(describing: self.shippingAddress!.address2 ?? ""))\n\(String(describing: self.shippingAddress!.city ?? "")),\(String(describing: self.shippingAddress!.state ?? "")),\(String(describing: self.shippingAddress!.country ?? ""))-\(String(describing: self.shippingAddress!.zipcode ?? ""))"
                    
                    
                    cell.lblFullAddress.text! = address
                    self.buttonBaseView.alpha = 1.0
                    self.buttonBaseView.isUserInteractionEnabled = true
                }
            }
            
            
            cell.fullAddressBaseView.addTarget(self, action: #selector(self.showCustomerAddresses), for: .touchUpInside)
            
            return cell
            
        }else {
            
            let cell = tblCheckout.dequeueReusableCell(withIdentifier:"OrderSummaryCell", for: indexPath) as! OrderSummaryCell
            
            let userEmail = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
            if  userEmail != nil {
                //            if GlobalObjects.CustomerAddresses.count == 0 {
                //                cell.btnAddNotes.alpha = 0.5
                //                cell.btnAddNotes.isUserInteractionEnabled = false
                //            }else{
                cell.btnAddNotes.alpha = 1.0
                cell.btnAddNotes.isUserInteractionEnabled = true
                cell.btnViewNotes.isUserInteractionEnabled = true
                cell.btnAddNotes.addTarget(self, action: #selector(self.addOrderNotes), for: .touchUpInside)
                cell.btnViewNotes.addTarget(self, action: #selector(self.addOrderNotes), for: .touchUpInside)
                //            }
            }
            else{
                cell.btnAddNotes.alpha = 0.5
                cell.btnAddNotes.isUserInteractionEnabled = false
                cell.btnViewNotes.isUserInteractionEnabled = false
            }
            
            if self.strOrderNotes.TrimString(self.strOrderNotes) != "" {
                let notes = "notes".localized
                cell.lblNote.text! = "\(notes) \(String(describing: self.strOrderNotes!))"
            }else{
                cell.lblNote.text! = "ordernote".localized
            }
            //            cell.lblItems.text! = "\(String(totalQty!)) item"
            //            cell.lblSubtotal.text! = "$\(String(format: "%.2f",totalPrice!))"
            //            cell.lblTotal.text! = "$\(String(format: "%.2f",totalPrice!))"
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let userEmail = UserDefaults.standard.string(forKey: MyUserDefaults.UserEmail)
        
        if indexPath.row == 0 {
            if userEmail != nil {
            }else{
                
            }
        }else if indexPath.row == 1 {
            if userEmail != nil {
//                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let detailVC = storyBoard.instantiateViewController(withIdentifier: "CustomerAddressVC") as? CustomerAddressVC
//                self.present(detailVC!, animated: true, completion: nil)
            }
            
        }else{
            
        }
    }
    
    func tableView (_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
}
