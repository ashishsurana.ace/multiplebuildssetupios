//
//  CartDB.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 05/06/21.
//

import Foundation
import SQLite3

class CartQueries: NSObject {
    
func InsertCartItem(variant_id: String, title: String, variant_type: String, variant_value: String, price:String, currency_code: String, quantity: Int, Img_url: String) -> Void {
    
    let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")
    
    var db: OpaquePointer? = nil
    var statement: OpaquePointer? = nil
    
    if sqlite3_open(path, &db) != SQLITE_OK {
        
    }
    
    let insertSQL = String(format: "insert into MYCART (VARIANT_ID, TITLE, VARIANT_TYPE, VARIANT_VALUE, PRICE, CURRENCY_CODE, QUANTITY, IMAGE_URL) values (\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%d\", \"%@\")", variant_id, title, variant_type, variant_value, price, currency_code, quantity, Img_url)

    if sqlite3_prepare_v2(db, insertSQL, -1, &statement, nil) != SQLITE_OK
    {
        let errmsg = String(cString: sqlite3_errmsg(db))
        print("error preparing insert addCountry: \(errmsg)")
    }
    
    if sqlite3_step(statement) != SQLITE_DONE
    {
        let errmsg = String(cString: sqlite3_errmsg(db))
        print("failure inserting null: \(errmsg)")
    }
    
    if sqlite3_finalize(statement) != SQLITE_OK
    {
        let errmsg = String(cString: sqlite3_errmsg(db))
        print("error finalizing prepared statement: \(errmsg)")
    }
    
    statement = nil
    print("Item saved successfully.")
    
}


func GetAllCartItems() -> [MyCartItems] {

    let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")

    var CartList = [MyCartItems]()
    var db: OpaquePointer? = nil
    var statement: OpaquePointer? = nil

    if sqlite3_open(path, &db) != SQLITE_OK {
        
    }

    let selectSQL:String = String(format: "SELECT * FROM MYCART")

    if(sqlite3_prepare_v2(db, selectSQL, -1, &statement, nil) != SQLITE_OK) {


        let errmsg = String(cString: sqlite3_errmsg(db))
        print("Error preparing select getContact: \(errmsg)")
    }

    while sqlite3_step(statement) == SQLITE_ROW {
         
        let id = Int(sqlite3_column_int64(statement, 0))
        let variant_id = String(cString: sqlite3_column_text(statement, 1))
        let variant_title = String(cString: sqlite3_column_text(statement, 2))
        let variant_type = String(cString: sqlite3_column_text(statement, 3))
        let variant_value = String(cString: sqlite3_column_text(statement, 4))
        let price = String(cString: sqlite3_column_text(statement, 5))
        let currency_code = String(cString: sqlite3_column_text(statement, 6))
        let quantity = Int(sqlite3_column_int64(statement, 7))
        let Img_url = String(cString: sqlite3_column_text(statement, 8))
        
        CartList.append(MyCartItems(id: id, variant_id: variant_id, title: variant_title, variant_type: variant_type, variant_value: variant_value, price: price, currency_code: currency_code, quantity: quantity, Img_url: Img_url))

    }

    if sqlite3_finalize(statement) != SQLITE_OK {

        let errmsg = String(cString: sqlite3_errmsg(db))
        print("Error finalizing prepared statement: \(errmsg)")
    }
    statement = nil

    if sqlite3_close(db) != SQLITE_OK {

        print("Error closing database")
    }

    db = nil

    return CartList
}


func GetCartItemById(variant_Id: String) -> [MyCartItems] {

    let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")

    var CartItem = [MyCartItems]()
    var db: OpaquePointer? = nil
    var statement: OpaquePointer? = nil

    if sqlite3_open(path, &db) != SQLITE_OK {
        
    }

    let selectSQL:String = String(format: "SELECT * FROM MYCART WHERE VARIANT_ID = '%@'", variant_Id)

    if(sqlite3_prepare_v2(db, selectSQL, -1, &statement, nil) != SQLITE_OK) {


        let errmsg = String(cString: sqlite3_errmsg(db))
        print("Error preparing select getContact: \(errmsg)")
    }

    while sqlite3_step(statement) == SQLITE_ROW {

        
        let id = Int(sqlite3_column_int64(statement, 0))
        let variant_id = String(cString: sqlite3_column_text(statement, 1))
        let variant_title = String(cString: sqlite3_column_text(statement, 2))
        let variant_type = String(cString: sqlite3_column_text(statement, 3))
        let variant_value = String(cString: sqlite3_column_text(statement, 4))
        let price = String(cString: sqlite3_column_text(statement, 5))
        let currency_code = String(cString: sqlite3_column_text(statement, 6))
        let quantity = Int(sqlite3_column_int64(statement, 7))
        let Img_url = String(cString: sqlite3_column_text(statement, 8))
        
        CartItem.append(MyCartItems(id: id, variant_id: variant_id, title: variant_title, variant_type: variant_type, variant_value: variant_value, price: price, currency_code: currency_code, quantity: quantity, Img_url: Img_url))

    }

    if sqlite3_finalize(statement) != SQLITE_OK {

        let errmsg = String(cString: sqlite3_errmsg(db))
        print("Error finalizing prepared statement: \(errmsg)")
    }
    statement = nil

    if sqlite3_close(db) != SQLITE_OK {

        print("Error closing database")
    }

    db = nil

    return CartItem
}

func updateCartItem(_ quantity: Int, price:String, variant_id: String) {
    
    let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")
    
    var db: OpaquePointer? = nil
    var updateStatement: OpaquePointer?
    
    if sqlite3_open(path, &db) != SQLITE_OK {
        
    }
    
    let quaryStr = String.init(format: "UPDATE MYCART SET QUANTITY = '%d', PRICE = '%@' WHERE VARIANT_ID = '%@'",quantity, price,variant_id)
    print("Update Query:", quaryStr)
      if sqlite3_prepare_v2(db, quaryStr, -1, &updateStatement, nil) ==
          SQLITE_OK {
        if sqlite3_step(updateStatement) == SQLITE_DONE {
          print("\nSuccessfully updated row.")
        } else {
          print("\nCould not update row.")
        }
      } else {
        print("\nUPDATE statement is not prepared")
      }
      sqlite3_finalize(updateStatement)
    
    db = nil
    
    print("Item updated successfully.")
    
}


func deleteCartItem( variant_Id: String) {
    
    let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")
    
    var db: OpaquePointer? = nil
    
    if sqlite3_open(path, &db) != SQLITE_OK {
        
    }
    
    let deleteStatementString = String.init(format: "DELETE FROM MYCART WHERE VARIANT_ID = '%@'",variant_Id)
    
    var deleteStatement: OpaquePointer?
      if sqlite3_prepare_v2(db, deleteStatementString, -1, &deleteStatement, nil) ==
          SQLITE_OK {
        if sqlite3_step(deleteStatement) == SQLITE_DONE {
          print("\nSuccessfully deleted row.")
        } else {
          print("\nCould not delete row.")
        }
      } else {
        print("\nDELETE statement could not be prepared")
      }
      
      sqlite3_finalize(deleteStatement)
    
    db = nil
    
    print("Item deleted successfully.")
    
}

func deleteAllCartItem() -> Bool{
    
    let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")
    var isSuccess: Bool!
    var db: OpaquePointer? = nil
    
    if sqlite3_open(path, &db) != SQLITE_OK {
        
    }
    
    let deleteStatementString = String.init(format: "DELETE FROM MYCART")
    
    var deleteStatement: OpaquePointer?
      if sqlite3_prepare_v2(db, deleteStatementString, -1, &deleteStatement, nil) ==
          SQLITE_OK {
        if sqlite3_step(deleteStatement) == SQLITE_DONE {
            isSuccess = true
          print("\nSuccessfully deleted records.")
        } else {
          print("\nCould not delete records.")
            
        }
      } else {
        print("\nDELETE All statement could not be prepared")
      }
      
      sqlite3_finalize(deleteStatement)
    
    db = nil
    
    print("All Cart Item deleted successfully.")
    return isSuccess
}

}
