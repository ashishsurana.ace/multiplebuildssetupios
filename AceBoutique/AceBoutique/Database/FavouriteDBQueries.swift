//
//  FavouriteDB.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 05/06/21.
//

import Foundation
import SQLite3

class FavouriteQueries: NSObject {
    
func InsertFavItems(variant_id: String, user_email:String, title: String, price:String, currency_code: String, Img_url: String) -> Void {
    
    let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")
    
    var db: OpaquePointer? = nil
    var statement: OpaquePointer? = nil
    
    if sqlite3_open(path, &db) != SQLITE_OK {
        
    }
    
    let insertSQL = String(format: "insert into FAVOURITE (VARIANT_ID, USER_EMAIL, TITLE, PRICE, CURRENCY_CODE, IMAGE_URL) values (\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", variant_id, user_email, title, price, currency_code, Img_url)

    if sqlite3_prepare_v2(db, insertSQL, -1, &statement, nil) != SQLITE_OK
    {
        let errmsg = String(cString: sqlite3_errmsg(db))
        print("error preparing insert addCountry: \(errmsg)")
    }
    
    if sqlite3_step(statement) != SQLITE_DONE
    {
        let errmsg = String(cString: sqlite3_errmsg(db))
        print("failure inserting null: \(errmsg)")
    }
    
    if sqlite3_finalize(statement) != SQLITE_OK
    {
        let errmsg = String(cString: sqlite3_errmsg(db))
        print("error finalizing prepared statement: \(errmsg)")
    }
    
    statement = nil
    print("Item added to favorite successfully.")
    
}

func GetFavByUserId(user_email: String) -> [FavouriteItems] {

    let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")

    var FavItem = [FavouriteItems]()
    var db: OpaquePointer? = nil
    var statement: OpaquePointer? = nil

    if sqlite3_open(path, &db) != SQLITE_OK {
        
    }

    let selectSQL:String = String(format: "SELECT * FROM FAVOURITE WHERE USER_EMAIL = '%@'", user_email)

    if(sqlite3_prepare_v2(db, selectSQL, -1, &statement, nil) != SQLITE_OK) {


        let errmsg = String(cString: sqlite3_errmsg(db))
        print("Error preparing select getContact: \(errmsg)")
    }

    while sqlite3_step(statement) == SQLITE_ROW {

        let id = Int(sqlite3_column_int64(statement, 0))
        let variant_id = String(cString: sqlite3_column_text(statement, 1))
        let user_email = String(cString: sqlite3_column_text(statement, 2))
        let variant_title = String(cString: sqlite3_column_text(statement, 3))
        let price = String(cString: sqlite3_column_text(statement, 4))
        let currency_code = String(cString: sqlite3_column_text(statement, 5))
        let Img_url = String(cString: sqlite3_column_text(statement, 6))
        
        FavItem.append(FavouriteItems(id: id, variant_id: variant_id, user_email: user_email, title: variant_title, price: price, currency_code: currency_code, Img_url: Img_url))

    }

    if sqlite3_finalize(statement) != SQLITE_OK {

        let errmsg = String(cString: sqlite3_errmsg(db))
        print("Error finalizing prepared statement: \(errmsg)")
    }
    
    statement = nil

    if sqlite3_close(db) != SQLITE_OK {

        print("Error closing database")
    }

    db = nil

    return FavItem
}

func deleteFavItem(variant_Id: String) {
    
    let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")
    
    var db: OpaquePointer? = nil
    
    if sqlite3_open(path, &db) != SQLITE_OK {
        
    }
    
    let deleteStatementString = String.init(format: "DELETE FROM FAVOURITE WHERE VARIANT_ID = '%@'",variant_Id)
    
    var deleteStatement: OpaquePointer?
    
      if sqlite3_prepare_v2(db, deleteStatementString, -1, &deleteStatement, nil) ==
          SQLITE_OK {
        if sqlite3_step(deleteStatement) == SQLITE_DONE {
          print("\nSuccessfully deleted row.")
        } else {
          print("\nCould not delete row.")
        }
      } else {
        print("\nDELETE statement could not be prepared")
      }
      
      sqlite3_finalize(deleteStatement)
    
    db = nil
    
    print("Item deleted successfully.")
    
}

}
