//
//  AppDataBase.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 11/03/21.
//

import Foundation
import SQLite3

class AppDataClass: NSObject {
     
    func createDB()-> Void {
        
        let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")
        print(path)
        var db: OpaquePointer? = nil
        if sqlite3_open(path, &db) != SQLITE_OK {
            
            print("error opening database")
            
        }
        
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS MYCART (id INTEGER PRIMARY KEY AUTOINCREMENT,VARIANT_ID TEXT, TITLE TEXT, VARIANT_TYPE TEXT, VARIANT_VALUE TEXT, PRICE TEXT, CURRENCY_CODE TEXT, QUANTITY INTEGER, IMAGE_URL TEXT)", nil, nil, nil) != SQLITE_OK {
            
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error creating table: \(errmsg)")
        }
        
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS FAVOURITE (id INTEGER PRIMARY KEY AUTOINCREMENT, VARIANT_ID TEXT, USER_EMAIL TEXT, TITLE TEXT, PRICE TEXT, CURRENCY_CODE TEXT, IMAGE_URL TEXT)", nil, nil, nil) != SQLITE_OK {
            
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error creating table: \(errmsg)")
        }
        
//        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS USER (id INTEGER PRIMARY KEY AUTOINCREMENT, FCM_TOKEN TEXT)", nil, nil, nil) != SQLITE_OK {
//            
//            let errmsg = String(cString: sqlite3_errmsg(db))
//            print("error creating table: \(errmsg)")
//        }
         
    }
    
    
//    func InsertToken(token: String) -> Void {
//
//        let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//        let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")
//
//        var db: OpaquePointer? = nil
//        var statement: OpaquePointer? = nil
//
//        if sqlite3_open(path, &db) != SQLITE_OK {
//
//        }
//
//        let insertSQL = String(format: "insert into USER (FCM_TOKEN) values (\"%@\")", token)
//
//        if sqlite3_prepare_v2(db, insertSQL, -1, &statement, nil) != SQLITE_OK
//        {
//            let errmsg = String(cString: sqlite3_errmsg(db))
//            print("error preparing insert token: \(errmsg)")
//        }
//
//        if sqlite3_step(statement) != SQLITE_DONE
//        {
//            let errmsg = String(cString: sqlite3_errmsg(db))
//            print("failure inserting null: \(errmsg)")
//        }
//
//        if sqlite3_finalize(statement) != SQLITE_OK
//        {
//            let errmsg = String(cString: sqlite3_errmsg(db))
//            print("error finalizing prepared statement: \(errmsg)")
//        }
//
//        statement = nil
//        print("token saved successfully.")
//
//    }
    
//    func GetToken() -> String {
//
//        let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//        let path = (documents as NSString).appendingPathComponent("ShopifyDatabase.db")
//
//        var FCM_Token:String = ""
//        var db: OpaquePointer? = nil
//        var statement: OpaquePointer? = nil
//
//        if sqlite3_open(path, &db) != SQLITE_OK {
//
//        }
//
//        let selectSQL:String = String(format: "SELECT * FROM USER")
//
//        if(sqlite3_prepare_v2(db, selectSQL, -1, &statement, nil) != SQLITE_OK) {
//
//
//            let errmsg = String(cString: sqlite3_errmsg(db))
//            print("Error preparing select getContact: \(errmsg)")
//        }
//
//        while sqlite3_step(statement) == SQLITE_ROW {
//
//            let id = Int(sqlite3_column_int64(statement, 0))
//            FCM_Token = String(cString: sqlite3_column_text(statement, 1))
//
//        }
//
//        if sqlite3_finalize(statement) != SQLITE_OK {
//
//            let errmsg = String(cString: sqlite3_errmsg(db))
//            print("Error finalizing prepared statement: \(errmsg)")
//        }
//
//        statement = nil
//
//        if sqlite3_close(db) != SQLITE_OK {
//
//            print("Error closing database")
//        }
//
//        db = nil
//
//        return FCM_Token
//    }
    
    
}




