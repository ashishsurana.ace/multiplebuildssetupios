//
//  DummySplashVC.swift
//  AceBoutique
//
//  Created by Aashiyana Shaikh on 12/08/21.
//

import UIKit
import CryptoSwift
import MobileBuySDK

class DummySplashVC: UIViewController {

    @IBOutlet weak var curvedView: UIView!
    @IBOutlet weak var baseView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        curvedView.roundCorners([.bottomRight, .bottomLeft], radius: 40)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if CheckNetwork.isConnectedToNetwork() == true {

            callAPI.shared.loadJson(fromURLString: appCountriesURL) { [self] result in
                switch result {
                case .success(let data):
                 print(data)
                    self.parseCountries(jsonData: data)

                case .failure(let error):
                    print("Loading countries Json error",error.localizedDescription)

                }
            }

            self.callAPIOfAppLayout { completed,error  in
                if completed {

                    if isFromNotification == true {

                        DispatchQueue.main.async {
                        self.setPushRootController()
                        }
                    }else if isFromDeepLink == true {

                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let window = UIWindow(frame: UIScreen.main.bounds)
                            appDelegateShared.window = window
                            if let detailVC = storyboard.instantiateViewController(withIdentifier: "ProductDetailVC") as? ProductDetailVC {
                                let navigationController = UINavigationController(rootViewController: detailVC)
                                navigationController.navigationBar.isHidden = true
                                appDelegateShared.window?.rootViewController = navigationController
                                appDelegateShared.window?.makeKeyAndVisible()
                            }
                        }
                    }
                    else{
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        DispatchQueue.main.async {
                            if let detailVC = storyBoard.instantiateViewController(withIdentifier: "rootController") as? RootViewController {
                                self.present(detailVC, animated: true, completion: nil)
                            }
                        }
                    }

                }else{
                    DispatchQueue.main.async {
                        self.Showalert1(message: error)
                    }
                }
            }
        }else{
            Showalert1(message: "no_internet_msg".localized)
        }
    }
    
    
    private func parseCountries(jsonData: Data) {
        
        do {
            let decodedData = try! JSONDecoder().decode(countries.self, from: jsonData)
            
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            
            do {
                let jsonData = try encoder.encode(decodedData)
                
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    
                    let jsonData = jsonString.data(using: .utf8)!
                    let objConfig = try! JSONDecoder().decode(countries.self, from: jsonData)
//                    GlobalObjects.arrCountries.removeAll()
                    arrCountries = objConfig
                    print("total countries: \(arrCountries.count)")
                    
                }
            } catch {
                print(error.localizedDescription)
            }
            
        } catch {
            
            print("decode error")
        }
    }
    
    func setPushRootController(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let window = UIWindow(frame: UIScreen.main.bounds)
        appDelegateShared.window = window

        let userInfo = getMyUserDefaults(key: MyUserDefaults.NotificationPayload) as! [AnyHashable: Any]
            guard let actionType = userInfo["action"] as? String else {
                return
            }
            
            if actionType.lowercased() == NotificationActions.general.rawValue.lowercased() {
                
                if let detailVC = storyboard.instantiateViewController(withIdentifier: "rootController") as? RootViewController {
                    appDelegateShared.window?.rootViewController = detailVC
                    appDelegateShared.window?.makeKeyAndVisible()
                }
                
            }else if actionType.lowercased() == NotificationActions.abandoned_checkout.rawValue.lowercased() {
              
                if let detailVC = storyboard.instantiateViewController(withIdentifier: "WebViewCheckoutVC") as? WebViewCheckoutVC {
                    appDelegateShared.window?.rootViewController = detailVC
                    appDelegateShared.window?.makeKeyAndVisible()
                }
                
                
            }else if actionType.lowercased() == NotificationActions.abandoned_cart.rawValue.lowercased() {
               
                if let detailVC = storyboard.instantiateViewController(withIdentifier: "CartNavigationController") as? CartNavigationController {
                    appDelegateShared.window?.rootViewController = detailVC
                    appDelegateShared.window?.makeKeyAndVisible()
                }
                
            }else if actionType.lowercased() == NotificationActions.open_collection.rawValue.lowercased() {
                if let detailVC = storyboard.instantiateViewController(withIdentifier: "ProductListVC") as? ProductListVC {
                    let navigationController = UINavigationController(rootViewController: detailVC)
                    navigationController.navigationBar.isHidden = true
                    appDelegateShared.window?.rootViewController = navigationController
                    appDelegateShared.window?.makeKeyAndVisible()
                }
                
            }else if actionType.lowercased() == NotificationActions.open_product.rawValue.lowercased() {
                if let detailVC = storyboard.instantiateViewController(withIdentifier: "ProductDetailVC") as? ProductDetailVC {
                    let navigationController = UINavigationController(rootViewController: detailVC)
                    navigationController.navigationBar.isHidden = true
                    appDelegateShared.window?.rootViewController = navigationController
                    appDelegateShared.window?.makeKeyAndVisible()
                }
                
            } else {
                if let detailVC = storyboard.instantiateViewController(withIdentifier: "rootController") as? RootViewController {
                    appDelegateShared.window?.rootViewController = detailVC
                    appDelegateShared.window?.makeKeyAndVisible()
                }
                
            }
    }
    
    //MARK:- -
    
    //Method that calls the API to fetch the config.json data and do encode/decode json
    func callAPIOfAppLayout(completion: @escaping (Bool, String) -> Void){
        
//        if CheckNetwork.isConnectedToNetwork() == true {
        callAPI.shared.loadJson(fromURLString: appConfigURL) { [self] result in
            switch result {
            case .success(let data):
                
                let jsonString = callAPI.shared.parse(jsonData: data)
                if jsonString != "" {
                    
                    print("Json Response String1: ",jsonString)
                    
                    let jsonData = jsonString.data(using: .utf8)!
                    let objConfig = try! JSONDecoder().decode(ConfigResponse.self, from: jsonData)
                    
                    GlobalObjects.objConfigResponse = nil
                    GlobalObjects.objConfigResponse = objConfig
                    
                    shopDomain = self.decryptMessage(encryptedMessage: (GlobalObjects.objConfigResponse.shopProperties?.ShopDomain!)!, encryptionKey: encyptionKey, iv: IV)!
                    storeAPIKey = self.decryptMessage(encryptedMessage: (GlobalObjects.objConfigResponse.shopProperties?.APIKEY!)!, encryptionKey: encyptionKey, iv: IV)!
                    adminToken = self.decryptMessage(encryptedMessage: (GlobalObjects.objConfigResponse.shopProperties?.adminToken!)!, encryptionKey: encyptionKey, iv: IV)!
                    graphql_version = (GlobalObjects.objConfigResponse.shopProperties?.GraphQLVersion!)!
                    
                    if let language = getMyUserDefaults(key: MyUserDefaults.LANGUAGE) as? String, language != "" {
                        
                        let locale = GetCurrentLanguage()
                        appLocale = Locale(identifier: locale)
                        
                    }
                    
                    client = Graph.Client(shopDomain: shopDomain, apiKey: storeAPIKey, locale: appLocale)
                    //Get Left Menu settings
                    GlobalObjects.arrMenuItems = objConfig.menu!.items!
                    // GlobalObjects.arrMenuItems = GlobalObjects.arrMenuItems.sorted { $0.order  < $1.order }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAPICall"), object: nil)
                    completion(true, "")
                }
                
            case .failure(let error):
                print("Loading Json error",error.localizedDescription)
                completion(false, error.localizedDescription)
            }
        }
//        }else{
//            self.Showalert1(message: "no_internet_msg".localized)
//        }
    }
    
    
    
    
    // MARK: - AES Encryption Decryption -
    
    func encryptMessage(message: String, encryptionKey: String, iv: String) -> String? {
           if let aes = try? AES(key: encryptionKey, iv: iv),
               let encrypted = try? aes.encrypt(Array<UInt8>(message.utf8)) {
               return encrypted.toBase64()
           }
           return nil
       }

    // Dycript any string
   func decryptMessage(encryptedMessage: String, encryptionKey: String, iv: String) -> String? {
       if let aes = try? AES(key: encryptionKey, iv: iv),
              //to decrypt use (hex: if msg is in hexString and base64: for Base64 String)
               let decrypted = try? aes.decrypt(Array<UInt8>(base64: encryptedMessage)) {
               return String(data: Data(bytes: decrypted), encoding: .utf8)
           }
           return nil
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIView {
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    self.layer.mask = mask
  }
}
