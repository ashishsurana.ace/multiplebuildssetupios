//
//  NotificationViewController.swift
//  APNSContent
//
//  Created by Aashiyana Shaikh on 31/08/21.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet var label: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        self.label?.text = notification.request.content.body
        let size = view.bounds.size
        self.preferredContentSize = CGSize(width: size.width, height: 400)
        self.view.setNeedsUpdateConstraints()
        self.view.setNeedsLayout() 
        
    }

}
